﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Surkhab.VideoSource
{
	class FileVideoSource : IVideoSource
	{
        private FileVideoSource()
        {
            Name = "Video File" + " " + _NameCounter.ToString();
            System.Threading.Interlocked.Increment(ref _NameCounter);
        }

		public static void GetDevices(ref Dictionary<string, IVideoSource> deviceCache)
		{
			var dev = new FileVideoSource();
			if (!deviceCache.ContainsKey(dev.Name))
				deviceCache.Add(dev.Name, dev);
		}

        private static int _NameCounter = 0;
        private string _Name;
		public string Name
		{
			get { return _Name; }
            set { _Name = value; }
		}
        public string Moniker
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public IEnumerable<DeviceCapabilityInfo> Capabilities
		{
			get { return Enumerable.Empty<DeviceCapabilityInfo>(); }
		}
		public DeviceCapabilityInfo SelectedCap { get; set; }
		public System.Drawing.Size VideoSize { get { return System.Drawing.Size.Empty; } }
		public bool HasSettings
		{
			get { return true; }
		}
		public bool IsRunning { get; set; }
		public void ShowSettings()
		{
			this.showSettings();
		}
		private bool showSettings()
		{
			var openFileDialog = new OpenFileDialog();
			// OpenFileDialog.Filter = "AVI File|*.avi|All Files|*.*";

			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				// create video source
				this.videoFile = new AForge.Video.DirectShow.FileVideoSource(openFileDialog.FileName);
				this.videoFile.NewFrame += videoFile_NewFrame;
				return true;
			}
			return false;
		}

		public event AForge.Video.NewFrameEventHandler NewFrame;

		public void Start()
		{
			var isSet = this.videoFile != null;
			//try to set up video file
			if (!isSet)
				isSet = showSettings();
			
			//when video file is set
			if (isSet)
			{
				this.videoFile.Start();
				this.IsRunning = true;
			}
		}
		void videoFile_NewFrame(object sender, AForge.Video.NewFrameEventArgs eventArgs)
		{
			if (this.NewFrame != null)
				this.NewFrame(this, eventArgs);
		}

		public void Stop()
		{
			if (this.videoFile != null)
				this.videoFile.SignalToStop();
			
			this.IsRunning = false;
			this.NewFrame = null;
		}

		private AForge.Video.DirectShow.FileVideoSource videoFile;

		public override string ToString()
		{
			return this.Name;
		}

	}
}
