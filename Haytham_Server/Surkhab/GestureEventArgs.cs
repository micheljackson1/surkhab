﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surkhab
{

        public class GestureEventArgs : EventArgs
        {

            private string gesture;
            private bool hasBegining;

            /// <summary>
            /// Initializes new instance of HeadGestureEventArgs
            /// </summary>
            /// <param name="gesture">The gesture performed.</param>
            public GestureEventArgs(string foundGesture, bool s)
        
            {
                gesture = foundGesture;
                hasBegining = s;
            }


            public string Gesture
            {
                get { return gesture; }
               // set { gesture = value; }
            }
            public bool HasBegining
            {
                get { return hasBegining; }
                // set { gesture = value; }
            }
        }
    
}
