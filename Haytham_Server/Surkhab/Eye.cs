﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;//point
using Emgu.CV.Structure;
using Emgu.CV;
//using AForge;
using AForge.Imaging;
using AForge.Math.Geometry;

namespace Surkhab
{
    public class Eye
    {
        public struct EyeData
        {
            //
            // Summary:
            //     Gets or sets the captured time of this point.
            //
            // Returns:
            //     captured time of this point.
            public DateTime time { get; set; }
            public string tag { get; set; }

            //PUPIL
            public bool pupilFound { get; set; }
            public AForge.Point pupilCenter { get; set; }
            public Ellipse pupilEllipse { get; set; }

            public int pupilDiameter { get; set; }

            //GLINT
            public AForge.Point glintCenter { get; set; }
        }


        public EyeData[] eyeData = new EyeData[500];

        //Images
        public Image<Bgr, Byte> Original;
        public Image<Bgr, Byte> ForShow;

        public bool vFlip;
        public bool rotate180;

        //Pupil
        public bool firstPupilDetection = true;
        public int counterBeforeDrawingIrisCircle;

        private AForge.Point pupilBlobCenter;
        public Blob_Aforge PupilBlob = null;
        public Rectangle pupilROI;

        public Boolean detectPupil;
        public Boolean dilateErode;

        public double MaxPupilScale = 0.9;
        public double MinPupilScale = 0.1;

        public bool showPupil;

        //      Adaptive Threshold
        public bool pupilAdaptive = true;
        public int pupilAdaptive_Constant;
        public int pupilThreshold;
        public int pupilAdaptive_blockSize;
        public Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE pupilAdaptive_type;

        // Glint
        public Boolean detectGlint;

        public int glintPyrLevel;
        public Rectangle glintROI;
        public Blob_Aforge glintBlob = null;

        public int glintThreshold;
        public bool showGlint;
        public Boolean removeGlint;

        //      Adaptive Threshold
        public bool glintAdaptive = true;
        public int glintAdaptive_Constant;
        public int glintAdaptive_blockSize;
        public Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE glintAdaptive_type;


        // Iris
        public bool showIris;
        public int irisDiameter;

        //
        public bool LargScan = true;

        //Gestures and Blink
        public bool Blink = false;
        public bool DoubleBlink = false;
        private bool CloseEye;
        private bool WaitForDoubleBlink;
        private int blinkcounter;
        private int doubleblinkWaitingTime_Counter;


        //....................................................................................................

        public AForge.Point GetGlintCenterMedian(int frames)
        {
            AForge.Point M = new AForge.Point();
            float[] Cx = new float[frames];
            float[] Cy = new float[frames];


            for (int i = 0; i < frames; i++)
            {
                Cx[i] = eyeData[i].glintCenter.X;
                Cy[i] = eyeData[i].glintCenter.Y;

            }

            M.X = (float)ImageProcessing_Emgu.FindMedian(Cx);
            M.Y = (float)ImageProcessing_Emgu.FindMedian(Cy);
            return M;
        }

        public AForge.Point GetPupilCenterMedian(int frames)
        {
            AForge.Point M = new AForge.Point();
            float[] Cx = new float[frames];
            float[] Cy = new float[frames];


            for (int i = 0; i < frames; i++)
            {
                Cx[i] = eyeData[i].pupilCenter.X;
                Cy[i] = eyeData[i].pupilCenter.Y;

            }

            M.X = (float)ImageProcessing_Emgu.FindMedian(Cx);
            M.Y = (float)ImageProcessing_Emgu.FindMedian(Cy);
            return M;
        }

        public AForge.Point GetPupilGlintVectorMedian(int frames)
        {
            AForge.Point M = new AForge.Point();
            float[] Cx = new float[frames];
            float[] Cy = new float[frames];


            for (int i = 0; i < frames; i++)
            {
                Cx[i] = eyeData[i].glintCenter.X - eyeData[i].pupilCenter.X;
                Cy[i] = eyeData[i].glintCenter.Y - eyeData[i].pupilCenter.Y;

            }

            M.X = (float)ImageProcessing_Emgu.FindMedian(Cx);
            M.Y = (float)ImageProcessing_Emgu.FindMedian(Cy);
            return M;
        }

        public SizeF GetPupilEllipseSizeMedian(int frames)
        {
            SizeF M = new SizeF();
            float[] Cx = new float[frames];
            float[] Cy = new float[frames];


            for (int i = 0; i < frames; i++)
            {
                Cx[i] = (float)eyeData[i].pupilEllipse.MCvBox2D.size.Width;
                Cy[i] = (float)eyeData[i].pupilEllipse.MCvBox2D.size.Height;

            }

            M.Width = (float)ImageProcessing_Emgu.FindMedian(Cx);
            M.Height = (float)ImageProcessing_Emgu.FindMedian(Cy);
            return M;
        }

        public void FindGlint(Image<Bgr, Byte> inputimg, int threshold)
        {

            glintPyrLevel = 0;
            Image<Gray, Byte> GrayImg = new Image<Gray, byte>(inputimg.Bitmap);


            //rough determination of ROI (size of ROI may exceeds the size of the image no problem for cvSetImageROI)
            int d = (int)(irisDiameter);
            if (detectPupil & eyeData[1].pupilFound)
            {
                glintROI = new Rectangle(
                    Math.Max(Convert.ToInt32(eyeData[1].pupilCenter.X) - d / 2, 0),
                      Math.Max(Convert.ToInt32(eyeData[1].pupilCenter.Y) - d / 2, 0),
                      Math.Min(d, GrayImg.Width),
                      Math.Min(d, GrayImg.Height)

                    );
            }
            else
            {
                d = 1 * d;//a little bit larger
                glintROI = new Rectangle(inputimg.Width / 2 - d / 2, inputimg.Height / 2 - d / 2, d, d);
            }


            //Modify ROI, it's needed when ROI is not inside the image area
            CvInvoke.cvSetImageROI(GrayImg, glintROI);


            //ROI will be used later for drawing
            glintROI = new Rectangle(glintROI.X, glintROI.Y, GrayImg.Width, GrayImg.Height);


            for (int i = 0; i < glintPyrLevel; i++)
            {
                GrayImg = GrayImg.PyrDown();
            }

            if (glintAdaptive)
            {
                GrayImg = ImageProcessing_Emgu.Filter_GlintAdaptiveThreshold(this, GrayImg, 255, false).Erode(1).Dilate(1);
            }
            else
            {
                GrayImg = ImageProcessing_Emgu.Filter_Threshold(GrayImg, threshold, false);
            }

            AForge.Point tempGlintCenter = new AForge.Point(0, 0);

            //METState.Current.ProcessTimeEyeBranch.Timer(glintBlobTimerName /*"GlintBlob"*/, "Start");
            glintBlob = new Blob_Aforge(GrayImg.Bitmap, 5, 30, 5, 30, 0.4, 10);
            //METState.Current.ProcessTimeEyeBranch.Timer(glintBlobTimerName, "Stop");

            glintPyrLevel++;

            #region filter blob
            if (glintBlob.blobs_Filtered.Count > 0)
            {

                glintBlob.SelectedBlob = glintBlob.blobs_Filtered[0];
                tempGlintCenter = CorrectGlintPoint(glintBlob.SelectedBlob.CenterOfGravity);

                if (glintBlob.blobs_Filtered.Count >= 2)
                {
                    List<Blob> temp = glintBlob.blobs_Filtered;
                    List<double> dists = new List<double>();

                    foreach (AForge.Imaging.Blob blob in temp)
                    {
                        tempGlintCenter = CorrectGlintPoint(blob.CenterOfGravity);

                        double dist = Math.Sqrt((Math.Pow(tempGlintCenter.X - eyeData[1].pupilCenter.X, 2)) + (Math.Pow(tempGlintCenter.Y - eyeData[1].pupilCenter.Y, 2)));
                        dists.Add(dist);
                    }
                    int i = dists.IndexOf(dists.Min());
                    ////----------------------------------------------------This filter takes the one which is closer to the pupil 
                    glintBlob.SelectedBlob = temp[i];
                    tempGlintCenter = CorrectGlintPoint(glintBlob.SelectedBlob.CenterOfGravity);


                    ////----------------------------------------------------This filter takes the average of the two closest glint to the pupil
                    //dists.RemoveAt(i);
                    //temp.RemoveAt(i);
                    //i = dists.IndexOf(dists.Min());//this will select the second closest glint
                    //AForge.Point tempGlintCenter2 = CorrectGlintPoint(temp[i].CenterOfGravity);
                    //tempGlintCenter = new AForge.Point((tempGlintCenter.X + tempGlintCenter2.X) / 2, (tempGlintCenter.Y + tempGlintCenter2.Y) / 2);

                }

            #endregion filter blob
            }


            eyeData[0].glintCenter = tempGlintCenter;
        }

        public AForge.Point CorrectGlintPoint(AForge.Point inp)
        {
            AForge.Point oup = new AForge.Point();

            oup.X = (float)inp.X * glintPyrLevel + glintROI.X;
            oup.Y = (float)inp.Y * glintPyrLevel + glintROI.Y;


            return oup;

        }

        public Eye()
        {
        }

        public void FindPupil(Image<Bgr, Byte> inputimg, int threshold)
        {
            try
            {
                #region Find Pupil
                Image<Bgr, Byte> CropedInputimg = new Image<Bgr, byte>(inputimg.Bitmap);
                Image<Gray, Byte> temp = new Image<Gray, byte>(inputimg.Width, inputimg.Height);

                SetPupilROI(CropedInputimg, true);
                SetPupilROI(ForShow, true);
                //if you want to reduce the size of the image e.g. PyrDown you should consider it for translation of the coordinates after blobDetection. 
                //Now I have assumed that translation is only ROI.corner and there is no *Factor

                #region Remove Glint

                if (!LargScan & removeGlint == true)
                {
                    //METState.Current.ProcessTimeEyeBranch.Timer(glintRemoveTimerName /*"GlintRemove"*/, "Start");
                    Image<Gray, Byte> GlintMask = new Image<Gray, byte>(CropedInputimg.Width, CropedInputimg.Height);

                    CropedInputimg = CropedInputimg.PyrDown();
                    GlintMask = CropedInputimg.Convert<Gray, byte>();
                    GlintMask = ImageProcessing_Emgu.Filter_Threshold(GlintMask, glintThreshold, false).Dilate(1);

                    if (removeGlint == true) CvInvoke.cvInpaint(CropedInputimg.Ptr, GlintMask.Ptr, CropedInputimg.Ptr, 3, Emgu.CV.CvEnum.INPAINT_TYPE.CV_INPAINT_NS);
                    CropedInputimg = CropedInputimg.PyrUp();

                    //METState.Current.ProcessTimeEyeBranch.Timer(glintRemoveTimerName, "Stop");

                }

                #endregion Remove Glint

                #region Gray & ErodeDilate
                Image<Gray, Byte> GrayImg;

                if (dilateErode == true)
                {
                    // only when largescan=false
                    //METState.Current.ProcessTimeEyeBranch.Timer(fillGapsTimerName /*"Fill Gaps"*/, "Start");
                    // GrayImg = (LargScan == true) ? CropedInputimg.Convert<Gray, byte>() : CropedInputimg.Convert<Gray, byte>().Erode(4).Dilate(4);
                    GrayImg = CropedInputimg.Convert<Gray, byte>().Erode(4).Dilate(3);

                    //METState.Current.ProcessTimeEyeBranch.Timer(fillGapsTimerName, "Stop");
                }
                else
                {

                    GrayImg = CropedInputimg.Convert<Gray, byte>();
                }
                #endregion Gray & ErodeDilate



                //METState.Current.ProcessTimeEyeBranch.Timer(pupilDetectionTimerName /*"Pupil Detection"*/, "Start");
                #region Threshold

                int constant = -1;
                // if (!LargScan & METState.Current.PAdaptive_new) constant = PAConstantSet(GrayImg);
                //Debug.WriteLine(constant + "," + METState.Current.PAdaptive_Constant);

                if (pupilAdaptive)
                {
                    GrayImg = ImageProcessing_Emgu.Filter_PupilAdaptiveThreshold(this, GrayImg, 255, true, constant);
                }
                else
                {
                    GrayImg = ImageProcessing_Emgu.Filter_Threshold(GrayImg, threshold, true);
                }

                #endregion Threshold



                // GrayImg = GrayImg.Erode(3).Dilate(2);

                GrayImg = GrayImg.Dilate(2).Erode(2);


                DetectPupilBlob(GrayImg, inputimg.Width, inputimg.Height);
                //METState.Current.ProcessTimeEyeBranch.Timer(pupilDetectionTimerName, "Stop");


                #region Show threshold mask
                if (showPupil)
                {
                    if (LargScan) CropedInputimg = ImageProcessing_Emgu.ColoredMask((Bitmap)PupilBlob.image, CropedInputimg, new Bgr(System.Drawing.Color.Green), true);
                }
                #endregion Show threshold mask

                CvInvoke.cvCopy(CropedInputimg.Ptr, ForShow.Ptr, new IntPtr());

                CvInvoke.cvResetImageROI(ForShow);
                CvInvoke.cvResetImageROI(CropedInputimg);//??


                #endregion Find Pupil


                #region Ellipse Drawing and modifing
                if (eyeData[0].pupilFound)
                {
                    if (showPupil)
                        ImageProcessing_Emgu.DrawEllipse(ForShow, eyeData[0].pupilEllipse, new Bgr(255, 255, 255));



                    //emgu ellipse bug: I need to modify it again after drawing. now ellipse box, verteces and angle is fine
                    eyeData[0].pupilEllipse = new Emgu.CV.Structure.Ellipse(eyeData[0].pupilEllipse.MCvBox2D.center, new SizeF(eyeData[0].pupilEllipse.MCvBox2D.size.Width, eyeData[0].pupilEllipse.MCvBox2D.size.Height), eyeData[0].pupilEllipse.MCvBox2D.angle - 90);
                    // ImageProcessing_Emgu.DrawRectangle(METState.Current.EyeImageForShow, pupildata[0].pupilEllipse.MCvBox2D.GetVertices(), 0, false, "");
                    // ImageProcessing_Emgu.DrawRectangle(METState.Current.EyeImageForShow, pupildata[0].pupilEllipse.MCvBox2D.center, pupildata[0].pupilEllipse.MCvBox2D.size);

                }
                #endregion Ellipse Drawing and modifing


                pupildata_Update(MeasureCenter(), MeasureDiameter());//MeasureDiameter should be after modification of ellipse

                blink(ref Blink, ref DoubleBlink);
            }
            catch (Exception error)
            {
                MainForm.logger.ErrorException("Eye.FindPupil", error);
                //System.Windows.Forms.MessageBox.Show(error.ToString());
            }


        }

        public void DetectPupilBlob(Image<Gray, Byte> inputimg, int fullSizeImageW, int fullSizeImageH)
        {
            pupilBlobCenter = new AForge.Point(0, 0);

            //Blob_Aforge PupilBlob;
            //PupilBlob = new Blob_Aforge(inputimg.Bitmap,
            //   (int)(METState.Current.MinPupilDiameter),
            //   (int)(METState.Current.MaxPupilDiameter),
            //   (int)(METState.Current.MinPupilDiameter),
            //    (int)(METState.Current.MaxPupilDiameter),
            //    0.55, 2);//should be defined in each frame
            PupilBlob = new Blob_Aforge(inputimg.Bitmap,
                (int)(MinPupilScale * irisDiameter),
                (int)(MaxPupilScale * irisDiameter),
                (int)(MinPupilScale * irisDiameter),
                (int)(MaxPupilScale * irisDiameter),
                0.55, 4);//????????????????????????????????????????????????    ,2)


            if (PupilBlob.blobs_Filtered.Count == 0)//Pupil not found
            {
                eyeData[0].pupilFound = false;

            }
            else// Pupil found
            {
                PupilBlob.SelectedBlob = PupilBlob.blobs_Filtered[0];

                #region distance from borders
                //if (PupilBlob.blobs_Filtered.Count == 1)
                //{
                //    double dist = Math.Sqrt((Math.Pow(PupilBlob.blobs_Filtered[0].CenterOfGravity.X - METState.Current.EyeImageOrginal.Width / 2, 2)) + (Math.Pow(PupilBlob.blobs_Filtered[0].CenterOfGravity.Y - METState.Current.EyeImageOrginal.Height / 2, 2)));
                //    if (dist > 0.850 * (METState.Current.EyeImageOrginal.Height / 2))
                //    {

                //        // PupilBlob.blobs_Filtered.Clear();
                //        // PupilBlob.SelectedBlob = null;
                //        _PupilBlobCenter.X = 0;
                //        _PupilBlobCenter.Y = 0;

                //    }

                //}
                #endregion filter by location


                #region distance from center
                if (PupilBlob.blobs_Filtered.Count > 1)//??????????????? mage nagofti faghat yedoone blob peida kon ... 0.55, 1);
                {
                    double minDis = 10000;

                    foreach (AForge.Imaging.Blob blob in PupilBlob.blobs_Filtered)
                    {
                        double dist = Math.Sqrt((Math.Pow(blob.CenterOfGravity.X - Original.Width / 2, 2)) + (Math.Pow(blob.CenterOfGravity.Y - Original.Height / 2, 2)));

                        if (dist < minDis)
                        {
                            PupilBlob.SelectedBlob = blob;
                            // border = PupilBlob.hulls[i];
                            minDis = dist;
                        }

                    }
                }

                #endregion distance from center



                //get SelectedBlob   
                pupilBlobCenter.X = (float)PupilBlob.SelectedBlob.CenterOfGravity.X;
                pupilBlobCenter.Y = (float)PupilBlob.SelectedBlob.CenterOfGravity.Y;

                //  pupildata[0].pupilFound = true;//.............2


                bool blobOnROIBorder;

                AForge.Point[] border = PupilBlob.GetBlobBorder(PupilBlob.SelectedBlob, pupilROI, new Size(fullSizeImageW, fullSizeImageH), out blobOnROIBorder);


                // pupildata[0].pupilEllipse = ImageProcessing_Emgu.EllipseLeastSquareFitting(border);//.................2


                //...................................1
                //............................remove 2

                if (blobOnROIBorder)//
                {
                    eyeData[0].pupilFound = false;
                }
                else
                {
                    eyeData[0].pupilEllipse = ImageProcessing_Emgu.EllipseLeastSquareFitting(border);
                    eyeData[0].pupilFound = true;

                }

            }

        }

        public int MeasureDiameter()
        {
            int d = 0;
            if (eyeData[0].pupilFound)
            {
                //approximately
                //d = (int)(PupilBlob.SelectedBlob.Rectangle.Height);

                //large axis of ellipse
                d = (int)eyeData[0].pupilEllipse.MCvBox2D.size.Height;//after modifying the ellipse twice


            }
            return d;
        }

        public AForge.Point MeasureCenter()
        {
            AForge.Point c = new AForge.Point(0, 0);

            if (eyeData[0].pupilFound)
            {

                //Debug.WriteLine(pupilROI.X + "," + pupilROI.Y);
                c.X = pupilBlobCenter.X + pupilROI.X;
                c.Y = pupilBlobCenter.Y + pupilROI.Y;


            }

            return c;
        }

        public void eyeData_Shift()
        {

            for (int i = eyeData.Length - 1; i > 0; i--) { eyeData[i] = eyeData[i - 1]; }
            eyeData[0] = new EyeData();
            eyeData[0].time = DateTime.Now;

        }

        public void pupildata_Update(AForge.Point pupilCenter, int pupilDiameter)
        {
            eyeData[0].pupilCenter = pupilCenter;
            eyeData[0].pupilDiameter = pupilDiameter;
        }

        /// <summary>
        /// Represents the method that will handle HeadGesture events.
        /// </summary>
        /// <param name="sender">The source of event.</param>
        /// <param name="start">A HeadGestureEventArgs that contains event data.</param>
        public delegate void GestureHandler(object sender, GestureEventArgs e);

        /// <summary>
        /// Occurs whether valid head gesture is performed.
        /// </summary>
        public event GestureHandler Gesture;

        public void blink(ref bool Blink, ref bool DoubleBlink)
        {
            GestureEventArgs args = null;

            Blink = false;
            DoubleBlink = false;


            int blinksensitivity = 70;//max time of closed eye to be considered as blink
            int doubleblinkWaitingTime = 20;



            if (eyeData[0].pupilFound)
            {
                # region eye is open................................................................


                # region just after opening the eye get blink or DbBlink

                if (CloseEye & blinkcounter < blinksensitivity & blinkcounter > 1)//last close was a real blink
                {

                    if (WaitForDoubleBlink)
                    {
                        if (doubleblinkWaitingTime_Counter < doubleblinkWaitingTime)
                        {
                            WaitForDoubleBlink = false;
                            DoubleBlink = true;
                            if (Gesture != null) { args = new GestureEventArgs("DbBlink", false); Gesture(this, args); }
                        }
                        else
                        {
                            WaitForDoubleBlink = false;
                            doubleblinkWaitingTime_Counter = 0;
                            Blink = true;
                            if (Gesture != null) { args = new GestureEventArgs("Blink", false); Gesture(this, args); }
                        }
                    }
                    else
                    {
                        WaitForDoubleBlink = true;
                        doubleblinkWaitingTime_Counter = 0;

                    }
                    //Blink = true;

                }
                # endregion just after opening the eye get blink or DbBlink

                # region  Double Blink waiting time

                if (WaitForDoubleBlink)
                {
                    if (doubleblinkWaitingTime_Counter <= doubleblinkWaitingTime)
                    {
                        doubleblinkWaitingTime_Counter++;

                    }
                    else
                    {
                        doubleblinkWaitingTime_Counter = 0;
                        WaitForDoubleBlink = false;
                        Blink = true;
                        if (Gesture != null) { args = new GestureEventArgs("Blink", false); Gesture(this, args); }

                    }
                }
                # endregion  Double Blink waiting time


                CloseEye = false;
                blinkcounter = 0;
                #  endregion eye is open................................................................
            }
            else
            {
                #region eye is close................................................................
                CloseEye = true;
                blinkcounter++;

                #region Use eye close

                #endregion  Use eye close

                #endregion eye is close................................................................
            }


            #region use Blink

            if (DoubleBlink)
            {
                /// Your code here
            }
            else if (Blink)
            {
                /// Your code here
            }
            #endregion use Blink

        }

        private void SetPupilROI(Image<Bgr, Byte> inputimg, bool ChangeWidthByDiameter)
        {
            double Factor = 2;//2
            int ROIWidth = 150;//???????????????????????????????????????????depends on resolution
            int W;
            int H;

            if (eyeData[1].pupilFound)//pupil was founded in previous frame
            {
                if (ChangeWidthByDiameter)
                {

                    W = (eyeData[1].pupilDiameter == 0) ? ROIWidth : (int)Math.Truncate(eyeData[1].pupilDiameter * Factor);
                    H = W;
                }
                else
                {
                    W = ROIWidth;
                    H = W;

                }
                pupilROI = new Rectangle((int)eyeData[1].pupilCenter.X - (W / 2), (int)eyeData[1].pupilCenter.Y - (H / 2), W, H);
                LargScan = false;
            }
            else
            {
                pupilROI = new Rectangle(0, 0, inputimg.Width, inputimg.Height);
                LargScan = true;
            }

            //size of ROI may exceeds the size of the image no problem for cvSetImageROI
            CvInvoke.cvSetImageROI(inputimg, pupilROI);

            //Modify ROI, it's needed when ROI was not inside the image area
            //ROI will be used later for drowing        
            pupilROI = new Rectangle(inputimg.ROI.Location.X, inputimg.ROI.Location.Y, inputimg.ROI.Width, inputimg.ROI.Height);
        }
    }
}
