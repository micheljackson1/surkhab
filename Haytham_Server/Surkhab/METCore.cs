﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.IO;
using AForge.Video.DirectShow;
using AForge.Video;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;


namespace Surkhab
{

    public class METCore
    {
        public _SendToForm SendToForm;

        public event TrackerEventHandler TrackerEventEye1;
        public event TrackerEventHandler TrackerEventEye2;


        private void FrameCaptured(Eye eye, NewFrameEventArgs eventArgs, string additionalArg, OnTrackerEventHandler onTrackerEvent)
        {
            var tempImage = new Image<Bgr, Byte>((Bitmap)eventArgs.Frame.Clone());

            if (eye.vFlip)
            {
                tempImage = tempImage.Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
            }

            if (eye.rotate180)
            {
                tempImage = tempImage.Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
                tempImage = tempImage.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
            }

            eye.Original = tempImage;
            eye.ForShow = tempImage.Clone();

            METEventArg metEventArg = new METEventArg();
            metEventArg.AdditionalArg = additionalArg;
            metEventArg.image = tempImage.Clone();

            onTrackerEvent(metEventArg);
        }

        public void Eye1FrameCaptured(object sender, NewFrameEventArgs eventArgs)
        {
            FrameCaptured(METState.Current.eye1, eventArgs, "Eye1", OnTrackerEventEye1);
        }

        public void Eye2FrameCaptured(object sender, NewFrameEventArgs eventArgs)
        {
            FrameCaptured(METState.Current.eye2, eventArgs, "Eye2", OnTrackerEventEye2);
        }

        private delegate void MainMethodEye(object sender, METEventArg e);

        private void MainMethod(Eye eye, METEventArg e, string pupilThresholdName,
            string glintCenterName, string pupilCenterName, 
            string eyeName,
            Surkhab.VideoSource.IVideoSource eyeCamera)
        {
            //METState.Current.ProcessTimeEyeBranch.Timer(totalTimerName /*"Total"*/, "Start");

            eye.eyeData_Shift();//Prepare eyeDataArray for new frame

            #region set pupil threshold first time
            //should be improved.................
            if (eye.firstPupilDetection)
            {
                eye.firstPupilDetection = false;
                Image<Gray, Byte> GrayImg = e.image.Convert<Gray, Byte>();
                // gather statistics
                AForge.Imaging.ImageStatistics stat = new AForge.Imaging.ImageStatistics(GrayImg.Bitmap);
                // get red channel's histogram
                AForge.Math.Histogram b = stat.Gray;
                // check mean value of red channel
                // Debug.WriteLine(b.Mean + "," + b.StdDev + "," + b.Min);
                //
                eye.pupilThreshold = (int)b.Mean / 2;
                SendToForm(eye.pupilThreshold, pupilThresholdName/*"Pupil1Threshold"*/);

            }
            #endregion set pupil threshold first time

            #region find glint
            if (eye.detectGlint)
            {
                //METState.Current.ProcessTimeEyeBranch.Timer(findGlintTimerName /*"Find Glint"*/, "Start");
                if ((!eye.detectPupil) | (eye.detectPupil & eye.eyeData[1].pupilFound))
                {
                    eye.FindGlint(e.image, eye.glintThreshold);// Glintcenter=(0,0) if there is no glint
                }
                if (eye.eyeData[0].glintCenter != new AForge.Point(0, 0)) SendToForm(eye.eyeData[0].glintCenter, glintCenterName /*"lblG1C"*/);
                else SendToForm("NoGlint", glintCenterName);

                //METState.Current.ProcessTimeEyeBranch.Timer(findGlintTimerName, "Stop");
            }
            #endregion find glint

            #region find pupil

            if (eye.detectPupil)
            {
                //METState.Current.ProcessTimeEyeBranch.Timer(findPupilTimerName /*"Find Pupil"*/, "Start");

                eye.FindPupil(e.image, eye.pupilThreshold);

                #region Draw Iris
                if (eye.showIris)
                {
                    SizeF size = new SizeF((eye.GetPupilEllipseSizeMedian(10).Width / eye.GetPupilEllipseSizeMedian(10).Height) * (float)eye.irisDiameter, (float)eye.irisDiameter);
                    if (size.Width == 0 | float.IsNaN(size.Width)) size = new SizeF((float)eye.irisDiameter, (float)eye.irisDiameter);//for the first frames where median is 0 or NoN


                    if (eye.LargScan == false && eye.eyeData[0].pupilFound)
                    {
                        eye.counterBeforeDrawingIrisCircle = 0; /*METState.Current.CounterBeforeDrawingIrisCircle1 = 0;*/
                        Ellipse IrisEllipse = new Emgu.CV.Structure.Ellipse(new PointF(eye.eyeData[0].pupilCenter.X, eye.eyeData[0].pupilCenter.Y), size, eye.eyeData[0].pupilEllipse.MCvBox2D.angle - 90);
                        ImageProcessing_Emgu.DrawEllipse(eye.ForShow, IrisEllipse, new Bgr(100, 100, 100));
                    }
                    else
                    {
                        #region wait
                        bool wait = false;
                        if (eye.counterBeforeDrawingIrisCircle /*METState.Current.CounterBeforeDrawingIrisCircle1*/ < 10)
                        {
                            eye.counterBeforeDrawingIrisCircle++;
                            wait = true;
                        }

                        #endregion wait

                        if (!wait) ImageProcessing_Emgu.DrawIrisCircle(eye, eye.ForShow, eye.ForShow.Width / 2, eye.ForShow.Height / 2, (int)(eye.irisDiameter));
                    }
                }
                #endregion Draw Iris

                if (eye.eyeData[0].pupilFound) SendToForm(eye.eyeData[0].pupilCenter, pupilCenterName /*"lblP1C"*/);
                else SendToForm("NoPupil", pupilCenterName);


                //METState.Current.ProcessTimeEyeBranch.Timer(findPupilTimerName, "Stop");
            }
            #endregion find pupil


            #region glint Drawing (injast chon mikham rooye drawing pupil biofte)

            if (eye.detectGlint && eye.eyeData[0].glintCenter != new AForge.Point(0, 0))
            {
                if (eye.showGlint)
                {
                    try
                    {
                        CvInvoke.cvSetImageROI(eye.ForShow, eye.glintROI);
                        Image<Bgr, Byte> temp = ImageProcessing_Emgu.ColoredMask((Bitmap)eye.glintBlob.image, eye.ForShow, new Bgr(System.Drawing.Color.DarkBlue), false);
                        CvInvoke.cvCopy(temp.Ptr, eye.ForShow.Ptr, new IntPtr());
                        CvInvoke.cvResetImageROI(eye.ForShow);
                    }
                    catch (Exception err)
                    {
                        MainForm.logger.ErrorException("METCore.MainMethod", err);
                        // System.Windows.Forms.MessageBox.Show(err.ToString());
                    }

                    if (eye.eyeData[0].glintCenter.X != 0)//glint found
                    {

                        if (eye.detectPupil & eye.eyeData[1].pupilFound)
                            ImageProcessing_Emgu.DrawLine(eye.ForShow, eye.eyeData[0].glintCenter, eye.eyeData[0].pupilCenter, System.Drawing.Color.Black);
                        else if (!eye.detectPupil)
                            ImageProcessing_Emgu.DrawCross(eye.ForShow, (int)eye.eyeData[0].glintCenter.X, (int)eye.eyeData[0].glintCenter.Y, System.Drawing.Color.Green);
                    }
                }
            }

            #endregion glint  Drawing (injast chon mikham rooye drawing pupil biofte)

            #region Send data

            METState.Current.server.Send(eyeName/*"Eye1"*/, new string[]
            {
                eye.eyeData[0].time.Ticks.ToString(),
                eye.eyeData[0].pupilDiameter.ToString(),
                eye.eyeData[0].pupilFound.ToString(),
                ((float)eye.eyeData[0].pupilCenter.X/eyeCamera.VideoSize.Width /*METState.Current.Eye1Camera.VideoSize.Width*/).ToString(),
                ((float)eye.eyeData[0].pupilCenter.Y/eyeCamera.VideoSize.Height /*METState.Current.Eye1Camera.VideoSize.Height*/).ToString(),
            });

            #endregion Send data

            //METState.Current.ProcessTimeEyeBranch.Timer(totalTimerName, "Stop");
            //SendToForm("", textBoxTimerName /*"textBoxTimerEye1"*/);//update the timer text box and show the ProcessTimeEyeBranch
            //METState.Current.ProcessTimeEyeBranch.TimerResults.Clear();
        }
            

        public void MainMethodEye1(object sender, METEventArg e)
        {
            string eyeName;

            if (METState.Current.monoOrBi == METState.MonoOrBi.BinocularEyeTracking)
            {
                eyeName = "Eye1";
            }
            else
            {
                eyeName = "Eye";
            }

            MainMethod(METState.Current.eye1, e, "Pupil1Threshold", "lblG1C", "lblP1C",
                eyeName, METState.Current.Eye1Camera);
        }

        public void MainMethodEye2(object sender, METEventArg e)
        {
            MainMethod(METState.Current.eye2, e, "Pupil2Threshold", "lblG2C", "lblP2C",
                "Eye2", METState.Current.Eye2Camera);
        }

        private void OnTrackerEvent(TrackerEventHandler trackerEventHandler, MainMethodEye mainMethodEye, METEventArg e)//protected virtual
        {
            if (trackerEventHandler != null)
            {
                mainMethodEye(this, e);
                try
                {
                    trackerEventHandler(this, e);//Raise the event
                }
                catch (Exception err)
                {
                    MainForm.logger.ErrorException("METCore.OnTrackerEvent", err);
                }
            }
        }

        private delegate void OnTrackerEventHandler(METEventArg e);

        public void OnTrackerEventEye1(METEventArg e)//protected virtual
        {
            OnTrackerEvent(TrackerEventEye1, MainMethodEye1, e);
        }

        public void OnTrackerEventEye2(METEventArg e)//protected virtual
        {
            OnTrackerEvent(TrackerEventEye2, MainMethodEye2, e);
        }
    }
}
