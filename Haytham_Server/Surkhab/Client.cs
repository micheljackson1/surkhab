﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Adrian Voßkühler
// email: adrian@.net
// modified by: Michel Jackson
// email: michel@sens.com


namespace Surkhab
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Net.Sockets;
    using System.Windows.Forms;

    /// <summary>
    /// </summary>
    public class Client
    {
        #region Fields

        /// <summary>
        ///   Socket for accepting a connection
        /// </summary>
        private readonly Socket connection;

        /// <summary>
        ///   The reader facilitates reading from the stream
        /// </summary>
        private readonly BinaryReader reader;

        /// <summary>
        ///   The reference to server
        /// </summary>
        private readonly Server server;

        /// <summary>
        ///   The socket network data stream
        /// </summary>
        private readonly NetworkStream socketStream;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="socket">
        /// The socket.
        /// </param>
        /// <param name="serverValue">
        /// The server value.
        /// </param>
        public Client(Socket socket, Server serverValue)
        {
            this.connection = socket;
            this.UserData = new Dictionary<string, string>();
            this.Status = new Dictionary<string, bool>
                      {
                        { "_Commands", false }, 
                        { "_Eye", false }, 
                        { "_Eye1", false }, 
                        { "_Eye2", false }, 
                        { "_Volume", false }, 
                        { "_VisualMarker", false }
                      };

            // create NetworkStream object for Socket
            this.socketStream = new NetworkStream(this.connection);

            // create Streams for reading/writing bytes
            this.Writer = new BinaryWriter(this.socketStream);
            this.reader = new BinaryReader(this.socketStream);
            this.server = serverValue;

            try
            {
                this.ClientType = this.reader.ReadString();
                this.ClientName = this.reader.ReadString();

                this.ClientName = this.server.DetermineClientName(this.ClientName, this.ClientType);

                MainForm.logger.Info("Client {0} connected", this.ClientName);
            }
            catch (Exception e)
            {
                MainForm.logger.WarnException("Client.ctor", e);
                this.ClientName = string.Empty;
            }

            if (this.ClientName == string.Empty)
            {
                this.Writer.Write("ERROR");
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Gets or sets the client name
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        ///   Gets or sets the client type
        /// </summary>
        public string ClientType { get; set; }

        /// <summary>
        ///   Gets or sets the height of the screen
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        ///   Gets or sets the status
        /// </summary>
        public Dictionary<string, bool> Status { get; set; }

        /// <summary>
        ///   Gets or sets the user data
        /// </summary>
        public Dictionary<string, string> UserData { get; set; }

        /// <summary>
        ///   Gets or sets the width of the screen
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        ///   Gets or sets the writer that facilitates writing to the stream
        /// </summary>
        public BinaryWriter Writer { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///   Runs this instance.
        /// </summary>
        public void Run()
        {
            this.Writer.Write(this.ClientName);

            string theReply;
            do
            {
                try
                {
                    // read the string sent to the server
                    // wait for message
                    theReply = this.reader.ReadString();

                    if (theReply == "Status_Commands")
                    {
                        this.Status["_Commands"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply == "Status_Eye")
                    {
                        this.Status["_Eye"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply == "Status_Eye1")
                    {
                        this.Status["_Eye1"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply == "Status_Eye2")
                    {
                        this.Status["_Eye2"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply == "Status_Volume")
                    {
                        this.Status["_Volume"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply == "Status_VisualMarker")
                    {
                        this.Status["_VisualMarker"] = Convert.ToBoolean(this.reader.ReadString());
                    }
                    else if (theReply.StartsWith("UserData"))
                    {
                        this.GetUserData(theReply);
                    }
                    else if (theReply == "Size")
                    {
                        this.Width = this.reader.ReadInt32();
                        this.Height = this.reader.ReadInt32();
                        this.server.DisplayMessage(
                          "\r\n resolution of " + this.ClientName + ": " + this.Width + "x" + this.Height + "\r\n");
                    }
                }
                catch (Exception e)
                {
                    MainForm.logger.WarnException("Client.Run", e);
                    break;
                }
            }
            while (theReply != "CLIENT>>> TERMINATE" && this.connection.Connected);

            // close the socket connection
            MainForm.logger.Info("Disconnecting client {0}", this.ClientName);

            this.Writer.Close();
            this.reader.Close();
            this.socketStream.Close();
            this.connection.Close();
            this.server.DisplayMessage("\r\n" + this.ClientName + " disconnected \r\n");
            this.server.DisplayMessage("*********************************************\r\n");
            this.server.RemoveClient(this.ClientName);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the user data.
        /// </summary>
        /// <param name="msg">
        /// The MSG.
        /// </param>
        private void GetUserData(string msg)
        {
            // var xxx = msg.Split('|').Skip(1);
            string temp = string.Empty;
            var msgArr = new List<string>();

            foreach (char t in msg)
            {
                if (t == '|')
                {
                    msgArr.Add(temp);
                    temp = string.Empty;
                }
                else
                {
                    temp += t;
                }
            }

            msgArr.RemoveAt(0); // remove the keyword from the begining

            if (this.UserData.ContainsKey(msgArr[0]))
            {
                this.UserData[msgArr[0]] = msgArr[1];
            }
            else
            {
                this.UserData.Add(msgArr[0], msgArr[1]);
            }
        }

        #endregion
    }
}