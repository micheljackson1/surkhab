﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Surkhab
{
    public class ProcessTime
    {
        public System.Collections.Concurrent.ConcurrentDictionary<string, object> TimerResults = new System.Collections.Concurrent.ConcurrentDictionary<string, object>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flag"></param>
        /// <param name="order">"Start" or "Stop"</param>
        public void Timer(string flag, string order)
        {
            System.Console.Out.WriteLine("flag: {0} order: {1}", flag, order);
            try
            {
                switch (order)
                {
                    case "Start":
                        UpdateTimerResults(flag, DateTime.Now);
                        break;

                    case "Stop":
                        TimeSpan elapsed = DateTime.Now - (DateTime)TimerResults[flag];
                        UpdateTimerResults(flag, Math.Round(elapsed.TotalMilliseconds, 2));
                        break;
                }
            }
            catch (Exception e)
            {
                MainForm.logger.ErrorException("ProcessTime.Timer", e);
            }
        }

        private void UpdateTimerResults(string flag, object time)
        {
            if (TimerResults.ContainsKey(flag) == true)
            {
                TimerResults[flag] = time;
            }
            else
            {
                TimerResults.GetOrAdd(flag, time);
            }
        }
    }
}
