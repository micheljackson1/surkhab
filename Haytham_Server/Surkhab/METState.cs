﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using AForge.Video.DirectShow;
using AForge.Video;
using System.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace Surkhab
{
    //delegates
    public delegate void TrackerEventHandler(object sender, METEventArg e);
    public delegate void _SendToForm(object message, string controlName);


    public sealed class METState
    {
        private static METState _Current = new METState();
        public static METState Current
        {
            get { return _Current; }
        }

        //Classes
        public METCore METCoreObject = new METCore();
        public Eye eye1 = new Eye();
        public Eye eye2 = new Eye();
        public Server server = new Server();

        //public ProcessTime ProcessTimeEyeBranch = new ProcessTime();

        //

        public enum MonoOrBi { MonocularEyeTracking, BinocularEyeTracking };
        public MonoOrBi monoOrBi;

        //Cameras
        public Surkhab.VideoSource.IVideoSource Eye1Camera;
        public Surkhab.VideoSource.IVideoSource Eye2Camera;

        //Server & Client
        public string ip = "";

        //Others
        public Boolean TimerEnable = true;
    }
}