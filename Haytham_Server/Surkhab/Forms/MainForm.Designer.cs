﻿namespace Surkhab
{
    public partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_Camera = new System.Windows.Forms.TabPage();
            this.gbEye2CameraDevice = new System.Windows.Forms.GroupBox();
            this.cb_eye2_Rotate180 = new System.Windows.Forms.CheckBox();
            this.cb_eye2_VFlip = new System.Windows.Forms.CheckBox();
            this.btnSettingsEye2 = new System.Windows.Forms.Button();
            this.btnStartEye2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbDeviceCapabilityEye2 = new System.Windows.Forms.ComboBox();
            this.cmbDeviceEye2 = new System.Windows.Forms.ComboBox();
            this.gbEye1CameraDevice = new System.Windows.Forms.GroupBox();
            this.cb_eye1_Rotate180 = new System.Windows.Forms.CheckBox();
            this.cb_eye1_VFlip = new System.Windows.Forms.CheckBox();
            this.btnSettingsEye1 = new System.Windows.Forms.Button();
            this.btnStartEye1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDeviceCapabilityEye1 = new System.Windows.Forms.ComboBox();
            this.cmbDeviceEye1 = new System.Windows.Forms.ComboBox();
            this.tabPage_Eye1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cbG1M = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.trackBarG1ABlockSize = new Surkhab.TransparentTrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.rbG1Gaussian = new System.Windows.Forms.RadioButton();
            this.rbG1Mean = new System.Windows.Forms.RadioButton();
            this.cbG1A = new System.Windows.Forms.RadioButton();
            this.trackBarThresholdGlint1 = new Surkhab.TransparentTrackBar();
            this.trackBarG1AConstant = new Surkhab.TransparentTrackBar();
            this.cbShowGlint1 = new System.Windows.Forms.CheckBox();
            this.cbGlint1Detection = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbP1M = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.trackBarP1ABlockSize = new Surkhab.TransparentTrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.rbP1Gaussian = new System.Windows.Forms.RadioButton();
            this.rbP1Mean = new System.Windows.Forms.RadioButton();
            this.cbP1A = new System.Windows.Forms.RadioButton();
            this.cbRemoveGlint1 = new System.Windows.Forms.CheckBox();
            this.cbDilateErode1 = new System.Windows.Forms.CheckBox();
            this.trackBarP1AConstant = new Surkhab.TransparentTrackBar();
            this.trackBarThresholdEye1 = new Surkhab.TransparentTrackBar();
            this.cbShowPupil1 = new System.Windows.Forms.CheckBox();
            this.cbPupil1Detection = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbShowIris1 = new System.Windows.Forms.CheckBox();
            this.trackBarIris1Size = new Surkhab.TransparentTrackBar();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage_Eye2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbG2M = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.trackBarG2ABlockSize = new Surkhab.TransparentTrackBar();
            this.label8 = new System.Windows.Forms.Label();
            this.rbG2Gaussian = new System.Windows.Forms.RadioButton();
            this.rbG2Mean = new System.Windows.Forms.RadioButton();
            this.cbG2A = new System.Windows.Forms.RadioButton();
            this.trackBarThresholdGlint2 = new Surkhab.TransparentTrackBar();
            this.trackBarG2AConstant = new Surkhab.TransparentTrackBar();
            this.cbShowGlint2 = new System.Windows.Forms.CheckBox();
            this.cbGlint2Detection = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbP2M = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.trackBarP2ABlockSize = new Surkhab.TransparentTrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.rbP2Gaussian = new System.Windows.Forms.RadioButton();
            this.rbP2Mean = new System.Windows.Forms.RadioButton();
            this.cbP2A = new System.Windows.Forms.RadioButton();
            this.cbRemoveGlint2 = new System.Windows.Forms.CheckBox();
            this.cbDilateErode2 = new System.Windows.Forms.CheckBox();
            this.trackBarP2AConstant = new Surkhab.TransparentTrackBar();
            this.trackBarThresholdEye2 = new Surkhab.TransparentTrackBar();
            this.cbShowPupil2 = new System.Windows.Forms.CheckBox();
            this.cbPupil2Detection = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbShowIris2 = new System.Windows.Forms.CheckBox();
            this.trackBarIris2Size = new Surkhab.TransparentTrackBar();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage_Clients = new System.Windows.Forms.TabPage();
            this.panelClients = new System.Windows.Forms.GroupBox();
            this.radioButtonAutoActivation = new System.Windows.Forms.RadioButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.TextBoxServer = new System.Windows.Forms.TextBox();
            this.tabPage_Gesture = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.btnDbBlink = new System.Windows.Forms.Button();
            this.btnBlink = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox_imgEye1 = new System.Windows.Forms.GroupBox();
            this.panelDisplayEye1 = new System.Windows.Forms.Panel();
            this.imEye1 = new System.Windows.Forms.PictureBox();
            this.groupBox_imgEye2 = new System.Windows.Forms.GroupBox();
            this.panelDisplayEye2 = new System.Windows.Forms.Panel();
            this.imEye2 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblGlint2Center = new System.Windows.Forms.Label();
            this.lblPupil2Center = new System.Windows.Forms.Label();
            this.comboBox_Eye2Timer = new System.Windows.Forms.ComboBox();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.lblIP = new System.Windows.Forms.TextBox();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.lblGlint1Center = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.lblPupil1Center = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.comboBox_Eye1Timer = new System.Windows.Forms.ComboBox();
            this.timerReset = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage_Camera.SuspendLayout();
            this.gbEye2CameraDevice.SuspendLayout();
            this.gbEye1CameraDevice.SuspendLayout();
            this.tabPage_Eye1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG1ABlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdGlint1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG1AConstant)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP1ABlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP1AConstant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdEye1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIris1Size)).BeginInit();
            this.tabPage_Eye2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG2ABlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdGlint2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG2AConstant)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP2ABlockSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP2AConstant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdEye2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIris2Size)).BeginInit();
            this.tabPage_Clients.SuspendLayout();
            this.panelClients.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage_Gesture.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox_imgEye1.SuspendLayout();
            this.panelDisplayEye1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imEye1)).BeginInit();
            this.groupBox_imgEye2.SuspendLayout();
            this.panelDisplayEye2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imEye2)).BeginInit();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox14);
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Size = new System.Drawing.Size(1268, 668);
            this.splitContainer1.SplitterDistance = 367;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 43;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_Camera);
            this.tabControl1.Controls.Add(this.tabPage_Eye1);
            this.tabControl1.Controls.Add(this.tabPage_Eye2);
            this.tabControl1.Controls.Add(this.tabPage_Clients);
            this.tabControl1.Controls.Add(this.tabPage_Gesture);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(367, 668);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage_Camera
            // 
            this.tabPage_Camera.Controls.Add(this.gbEye2CameraDevice);
            this.tabPage_Camera.Controls.Add(this.gbEye1CameraDevice);
            this.tabPage_Camera.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Camera.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage_Camera.Name = "tabPage_Camera";
            this.tabPage_Camera.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage_Camera.Size = new System.Drawing.Size(359, 642);
            this.tabPage_Camera.TabIndex = 0;
            this.tabPage_Camera.Text = "Camera";
            this.tabPage_Camera.UseVisualStyleBackColor = true;
            // 
            // gbEye2CameraDevice
            // 
            this.gbEye2CameraDevice.Controls.Add(this.cb_eye2_Rotate180);
            this.gbEye2CameraDevice.Controls.Add(this.cb_eye2_VFlip);
            this.gbEye2CameraDevice.Controls.Add(this.btnSettingsEye2);
            this.gbEye2CameraDevice.Controls.Add(this.btnStartEye2);
            this.gbEye2CameraDevice.Controls.Add(this.label6);
            this.gbEye2CameraDevice.Controls.Add(this.label7);
            this.gbEye2CameraDevice.Controls.Add(this.cmbDeviceCapabilityEye2);
            this.gbEye2CameraDevice.Controls.Add(this.cmbDeviceEye2);
            this.gbEye2CameraDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbEye2CameraDevice.Location = new System.Drawing.Point(2, 168);
            this.gbEye2CameraDevice.Margin = new System.Windows.Forms.Padding(2);
            this.gbEye2CameraDevice.Name = "gbEye2CameraDevice";
            this.gbEye2CameraDevice.Padding = new System.Windows.Forms.Padding(2);
            this.gbEye2CameraDevice.Size = new System.Drawing.Size(355, 166);
            this.gbEye2CameraDevice.TabIndex = 57;
            this.gbEye2CameraDevice.TabStop = false;
            this.gbEye2CameraDevice.Text = "Eye2 Camera";
            // 
            // cb_eye2_Rotate180
            // 
            this.cb_eye2_Rotate180.AutoSize = true;
            this.cb_eye2_Rotate180.Checked = true;
            this.cb_eye2_Rotate180.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_eye2_Rotate180.Location = new System.Drawing.Point(97, 133);
            this.cb_eye2_Rotate180.Margin = new System.Windows.Forms.Padding(2);
            this.cb_eye2_Rotate180.Name = "cb_eye2_Rotate180";
            this.cb_eye2_Rotate180.Size = new System.Drawing.Size(83, 17);
            this.cb_eye2_Rotate180.TabIndex = 59;
            this.cb_eye2_Rotate180.Text = "Rotate 180°";
            this.cb_eye2_Rotate180.UseVisualStyleBackColor = true;
            this.cb_eye2_Rotate180.CheckedChanged += new System.EventHandler(this.cb_eye2_Rotate180_CheckedChanged);
            // 
            // cb_eye2_VFlip
            // 
            this.cb_eye2_VFlip.AutoSize = true;
            this.cb_eye2_VFlip.Location = new System.Drawing.Point(8, 133);
            this.cb_eye2_VFlip.Margin = new System.Windows.Forms.Padding(2);
            this.cb_eye2_VFlip.Name = "cb_eye2_VFlip";
            this.cb_eye2_VFlip.Size = new System.Drawing.Size(80, 17);
            this.cb_eye2_VFlip.TabIndex = 57;
            this.cb_eye2_VFlip.Text = "Flip Vertical";
            this.cb_eye2_VFlip.UseVisualStyleBackColor = true;
            this.cb_eye2_VFlip.CheckedChanged += new System.EventHandler(this.cb_eye2_VFlip_CheckedChanged);
            // 
            // btnSettingsEye2
            // 
            this.btnSettingsEye2.Location = new System.Drawing.Point(8, 84);
            this.btnSettingsEye2.Margin = new System.Windows.Forms.Padding(2);
            this.btnSettingsEye2.Name = "btnSettingsEye2";
            this.btnSettingsEye2.Size = new System.Drawing.Size(84, 37);
            this.btnSettingsEye2.TabIndex = 10;
            this.btnSettingsEye2.Text = "Settings";
            this.btnSettingsEye2.UseVisualStyleBackColor = true;
            this.btnSettingsEye2.Click += new System.EventHandler(this.btnSettingsEye2_Click);
            // 
            // btnStartEye2
            // 
            this.btnStartEye2.Location = new System.Drawing.Point(97, 84);
            this.btnStartEye2.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartEye2.Name = "btnStartEye2";
            this.btnStartEye2.Size = new System.Drawing.Size(115, 37);
            this.btnStartEye2.TabIndex = 9;
            this.btnStartEye2.Text = "Start";
            this.btnStartEye2.UseVisualStyleBackColor = true;
            this.btnStartEye2.Click += new System.EventHandler(this.btnStartEye2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Resolution";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Camera";
            // 
            // cmbDeviceCapabilityEye2
            // 
            this.cmbDeviceCapabilityEye2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeviceCapabilityEye2.FormattingEnabled = true;
            this.cmbDeviceCapabilityEye2.Location = new System.Drawing.Point(61, 50);
            this.cmbDeviceCapabilityEye2.Name = "cmbDeviceCapabilityEye2";
            this.cmbDeviceCapabilityEye2.Size = new System.Drawing.Size(287, 21);
            this.cmbDeviceCapabilityEye2.TabIndex = 8;
            // 
            // cmbDeviceEye2
            // 
            this.cmbDeviceEye2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeviceEye2.FormattingEnabled = true;
            this.cmbDeviceEye2.Location = new System.Drawing.Point(50, 24);
            this.cmbDeviceEye2.Name = "cmbDeviceEye2";
            this.cmbDeviceEye2.Size = new System.Drawing.Size(297, 21);
            this.cmbDeviceEye2.TabIndex = 6;
            this.cmbDeviceEye2.SelectedIndexChanged += new System.EventHandler(this.cmbDeviceEye2_SelectedIndexChanged);
            // 
            // gbEye1CameraDevice
            // 
            this.gbEye1CameraDevice.Controls.Add(this.cb_eye1_Rotate180);
            this.gbEye1CameraDevice.Controls.Add(this.cb_eye1_VFlip);
            this.gbEye1CameraDevice.Controls.Add(this.btnSettingsEye1);
            this.gbEye1CameraDevice.Controls.Add(this.btnStartEye1);
            this.gbEye1CameraDevice.Controls.Add(this.label5);
            this.gbEye1CameraDevice.Controls.Add(this.label1);
            this.gbEye1CameraDevice.Controls.Add(this.cmbDeviceCapabilityEye1);
            this.gbEye1CameraDevice.Controls.Add(this.cmbDeviceEye1);
            this.gbEye1CameraDevice.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbEye1CameraDevice.Location = new System.Drawing.Point(2, 2);
            this.gbEye1CameraDevice.Margin = new System.Windows.Forms.Padding(2);
            this.gbEye1CameraDevice.Name = "gbEye1CameraDevice";
            this.gbEye1CameraDevice.Padding = new System.Windows.Forms.Padding(2);
            this.gbEye1CameraDevice.Size = new System.Drawing.Size(355, 166);
            this.gbEye1CameraDevice.TabIndex = 51;
            this.gbEye1CameraDevice.TabStop = false;
            this.gbEye1CameraDevice.Text = "Eye1 Camera";
            // 
            // cb_eye1_Rotate180
            // 
            this.cb_eye1_Rotate180.AutoSize = true;
            this.cb_eye1_Rotate180.Location = new System.Drawing.Point(97, 133);
            this.cb_eye1_Rotate180.Margin = new System.Windows.Forms.Padding(2);
            this.cb_eye1_Rotate180.Name = "cb_eye1_Rotate180";
            this.cb_eye1_Rotate180.Size = new System.Drawing.Size(83, 17);
            this.cb_eye1_Rotate180.TabIndex = 58;
            this.cb_eye1_Rotate180.Text = "Rotate 180°";
            this.cb_eye1_Rotate180.UseVisualStyleBackColor = true;
            this.cb_eye1_Rotate180.CheckedChanged += new System.EventHandler(this.cb_eye1_Rotate180_CheckedChanged);
            // 
            // cb_eye1_VFlip
            // 
            this.cb_eye1_VFlip.AutoSize = true;
            this.cb_eye1_VFlip.Location = new System.Drawing.Point(8, 133);
            this.cb_eye1_VFlip.Margin = new System.Windows.Forms.Padding(2);
            this.cb_eye1_VFlip.Name = "cb_eye1_VFlip";
            this.cb_eye1_VFlip.Size = new System.Drawing.Size(80, 17);
            this.cb_eye1_VFlip.TabIndex = 57;
            this.cb_eye1_VFlip.Text = "Flip Vertical";
            this.cb_eye1_VFlip.UseVisualStyleBackColor = true;
            this.cb_eye1_VFlip.CheckedChanged += new System.EventHandler(this.cb_eye1_VFlip_CheckedChanged);
            // 
            // btnSettingsEye1
            // 
            this.btnSettingsEye1.Location = new System.Drawing.Point(8, 84);
            this.btnSettingsEye1.Margin = new System.Windows.Forms.Padding(2);
            this.btnSettingsEye1.Name = "btnSettingsEye1";
            this.btnSettingsEye1.Size = new System.Drawing.Size(84, 37);
            this.btnSettingsEye1.TabIndex = 10;
            this.btnSettingsEye1.Text = "Settings";
            this.btnSettingsEye1.UseVisualStyleBackColor = true;
            this.btnSettingsEye1.Click += new System.EventHandler(this.btnSettingsEye1Click);
            // 
            // btnStartEye1
            // 
            this.btnStartEye1.Location = new System.Drawing.Point(97, 84);
            this.btnStartEye1.Margin = new System.Windows.Forms.Padding(2);
            this.btnStartEye1.Name = "btnStartEye1";
            this.btnStartEye1.Size = new System.Drawing.Size(115, 37);
            this.btnStartEye1.TabIndex = 9;
            this.btnStartEye1.Text = "Start";
            this.btnStartEye1.UseVisualStyleBackColor = true;
            this.btnStartEye1.Click += new System.EventHandler(this.btnStartEye1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Resolution";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Camera";
            // 
            // cmbDeviceCapabilityEye1
            // 
            this.cmbDeviceCapabilityEye1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeviceCapabilityEye1.FormattingEnabled = true;
            this.cmbDeviceCapabilityEye1.Location = new System.Drawing.Point(61, 50);
            this.cmbDeviceCapabilityEye1.Name = "cmbDeviceCapabilityEye1";
            this.cmbDeviceCapabilityEye1.Size = new System.Drawing.Size(287, 21);
            this.cmbDeviceCapabilityEye1.TabIndex = 8;
            // 
            // cmbDeviceEye1
            // 
            this.cmbDeviceEye1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeviceEye1.FormattingEnabled = true;
            this.cmbDeviceEye1.Location = new System.Drawing.Point(50, 24);
            this.cmbDeviceEye1.Name = "cmbDeviceEye1";
            this.cmbDeviceEye1.Size = new System.Drawing.Size(297, 21);
            this.cmbDeviceEye1.TabIndex = 6;
            this.cmbDeviceEye1.SelectedIndexChanged += new System.EventHandler(this.cmbDeviceEye1_SelectedIndexChanged);
            // 
            // tabPage_Eye1
            // 
            this.tabPage_Eye1.Controls.Add(this.groupBox5);
            this.tabPage_Eye1.Controls.Add(this.groupBox6);
            this.tabPage_Eye1.Controls.Add(this.groupBox4);
            this.tabPage_Eye1.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Eye1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage_Eye1.Name = "tabPage_Eye1";
            this.tabPage_Eye1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage_Eye1.Size = new System.Drawing.Size(359, 642);
            this.tabPage_Eye1.TabIndex = 1;
            this.tabPage_Eye1.Text = "Eye 1";
            this.tabPage_Eye1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cbG1M);
            this.groupBox5.Controls.Add(this.panel4);
            this.groupBox5.Controls.Add(this.cbG1A);
            this.groupBox5.Controls.Add(this.trackBarThresholdGlint1);
            this.groupBox5.Controls.Add(this.trackBarG1AConstant);
            this.groupBox5.Controls.Add(this.cbShowGlint1);
            this.groupBox5.Controls.Add(this.cbGlint1Detection);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(2, 248);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(355, 120);
            this.groupBox5.TabIndex = 60;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Glint Detection";
            // 
            // cbG1M
            // 
            this.cbG1M.AutoSize = true;
            this.cbG1M.Location = new System.Drawing.Point(18, 67);
            this.cbG1M.Margin = new System.Windows.Forms.Padding(2);
            this.cbG1M.Name = "cbG1M";
            this.cbG1M.Size = new System.Drawing.Size(60, 17);
            this.cbG1M.TabIndex = 59;
            this.cbG1M.Text = "Manual";
            this.cbG1M.UseVisualStyleBackColor = true;
            this.cbG1M.CheckedChanged += new System.EventHandler(this.cbG1M_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.trackBarG1ABlockSize);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.rbG1Gaussian);
            this.panel4.Controls.Add(this.rbG1Mean);
            this.panel4.Location = new System.Drawing.Point(9, 141);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(202, 112);
            this.panel4.TabIndex = 69;
            this.panel4.Visible = false;
            // 
            // trackBarG1ABlockSize
            // 
            this.trackBarG1ABlockSize.AutoSize = false;
            this.trackBarG1ABlockSize.Location = new System.Drawing.Point(57, 32);
            this.trackBarG1ABlockSize.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarG1ABlockSize.Maximum = 151;
            this.trackBarG1ABlockSize.Minimum = 33;
            this.trackBarG1ABlockSize.Name = "trackBarG1ABlockSize";
            this.trackBarG1ABlockSize.Size = new System.Drawing.Size(146, 24);
            this.trackBarG1ABlockSize.TabIndex = 65;
            this.trackBarG1ABlockSize.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarG1ABlockSize.Value = 113;
            this.trackBarG1ABlockSize.ValueChanged += new System.EventHandler(this.trackBarG1ABlockSize_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 32);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 64;
            this.label11.Text = "blockSize";
            // 
            // rbG1Gaussian
            // 
            this.rbG1Gaussian.AutoSize = true;
            this.rbG1Gaussian.Location = new System.Drawing.Point(112, 11);
            this.rbG1Gaussian.Margin = new System.Windows.Forms.Padding(2);
            this.rbG1Gaussian.Name = "rbG1Gaussian";
            this.rbG1Gaussian.Size = new System.Drawing.Size(69, 17);
            this.rbG1Gaussian.TabIndex = 61;
            this.rbG1Gaussian.Text = "Gaussian";
            this.rbG1Gaussian.UseVisualStyleBackColor = true;
            this.rbG1Gaussian.CheckedChanged += new System.EventHandler(this.rbG1Gaussian_CheckedChanged);
            // 
            // rbG1Mean
            // 
            this.rbG1Mean.AutoSize = true;
            this.rbG1Mean.Checked = true;
            this.rbG1Mean.Location = new System.Drawing.Point(35, 11);
            this.rbG1Mean.Margin = new System.Windows.Forms.Padding(2);
            this.rbG1Mean.Name = "rbG1Mean";
            this.rbG1Mean.Size = new System.Drawing.Size(52, 17);
            this.rbG1Mean.TabIndex = 60;
            this.rbG1Mean.TabStop = true;
            this.rbG1Mean.Text = "Mean";
            this.rbG1Mean.UseVisualStyleBackColor = true;
            this.rbG1Mean.CheckedChanged += new System.EventHandler(this.rbG1Mean_CheckedChanged);
            // 
            // cbG1A
            // 
            this.cbG1A.AutoSize = true;
            this.cbG1A.Checked = true;
            this.cbG1A.Location = new System.Drawing.Point(18, 42);
            this.cbG1A.Margin = new System.Windows.Forms.Padding(2);
            this.cbG1A.Name = "cbG1A";
            this.cbG1A.Size = new System.Drawing.Size(47, 17);
            this.cbG1A.TabIndex = 58;
            this.cbG1A.TabStop = true;
            this.cbG1A.Text = "Auto";
            this.cbG1A.UseVisualStyleBackColor = true;
            this.cbG1A.CheckedChanged += new System.EventHandler(this.cbG1A_CheckedChanged);
            // 
            // trackBarThresholdGlint1
            // 
            this.trackBarThresholdGlint1.AutoSize = false;
            this.trackBarThresholdGlint1.Location = new System.Drawing.Point(76, 67);
            this.trackBarThresholdGlint1.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarThresholdGlint1.Maximum = 255;
            this.trackBarThresholdGlint1.Minimum = 120;
            this.trackBarThresholdGlint1.Name = "trackBarThresholdGlint1";
            this.trackBarThresholdGlint1.Size = new System.Drawing.Size(146, 24);
            this.trackBarThresholdGlint1.TabIndex = 67;
            this.trackBarThresholdGlint1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarThresholdGlint1.Value = 200;
            this.trackBarThresholdGlint1.ValueChanged += new System.EventHandler(this.trackBarThresholdGlint1_ValueChanged);
            // 
            // trackBarG1AConstant
            // 
            this.trackBarG1AConstant.AutoSize = false;
            this.trackBarG1AConstant.Location = new System.Drawing.Point(76, 42);
            this.trackBarG1AConstant.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarG1AConstant.Maximum = 0;
            this.trackBarG1AConstant.Minimum = -100;
            this.trackBarG1AConstant.Name = "trackBarG1AConstant";
            this.trackBarG1AConstant.Size = new System.Drawing.Size(146, 24);
            this.trackBarG1AConstant.TabIndex = 68;
            this.trackBarG1AConstant.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarG1AConstant.Value = -100;
            this.trackBarG1AConstant.ValueChanged += new System.EventHandler(this.trackBarG1AConstant_ValueChanged);
            // 
            // cbShowGlint1
            // 
            this.cbShowGlint1.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowGlint1.BackColor = System.Drawing.Color.Yellow;
            this.cbShowGlint1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowGlint1.BackgroundImage")));
            this.cbShowGlint1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowGlint1.Checked = true;
            this.cbShowGlint1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowGlint1.FlatAppearance.BorderSize = 0;
            this.cbShowGlint1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowGlint1.Location = new System.Drawing.Point(195, 0);
            this.cbShowGlint1.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowGlint1.Name = "cbShowGlint1";
            this.cbShowGlint1.Size = new System.Drawing.Size(14, 18);
            this.cbShowGlint1.TabIndex = 57;
            this.cbShowGlint1.UseVisualStyleBackColor = false;
            this.cbShowGlint1.CheckedChanged += new System.EventHandler(this.cbShowGlint1_CheckedChanged);
            // 
            // cbGlint1Detection
            // 
            this.cbGlint1Detection.Checked = true;
            this.cbGlint1Detection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGlint1Detection.Location = new System.Drawing.Point(83, 0);
            this.cbGlint1Detection.Margin = new System.Windows.Forms.Padding(2);
            this.cbGlint1Detection.Name = "cbGlint1Detection";
            this.cbGlint1Detection.Size = new System.Drawing.Size(106, 17);
            this.cbGlint1Detection.TabIndex = 56;
            this.cbGlint1Detection.UseVisualStyleBackColor = true;
            this.cbGlint1Detection.CheckedChanged += new System.EventHandler(this.cbGlint1Detection_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbP1M);
            this.groupBox6.Controls.Add(this.panel3);
            this.groupBox6.Controls.Add(this.cbP1A);
            this.groupBox6.Controls.Add(this.cbRemoveGlint1);
            this.groupBox6.Controls.Add(this.cbDilateErode1);
            this.groupBox6.Controls.Add(this.trackBarP1AConstant);
            this.groupBox6.Controls.Add(this.trackBarThresholdEye1);
            this.groupBox6.Controls.Add(this.cbShowPupil1);
            this.groupBox6.Controls.Add(this.cbPupil1Detection);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(2, 72);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(355, 176);
            this.groupBox6.TabIndex = 60;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pupil Detection";
            // 
            // cbP1M
            // 
            this.cbP1M.AutoSize = true;
            this.cbP1M.Location = new System.Drawing.Point(17, 69);
            this.cbP1M.Margin = new System.Windows.Forms.Padding(2);
            this.cbP1M.Name = "cbP1M";
            this.cbP1M.Size = new System.Drawing.Size(60, 17);
            this.cbP1M.TabIndex = 59;
            this.cbP1M.Text = "Manual";
            this.cbP1M.UseVisualStyleBackColor = true;
            this.cbP1M.CheckedChanged += new System.EventHandler(this.cbP1M_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.trackBarP1ABlockSize);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.rbP1Gaussian);
            this.panel3.Controls.Add(this.rbP1Mean);
            this.panel3.Location = new System.Drawing.Point(7, 190);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(202, 112);
            this.panel3.TabIndex = 67;
            this.panel3.Visible = false;
            // 
            // trackBarP1ABlockSize
            // 
            this.trackBarP1ABlockSize.AutoSize = false;
            this.trackBarP1ABlockSize.Location = new System.Drawing.Point(57, 32);
            this.trackBarP1ABlockSize.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarP1ABlockSize.Maximum = 151;
            this.trackBarP1ABlockSize.Minimum = 33;
            this.trackBarP1ABlockSize.Name = "trackBarP1ABlockSize";
            this.trackBarP1ABlockSize.Size = new System.Drawing.Size(146, 24);
            this.trackBarP1ABlockSize.TabIndex = 65;
            this.trackBarP1ABlockSize.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarP1ABlockSize.Value = 113;
            this.trackBarP1ABlockSize.ValueChanged += new System.EventHandler(this.trackBarP1ABlockSize_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 32);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 64;
            this.label10.Text = "blockSize";
            // 
            // rbP1Gaussian
            // 
            this.rbP1Gaussian.AutoSize = true;
            this.rbP1Gaussian.Location = new System.Drawing.Point(112, 11);
            this.rbP1Gaussian.Margin = new System.Windows.Forms.Padding(2);
            this.rbP1Gaussian.Name = "rbP1Gaussian";
            this.rbP1Gaussian.Size = new System.Drawing.Size(69, 17);
            this.rbP1Gaussian.TabIndex = 61;
            this.rbP1Gaussian.Text = "Gaussian";
            this.rbP1Gaussian.UseVisualStyleBackColor = true;
            this.rbP1Gaussian.CheckedChanged += new System.EventHandler(this.rbP1Gaussian_CheckedChanged);
            // 
            // rbP1Mean
            // 
            this.rbP1Mean.AutoSize = true;
            this.rbP1Mean.Checked = true;
            this.rbP1Mean.Location = new System.Drawing.Point(35, 11);
            this.rbP1Mean.Margin = new System.Windows.Forms.Padding(2);
            this.rbP1Mean.Name = "rbP1Mean";
            this.rbP1Mean.Size = new System.Drawing.Size(52, 17);
            this.rbP1Mean.TabIndex = 60;
            this.rbP1Mean.TabStop = true;
            this.rbP1Mean.Text = "Mean";
            this.rbP1Mean.UseVisualStyleBackColor = true;
            this.rbP1Mean.CheckedChanged += new System.EventHandler(this.rbP1Mean_CheckedChanged);
            // 
            // cbP1A
            // 
            this.cbP1A.AutoSize = true;
            this.cbP1A.Checked = true;
            this.cbP1A.Location = new System.Drawing.Point(18, 44);
            this.cbP1A.Margin = new System.Windows.Forms.Padding(2);
            this.cbP1A.Name = "cbP1A";
            this.cbP1A.Size = new System.Drawing.Size(47, 17);
            this.cbP1A.TabIndex = 58;
            this.cbP1A.TabStop = true;
            this.cbP1A.Text = "Auto";
            this.cbP1A.UseVisualStyleBackColor = true;
            this.cbP1A.CheckedChanged += new System.EventHandler(this.cbP1A_CheckedChanged);
            // 
            // cbRemoveGlint1
            // 
            this.cbRemoveGlint1.Location = new System.Drawing.Point(18, 131);
            this.cbRemoveGlint1.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemoveGlint1.Name = "cbRemoveGlint1";
            this.cbRemoveGlint1.Size = new System.Drawing.Size(148, 17);
            this.cbRemoveGlint1.TabIndex = 66;
            this.cbRemoveGlint1.Text = "Remove Glint";
            this.cbRemoveGlint1.UseVisualStyleBackColor = true;
            this.cbRemoveGlint1.CheckedChanged += new System.EventHandler(this.cbRemoveGlint1_CheckedChanged);
            // 
            // cbDilateErode1
            // 
            this.cbDilateErode1.Location = new System.Drawing.Point(18, 110);
            this.cbDilateErode1.Margin = new System.Windows.Forms.Padding(2);
            this.cbDilateErode1.Name = "cbDilateErode1";
            this.cbDilateErode1.Size = new System.Drawing.Size(148, 17);
            this.cbDilateErode1.TabIndex = 65;
            this.cbDilateErode1.Text = "Fill Gaps";
            this.cbDilateErode1.UseVisualStyleBackColor = true;
            this.cbDilateErode1.CheckedChanged += new System.EventHandler(this.cbDilateErode1_CheckedChanged);
            // 
            // trackBarP1AConstant
            // 
            this.trackBarP1AConstant.AutoSize = false;
            this.trackBarP1AConstant.Location = new System.Drawing.Point(76, 43);
            this.trackBarP1AConstant.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarP1AConstant.Maximum = 50;
            this.trackBarP1AConstant.Minimum = 5;
            this.trackBarP1AConstant.Name = "trackBarP1AConstant";
            this.trackBarP1AConstant.Size = new System.Drawing.Size(146, 24);
            this.trackBarP1AConstant.TabIndex = 64;
            this.trackBarP1AConstant.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarP1AConstant.Value = 15;
            this.trackBarP1AConstant.ValueChanged += new System.EventHandler(this.trackBarP1AConstant_ValueChanged);
            // 
            // trackBarThresholdEye1
            // 
            this.trackBarThresholdEye1.AutoSize = false;
            this.trackBarThresholdEye1.Location = new System.Drawing.Point(76, 70);
            this.trackBarThresholdEye1.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarThresholdEye1.Maximum = 255;
            this.trackBarThresholdEye1.Name = "trackBarThresholdEye1";
            this.trackBarThresholdEye1.Size = new System.Drawing.Size(146, 24);
            this.trackBarThresholdEye1.TabIndex = 63;
            this.trackBarThresholdEye1.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarThresholdEye1.Value = 70;
            this.trackBarThresholdEye1.ValueChanged += new System.EventHandler(this.trackBarThreshold1_ValueChanged);
            // 
            // cbShowPupil1
            // 
            this.cbShowPupil1.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowPupil1.BackColor = System.Drawing.Color.Yellow;
            this.cbShowPupil1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowPupil1.BackgroundImage")));
            this.cbShowPupil1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowPupil1.Checked = true;
            this.cbShowPupil1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowPupil1.FlatAppearance.BorderSize = 0;
            this.cbShowPupil1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowPupil1.Location = new System.Drawing.Point(195, -2);
            this.cbShowPupil1.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowPupil1.Name = "cbShowPupil1";
            this.cbShowPupil1.Size = new System.Drawing.Size(14, 18);
            this.cbShowPupil1.TabIndex = 56;
            this.cbShowPupil1.UseVisualStyleBackColor = false;
            this.cbShowPupil1.CheckedChanged += new System.EventHandler(this.cbShowPupil1_CheckedChanged);
            // 
            // cbPupil1Detection
            // 
            this.cbPupil1Detection.Checked = true;
            this.cbPupil1Detection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPupil1Detection.Location = new System.Drawing.Point(83, -1);
            this.cbPupil1Detection.Margin = new System.Windows.Forms.Padding(2);
            this.cbPupil1Detection.Name = "cbPupil1Detection";
            this.cbPupil1Detection.Size = new System.Drawing.Size(106, 17);
            this.cbPupil1Detection.TabIndex = 55;
            this.cbPupil1Detection.UseVisualStyleBackColor = true;
            this.cbPupil1Detection.CheckedChanged += new System.EventHandler(this.cbPupil1Detection_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbShowIris1);
            this.groupBox4.Controls.Add(this.trackBarIris1Size);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(2, 2);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(355, 70);
            this.groupBox4.TabIndex = 59;
            this.groupBox4.TabStop = false;
            // 
            // cbShowIris1
            // 
            this.cbShowIris1.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowIris1.BackColor = System.Drawing.Color.Yellow;
            this.cbShowIris1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowIris1.BackgroundImage")));
            this.cbShowIris1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowIris1.Checked = true;
            this.cbShowIris1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowIris1.FlatAppearance.BorderSize = 0;
            this.cbShowIris1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowIris1.Location = new System.Drawing.Point(194, 24);
            this.cbShowIris1.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowIris1.Name = "cbShowIris1";
            this.cbShowIris1.Size = new System.Drawing.Size(14, 18);
            this.cbShowIris1.TabIndex = 3;
            this.cbShowIris1.UseVisualStyleBackColor = false;
            this.cbShowIris1.CheckedChanged += new System.EventHandler(this.cbShowIris1_CheckedChanged);
            // 
            // trackBarIris1Size
            // 
            this.trackBarIris1Size.AutoSize = false;
            this.trackBarIris1Size.Location = new System.Drawing.Point(51, 23);
            this.trackBarIris1Size.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarIris1Size.Maximum = 500;
            this.trackBarIris1Size.Minimum = 80;
            this.trackBarIris1Size.Name = "trackBarIris1Size";
            this.trackBarIris1Size.Size = new System.Drawing.Size(146, 27);
            this.trackBarIris1Size.TabIndex = 2;
            this.trackBarIris1Size.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarIris1Size.Value = 200;
            this.trackBarIris1Size.ValueChanged += new System.EventHandler(this.trackBarIris1Size_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 28);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "IrisSize";
            // 
            // tabPage_Eye2
            // 
            this.tabPage_Eye2.Controls.Add(this.groupBox1);
            this.tabPage_Eye2.Controls.Add(this.groupBox2);
            this.tabPage_Eye2.Controls.Add(this.groupBox3);
            this.tabPage_Eye2.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Eye2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage_Eye2.Name = "tabPage_Eye2";
            this.tabPage_Eye2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage_Eye2.Size = new System.Drawing.Size(359, 642);
            this.tabPage_Eye2.TabIndex = 8;
            this.tabPage_Eye2.Text = "Eye 2";
            this.tabPage_Eye2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbG2M);
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Controls.Add(this.cbG2A);
            this.groupBox1.Controls.Add(this.trackBarThresholdGlint2);
            this.groupBox1.Controls.Add(this.trackBarG2AConstant);
            this.groupBox1.Controls.Add(this.cbShowGlint2);
            this.groupBox1.Controls.Add(this.cbGlint2Detection);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(2, 248);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(355, 120);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Glint Detection";
            // 
            // cbG2M
            // 
            this.cbG2M.AutoSize = true;
            this.cbG2M.Location = new System.Drawing.Point(18, 67);
            this.cbG2M.Margin = new System.Windows.Forms.Padding(2);
            this.cbG2M.Name = "cbG2M";
            this.cbG2M.Size = new System.Drawing.Size(60, 17);
            this.cbG2M.TabIndex = 59;
            this.cbG2M.Text = "Manual";
            this.cbG2M.UseVisualStyleBackColor = true;
            this.cbG2M.CheckedChanged += new System.EventHandler(this.cbG2M_CheckedChanged);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.trackBarG2ABlockSize);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.rbG2Gaussian);
            this.panel7.Controls.Add(this.rbG2Mean);
            this.panel7.Location = new System.Drawing.Point(9, 141);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(202, 112);
            this.panel7.TabIndex = 69;
            this.panel7.Visible = false;
            // 
            // trackBarG2ABlockSize
            // 
            this.trackBarG2ABlockSize.AutoSize = false;
            this.trackBarG2ABlockSize.Location = new System.Drawing.Point(57, 32);
            this.trackBarG2ABlockSize.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarG2ABlockSize.Maximum = 151;
            this.trackBarG2ABlockSize.Minimum = 33;
            this.trackBarG2ABlockSize.Name = "trackBarG2ABlockSize";
            this.trackBarG2ABlockSize.Size = new System.Drawing.Size(146, 24);
            this.trackBarG2ABlockSize.TabIndex = 65;
            this.trackBarG2ABlockSize.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarG2ABlockSize.Value = 113;
            this.trackBarG2ABlockSize.ValueChanged += new System.EventHandler(this.trackBarG2ABlockSize_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 32);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 64;
            this.label8.Text = "blockSize";
            // 
            // rbG2Gaussian
            // 
            this.rbG2Gaussian.AutoSize = true;
            this.rbG2Gaussian.Location = new System.Drawing.Point(112, 11);
            this.rbG2Gaussian.Margin = new System.Windows.Forms.Padding(2);
            this.rbG2Gaussian.Name = "rbG2Gaussian";
            this.rbG2Gaussian.Size = new System.Drawing.Size(69, 17);
            this.rbG2Gaussian.TabIndex = 61;
            this.rbG2Gaussian.Text = "Gaussian";
            this.rbG2Gaussian.UseVisualStyleBackColor = true;
            this.rbG2Gaussian.CheckedChanged += new System.EventHandler(this.rbG2Gaussian_CheckedChanged);
            // 
            // rbG2Mean
            // 
            this.rbG2Mean.AutoSize = true;
            this.rbG2Mean.Checked = true;
            this.rbG2Mean.Location = new System.Drawing.Point(35, 11);
            this.rbG2Mean.Margin = new System.Windows.Forms.Padding(2);
            this.rbG2Mean.Name = "rbG2Mean";
            this.rbG2Mean.Size = new System.Drawing.Size(52, 17);
            this.rbG2Mean.TabIndex = 60;
            this.rbG2Mean.TabStop = true;
            this.rbG2Mean.Text = "Mean";
            this.rbG2Mean.UseVisualStyleBackColor = true;
            this.rbG2Mean.CheckedChanged += new System.EventHandler(this.rbG2Mean_CheckedChanged);
            // 
            // cbG2A
            // 
            this.cbG2A.AutoSize = true;
            this.cbG2A.Checked = true;
            this.cbG2A.Location = new System.Drawing.Point(18, 42);
            this.cbG2A.Margin = new System.Windows.Forms.Padding(2);
            this.cbG2A.Name = "cbG2A";
            this.cbG2A.Size = new System.Drawing.Size(47, 17);
            this.cbG2A.TabIndex = 58;
            this.cbG2A.TabStop = true;
            this.cbG2A.Text = "Auto";
            this.cbG2A.UseVisualStyleBackColor = true;
            this.cbG2A.CheckedChanged += new System.EventHandler(this.cbG2A_CheckedChanged);
            // 
            // trackBarThresholdGlint2
            // 
            this.trackBarThresholdGlint2.AutoSize = false;
            this.trackBarThresholdGlint2.Location = new System.Drawing.Point(76, 67);
            this.trackBarThresholdGlint2.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarThresholdGlint2.Maximum = 255;
            this.trackBarThresholdGlint2.Minimum = 120;
            this.trackBarThresholdGlint2.Name = "trackBarThresholdGlint2";
            this.trackBarThresholdGlint2.Size = new System.Drawing.Size(146, 24);
            this.trackBarThresholdGlint2.TabIndex = 67;
            this.trackBarThresholdGlint2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarThresholdGlint2.Value = 200;
            this.trackBarThresholdGlint2.ValueChanged += new System.EventHandler(this.trackBarThresholdGlint2_ValueChanged);
            // 
            // trackBarG2AConstant
            // 
            this.trackBarG2AConstant.AutoSize = false;
            this.trackBarG2AConstant.Location = new System.Drawing.Point(76, 42);
            this.trackBarG2AConstant.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarG2AConstant.Maximum = 0;
            this.trackBarG2AConstant.Minimum = -100;
            this.trackBarG2AConstant.Name = "trackBarG2AConstant";
            this.trackBarG2AConstant.Size = new System.Drawing.Size(146, 24);
            this.trackBarG2AConstant.TabIndex = 68;
            this.trackBarG2AConstant.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarG2AConstant.Value = -100;
            this.trackBarG2AConstant.ValueChanged += new System.EventHandler(this.trackBarG2AConstant_ValueChanged);
            // 
            // cbShowGlint2
            // 
            this.cbShowGlint2.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowGlint2.BackColor = System.Drawing.Color.Yellow;
            this.cbShowGlint2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowGlint2.BackgroundImage")));
            this.cbShowGlint2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowGlint2.Checked = true;
            this.cbShowGlint2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowGlint2.FlatAppearance.BorderSize = 0;
            this.cbShowGlint2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowGlint2.Location = new System.Drawing.Point(195, 0);
            this.cbShowGlint2.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowGlint2.Name = "cbShowGlint2";
            this.cbShowGlint2.Size = new System.Drawing.Size(14, 18);
            this.cbShowGlint2.TabIndex = 57;
            this.cbShowGlint2.UseVisualStyleBackColor = false;
            this.cbShowGlint2.CheckedChanged += new System.EventHandler(this.cbShowGlint2_CheckedChanged);
            // 
            // cbGlint2Detection
            // 
            this.cbGlint2Detection.Checked = true;
            this.cbGlint2Detection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGlint2Detection.Location = new System.Drawing.Point(83, 0);
            this.cbGlint2Detection.Margin = new System.Windows.Forms.Padding(2);
            this.cbGlint2Detection.Name = "cbGlint2Detection";
            this.cbGlint2Detection.Size = new System.Drawing.Size(106, 17);
            this.cbGlint2Detection.TabIndex = 56;
            this.cbGlint2Detection.UseVisualStyleBackColor = true;
            this.cbGlint2Detection.CheckedChanged += new System.EventHandler(this.cbGlint2Detection_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbP2M);
            this.groupBox2.Controls.Add(this.panel8);
            this.groupBox2.Controls.Add(this.cbP2A);
            this.groupBox2.Controls.Add(this.cbRemoveGlint2);
            this.groupBox2.Controls.Add(this.cbDilateErode2);
            this.groupBox2.Controls.Add(this.trackBarP2AConstant);
            this.groupBox2.Controls.Add(this.trackBarThresholdEye2);
            this.groupBox2.Controls.Add(this.cbShowPupil2);
            this.groupBox2.Controls.Add(this.cbPupil2Detection);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(2, 72);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(355, 176);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pupil Detection";
            // 
            // cbP2M
            // 
            this.cbP2M.AutoSize = true;
            this.cbP2M.Location = new System.Drawing.Point(17, 69);
            this.cbP2M.Margin = new System.Windows.Forms.Padding(2);
            this.cbP2M.Name = "cbP2M";
            this.cbP2M.Size = new System.Drawing.Size(60, 17);
            this.cbP2M.TabIndex = 59;
            this.cbP2M.Text = "Manual";
            this.cbP2M.UseVisualStyleBackColor = true;
            this.cbP2M.CheckedChanged += new System.EventHandler(this.cbP2M_CheckedChanged);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.trackBarP2ABlockSize);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.rbP2Gaussian);
            this.panel8.Controls.Add(this.rbP2Mean);
            this.panel8.Location = new System.Drawing.Point(7, 190);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(202, 112);
            this.panel8.TabIndex = 67;
            this.panel8.Visible = false;
            // 
            // trackBarP2ABlockSize
            // 
            this.trackBarP2ABlockSize.AutoSize = false;
            this.trackBarP2ABlockSize.Location = new System.Drawing.Point(57, 32);
            this.trackBarP2ABlockSize.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarP2ABlockSize.Maximum = 151;
            this.trackBarP2ABlockSize.Minimum = 33;
            this.trackBarP2ABlockSize.Name = "trackBarP2ABlockSize";
            this.trackBarP2ABlockSize.Size = new System.Drawing.Size(146, 24);
            this.trackBarP2ABlockSize.TabIndex = 65;
            this.trackBarP2ABlockSize.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarP2ABlockSize.Value = 113;
            this.trackBarP2ABlockSize.ValueChanged += new System.EventHandler(this.trackBarP2ABlockSize_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 32);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 64;
            this.label12.Text = "blockSize";
            // 
            // rbP2Gaussian
            // 
            this.rbP2Gaussian.AutoSize = true;
            this.rbP2Gaussian.Location = new System.Drawing.Point(112, 11);
            this.rbP2Gaussian.Margin = new System.Windows.Forms.Padding(2);
            this.rbP2Gaussian.Name = "rbP2Gaussian";
            this.rbP2Gaussian.Size = new System.Drawing.Size(69, 17);
            this.rbP2Gaussian.TabIndex = 61;
            this.rbP2Gaussian.Text = "Gaussian";
            this.rbP2Gaussian.UseVisualStyleBackColor = true;
            this.rbP2Gaussian.CheckedChanged += new System.EventHandler(this.rbP2Gaussian_CheckedChanged);
            // 
            // rbP2Mean
            // 
            this.rbP2Mean.AutoSize = true;
            this.rbP2Mean.Checked = true;
            this.rbP2Mean.Location = new System.Drawing.Point(35, 11);
            this.rbP2Mean.Margin = new System.Windows.Forms.Padding(2);
            this.rbP2Mean.Name = "rbP2Mean";
            this.rbP2Mean.Size = new System.Drawing.Size(52, 17);
            this.rbP2Mean.TabIndex = 60;
            this.rbP2Mean.TabStop = true;
            this.rbP2Mean.Text = "Mean";
            this.rbP2Mean.UseVisualStyleBackColor = true;
            this.rbP2Mean.CheckedChanged += new System.EventHandler(this.rbP2Mean_CheckedChanged);
            // 
            // cbP2A
            // 
            this.cbP2A.AutoSize = true;
            this.cbP2A.Checked = true;
            this.cbP2A.Location = new System.Drawing.Point(18, 44);
            this.cbP2A.Margin = new System.Windows.Forms.Padding(2);
            this.cbP2A.Name = "cbP2A";
            this.cbP2A.Size = new System.Drawing.Size(47, 17);
            this.cbP2A.TabIndex = 58;
            this.cbP2A.TabStop = true;
            this.cbP2A.Text = "Auto";
            this.cbP2A.UseVisualStyleBackColor = true;
            this.cbP2A.CheckedChanged += new System.EventHandler(this.cbP2A_CheckedChanged);
            // 
            // cbRemoveGlint2
            // 
            this.cbRemoveGlint2.Location = new System.Drawing.Point(18, 131);
            this.cbRemoveGlint2.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemoveGlint2.Name = "cbRemoveGlint2";
            this.cbRemoveGlint2.Size = new System.Drawing.Size(148, 17);
            this.cbRemoveGlint2.TabIndex = 66;
            this.cbRemoveGlint2.Text = "Remove Glint";
            this.cbRemoveGlint2.UseVisualStyleBackColor = true;
            this.cbRemoveGlint2.CheckedChanged += new System.EventHandler(this.cbRemoveGlint2_CheckedChanged);
            // 
            // cbDilateErode2
            // 
            this.cbDilateErode2.Location = new System.Drawing.Point(18, 110);
            this.cbDilateErode2.Margin = new System.Windows.Forms.Padding(2);
            this.cbDilateErode2.Name = "cbDilateErode2";
            this.cbDilateErode2.Size = new System.Drawing.Size(148, 17);
            this.cbDilateErode2.TabIndex = 65;
            this.cbDilateErode2.Text = "Fill Gaps";
            this.cbDilateErode2.UseVisualStyleBackColor = true;
            this.cbDilateErode2.CheckedChanged += new System.EventHandler(this.cbDilateErode2_CheckedChanged);
            // 
            // trackBarP2AConstant
            // 
            this.trackBarP2AConstant.AutoSize = false;
            this.trackBarP2AConstant.Location = new System.Drawing.Point(76, 43);
            this.trackBarP2AConstant.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarP2AConstant.Maximum = 50;
            this.trackBarP2AConstant.Minimum = 5;
            this.trackBarP2AConstant.Name = "trackBarP2AConstant";
            this.trackBarP2AConstant.Size = new System.Drawing.Size(146, 24);
            this.trackBarP2AConstant.TabIndex = 64;
            this.trackBarP2AConstant.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarP2AConstant.Value = 15;
            this.trackBarP2AConstant.ValueChanged += new System.EventHandler(this.trackBarP2AConstant_ValueChanged);
            // 
            // trackBarThresholdEye2
            // 
            this.trackBarThresholdEye2.AutoSize = false;
            this.trackBarThresholdEye2.Location = new System.Drawing.Point(76, 70);
            this.trackBarThresholdEye2.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarThresholdEye2.Maximum = 255;
            this.trackBarThresholdEye2.Name = "trackBarThresholdEye2";
            this.trackBarThresholdEye2.Size = new System.Drawing.Size(146, 24);
            this.trackBarThresholdEye2.TabIndex = 63;
            this.trackBarThresholdEye2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarThresholdEye2.Value = 70;
            this.trackBarThresholdEye2.ValueChanged += new System.EventHandler(this.trackBarThreshold2_ValueChanged);
            // 
            // cbShowPupil2
            // 
            this.cbShowPupil2.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowPupil2.BackColor = System.Drawing.Color.Yellow;
            this.cbShowPupil2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowPupil2.BackgroundImage")));
            this.cbShowPupil2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowPupil2.Checked = true;
            this.cbShowPupil2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowPupil2.FlatAppearance.BorderSize = 0;
            this.cbShowPupil2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowPupil2.Location = new System.Drawing.Point(195, -2);
            this.cbShowPupil2.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowPupil2.Name = "cbShowPupil2";
            this.cbShowPupil2.Size = new System.Drawing.Size(14, 18);
            this.cbShowPupil2.TabIndex = 56;
            this.cbShowPupil2.UseVisualStyleBackColor = false;
            this.cbShowPupil2.CheckedChanged += new System.EventHandler(this.cbShowPupil2_CheckedChanged);
            // 
            // cbPupil2Detection
            // 
            this.cbPupil2Detection.Checked = true;
            this.cbPupil2Detection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPupil2Detection.Location = new System.Drawing.Point(83, -1);
            this.cbPupil2Detection.Margin = new System.Windows.Forms.Padding(2);
            this.cbPupil2Detection.Name = "cbPupil2Detection";
            this.cbPupil2Detection.Size = new System.Drawing.Size(106, 17);
            this.cbPupil2Detection.TabIndex = 55;
            this.cbPupil2Detection.UseVisualStyleBackColor = true;
            this.cbPupil2Detection.CheckedChanged += new System.EventHandler(this.cbPupil2Detection_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbShowIris2);
            this.groupBox3.Controls.Add(this.trackBarIris2Size);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(2, 2);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(355, 70);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            // 
            // cbShowIris2
            // 
            this.cbShowIris2.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbShowIris2.BackColor = System.Drawing.Color.Yellow;
            this.cbShowIris2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cbShowIris2.BackgroundImage")));
            this.cbShowIris2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cbShowIris2.Checked = true;
            this.cbShowIris2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowIris2.FlatAppearance.BorderSize = 0;
            this.cbShowIris2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbShowIris2.Location = new System.Drawing.Point(194, 24);
            this.cbShowIris2.Margin = new System.Windows.Forms.Padding(2);
            this.cbShowIris2.Name = "cbShowIris2";
            this.cbShowIris2.Size = new System.Drawing.Size(14, 18);
            this.cbShowIris2.TabIndex = 3;
            this.cbShowIris2.UseVisualStyleBackColor = false;
            this.cbShowIris2.CheckedChanged += new System.EventHandler(this.cbShowIris2_CheckedChanged);
            // 
            // trackBarIris2Size
            // 
            this.trackBarIris2Size.AutoSize = false;
            this.trackBarIris2Size.Location = new System.Drawing.Point(51, 23);
            this.trackBarIris2Size.Margin = new System.Windows.Forms.Padding(2);
            this.trackBarIris2Size.Maximum = 500;
            this.trackBarIris2Size.Minimum = 80;
            this.trackBarIris2Size.Name = "trackBarIris2Size";
            this.trackBarIris2Size.Size = new System.Drawing.Size(146, 27);
            this.trackBarIris2Size.TabIndex = 2;
            this.trackBarIris2Size.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarIris2Size.Value = 200;
            this.trackBarIris2Size.ValueChanged += new System.EventHandler(this.trackBarIris2Size_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 28);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "IrisSize";
            // 
            // tabPage_Clients
            // 
            this.tabPage_Clients.Controls.Add(this.panelClients);
            this.tabPage_Clients.Controls.Add(this.groupBox12);
            this.tabPage_Clients.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Clients.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage_Clients.Name = "tabPage_Clients";
            this.tabPage_Clients.Size = new System.Drawing.Size(359, 642);
            this.tabPage_Clients.TabIndex = 5;
            this.tabPage_Clients.Text = "Clients";
            this.tabPage_Clients.UseVisualStyleBackColor = true;
            // 
            // panelClients
            // 
            this.panelClients.AutoSize = true;
            this.panelClients.Controls.Add(this.radioButtonAutoActivation);
            this.panelClients.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelClients.Location = new System.Drawing.Point(0, 230);
            this.panelClients.Margin = new System.Windows.Forms.Padding(2);
            this.panelClients.Name = "panelClients";
            this.panelClients.Padding = new System.Windows.Forms.Padding(2);
            this.panelClients.Size = new System.Drawing.Size(359, 53);
            this.panelClients.TabIndex = 64;
            this.panelClients.TabStop = false;
            this.panelClients.Text = "Clients";
            // 
            // radioButtonAutoActivation
            // 
            this.radioButtonAutoActivation.AutoSize = true;
            this.radioButtonAutoActivation.Checked = true;
            this.radioButtonAutoActivation.Location = new System.Drawing.Point(132, 18);
            this.radioButtonAutoActivation.Name = "radioButtonAutoActivation";
            this.radioButtonAutoActivation.Size = new System.Drawing.Size(97, 17);
            this.radioButtonAutoActivation.TabIndex = 0;
            this.radioButtonAutoActivation.TabStop = true;
            this.radioButtonAutoActivation.Text = "Auto Activation";
            this.radioButtonAutoActivation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButtonAutoActivation.UseVisualStyleBackColor = true;
            this.radioButtonAutoActivation.CheckedChanged += new System.EventHandler(this.radioButtonAutoActivation_CheckedChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.TextBoxServer);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox12.Location = new System.Drawing.Point(0, 0);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox12.Size = new System.Drawing.Size(359, 230);
            this.groupBox12.TabIndex = 63;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Messages ";
            // 
            // TextBoxServer
            // 
            this.TextBoxServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBoxServer.Location = new System.Drawing.Point(2, 15);
            this.TextBoxServer.Multiline = true;
            this.TextBoxServer.Name = "TextBoxServer";
            this.TextBoxServer.ReadOnly = true;
            this.TextBoxServer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxServer.Size = new System.Drawing.Size(355, 213);
            this.TextBoxServer.TabIndex = 38;
            // 
            // tabPage_Gesture
            // 
            this.tabPage_Gesture.Controls.Add(this.groupBox17);
            this.tabPage_Gesture.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Gesture.Name = "tabPage_Gesture";
            this.tabPage_Gesture.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Gesture.Size = new System.Drawing.Size(359, 642);
            this.tabPage_Gesture.TabIndex = 6;
            this.tabPage_Gesture.Text = "Gestures";
            this.tabPage_Gesture.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.btnDbBlink);
            this.groupBox17.Controls.Add(this.btnBlink);
            this.groupBox17.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox17.Location = new System.Drawing.Point(3, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(353, 63);
            this.groupBox17.TabIndex = 3;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Blink";
            // 
            // btnDbBlink
            // 
            this.btnDbBlink.BackColor = System.Drawing.Color.White;
            this.btnDbBlink.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDbBlink.Location = new System.Drawing.Point(115, 15);
            this.btnDbBlink.Name = "btnDbBlink";
            this.btnDbBlink.Size = new System.Drawing.Size(84, 37);
            this.btnDbBlink.TabIndex = 68;
            this.btnDbBlink.Tag = "DbBlink";
            this.btnDbBlink.Text = "DbBlink";
            this.btnDbBlink.UseVisualStyleBackColor = false;
            // 
            // btnBlink
            // 
            this.btnBlink.BackColor = System.Drawing.Color.White;
            this.btnBlink.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnBlink.Location = new System.Drawing.Point(29, 15);
            this.btnBlink.Name = "btnBlink";
            this.btnBlink.Size = new System.Drawing.Size(84, 37);
            this.btnBlink.TabIndex = 68;
            this.btnBlink.Tag = "Blink";
            this.btnBlink.Text = "Blink";
            this.btnBlink.UseVisualStyleBackColor = false;
            // 
            // groupBox14
            // 
            this.groupBox14.AutoSize = true;
            this.groupBox14.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox14.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox14.Controls.Add(this.tableLayoutPanel1);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox14.Location = new System.Drawing.Point(0, 24);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox14.Size = new System.Drawing.Size(898, 644);
            this.groupBox14.TabIndex = 1;
            this.groupBox14.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.tableLayoutPanel1.Controls.Add(this.splitContainer2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 15);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(894, 627);
            this.tableLayoutPanel1.TabIndex = 75;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox_imgEye1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox_imgEye2);
            this.splitContainer2.Size = new System.Drawing.Size(887, 621);
            this.splitContainer2.SplitterDistance = 397;
            this.splitContainer2.TabIndex = 73;
            // 
            // groupBox_imgEye1
            // 
            this.groupBox_imgEye1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_imgEye1.Controls.Add(this.panelDisplayEye1);
            this.groupBox_imgEye1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_imgEye1.Location = new System.Drawing.Point(0, 0);
            this.groupBox_imgEye1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox_imgEye1.Name = "groupBox_imgEye1";
            this.groupBox_imgEye1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox_imgEye1.Size = new System.Drawing.Size(397, 621);
            this.groupBox_imgEye1.TabIndex = 67;
            this.groupBox_imgEye1.TabStop = false;
            this.groupBox_imgEye1.Text = "Eye1";
            // 
            // panelDisplayEye1
            // 
            this.panelDisplayEye1.AutoScroll = true;
            this.panelDisplayEye1.Controls.Add(this.imEye1);
            this.panelDisplayEye1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplayEye1.Location = new System.Drawing.Point(2, 15);
            this.panelDisplayEye1.Name = "panelDisplayEye1";
            this.panelDisplayEye1.Size = new System.Drawing.Size(393, 604);
            this.panelDisplayEye1.TabIndex = 73;
            // 
            // imEye1
            // 
            this.imEye1.Location = new System.Drawing.Point(0, 0);
            this.imEye1.Name = "imEye1";
            this.imEye1.Size = new System.Drawing.Size(60, 60);
            this.imEye1.TabIndex = 72;
            this.imEye1.TabStop = false;
            this.imEye1.Paint += new System.Windows.Forms.PaintEventHandler(this.imEye1_Paint);
            // 
            // groupBox_imgEye2
            // 
            this.groupBox_imgEye2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox_imgEye2.Controls.Add(this.panelDisplayEye2);
            this.groupBox_imgEye2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_imgEye2.Location = new System.Drawing.Point(0, 0);
            this.groupBox_imgEye2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox_imgEye2.Name = "groupBox_imgEye2";
            this.groupBox_imgEye2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox_imgEye2.Size = new System.Drawing.Size(486, 621);
            this.groupBox_imgEye2.TabIndex = 68;
            this.groupBox_imgEye2.TabStop = false;
            this.groupBox_imgEye2.Text = "Eye2";
            // 
            // panelDisplayEye2
            // 
            this.panelDisplayEye2.AutoScroll = true;
            this.panelDisplayEye2.Controls.Add(this.imEye2);
            this.panelDisplayEye2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDisplayEye2.Location = new System.Drawing.Point(2, 15);
            this.panelDisplayEye2.Name = "panelDisplayEye2";
            this.panelDisplayEye2.Size = new System.Drawing.Size(482, 604);
            this.panelDisplayEye2.TabIndex = 73;
            // 
            // imEye2
            // 
            this.imEye2.Location = new System.Drawing.Point(0, 0);
            this.imEye2.Name = "imEye2";
            this.imEye2.Size = new System.Drawing.Size(60, 60);
            this.imEye2.TabIndex = 72;
            this.imEye2.TabStop = false;
            this.imEye2.Paint += new System.Windows.Forms.PaintEventHandler(this.imEye2_Paint);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblGlint2Center);
            this.panel6.Controls.Add(this.lblPupil2Center);
            this.panel6.Controls.Add(this.comboBox_Eye2Timer);
            this.panel6.Controls.Add(this.splitter7);
            this.panel6.Controls.Add(this.lblIP);
            this.panel6.Controls.Add(this.splitter6);
            this.panel6.Controls.Add(this.lblGlint1Center);
            this.panel6.Controls.Add(this.splitter3);
            this.panel6.Controls.Add(this.lblPupil1Center);
            this.panel6.Controls.Add(this.splitter2);
            this.panel6.Controls.Add(this.splitter1);
            this.panel6.Controls.Add(this.comboBox_Eye1Timer);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(898, 24);
            this.panel6.TabIndex = 0;
            // 
            // lblGlint2Center
            // 
            this.lblGlint2Center.AutoSize = true;
            this.lblGlint2Center.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGlint2Center.Location = new System.Drawing.Point(623, 0);
            this.lblGlint2Center.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGlint2Center.Name = "lblGlint2Center";
            this.lblGlint2Center.Size = new System.Drawing.Size(62, 13);
            this.lblGlint2Center.TabIndex = 76;
            this.lblGlint2Center.Text = "Glint Center";
            this.lblGlint2Center.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPupil2Center
            // 
            this.lblPupil2Center.AutoSize = true;
            this.lblPupil2Center.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPupil2Center.Location = new System.Drawing.Point(559, 0);
            this.lblPupil2Center.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPupil2Center.Name = "lblPupil2Center";
            this.lblPupil2Center.Size = new System.Drawing.Size(64, 13);
            this.lblPupil2Center.TabIndex = 75;
            this.lblPupil2Center.Text = "Pupil Center";
            this.lblPupil2Center.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // comboBox_Eye2Timer
            // 
            this.comboBox_Eye2Timer.BackColor = System.Drawing.SystemColors.Control;
            this.comboBox_Eye2Timer.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBox_Eye2Timer.FormattingEnabled = true;
            this.comboBox_Eye2Timer.Location = new System.Drawing.Point(437, 0);
            this.comboBox_Eye2Timer.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Eye2Timer.Name = "comboBox_Eye2Timer";
            this.comboBox_Eye2Timer.Size = new System.Drawing.Size(122, 21);
            this.comboBox_Eye2Timer.TabIndex = 74;
            // 
            // splitter7
            // 
            this.splitter7.Location = new System.Drawing.Point(435, 0);
            this.splitter7.Margin = new System.Windows.Forms.Padding(2);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(2, 24);
            this.splitter7.TabIndex = 73;
            this.splitter7.TabStop = false;
            // 
            // lblIP
            // 
            this.lblIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblIP.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblIP.Location = new System.Drawing.Point(256, 0);
            this.lblIP.Margin = new System.Windows.Forms.Padding(2);
            this.lblIP.Multiline = true;
            this.lblIP.Name = "lblIP";
            this.lblIP.ReadOnly = true;
            this.lblIP.Size = new System.Drawing.Size(179, 24);
            this.lblIP.TabIndex = 72;
            this.lblIP.Text = "Server IP: 000.000.000.000";
            this.lblIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // splitter6
            // 
            this.splitter6.Location = new System.Drawing.Point(254, 0);
            this.splitter6.Margin = new System.Windows.Forms.Padding(2);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(2, 24);
            this.splitter6.TabIndex = 71;
            this.splitter6.TabStop = false;
            // 
            // lblGlint1Center
            // 
            this.lblGlint1Center.AutoSize = true;
            this.lblGlint1Center.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGlint1Center.Location = new System.Drawing.Point(192, 0);
            this.lblGlint1Center.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGlint1Center.Name = "lblGlint1Center";
            this.lblGlint1Center.Size = new System.Drawing.Size(62, 13);
            this.lblGlint1Center.TabIndex = 66;
            this.lblGlint1Center.Text = "Glint Center";
            this.lblGlint1Center.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(190, 0);
            this.splitter3.Margin = new System.Windows.Forms.Padding(2);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(2, 24);
            this.splitter3.TabIndex = 65;
            this.splitter3.TabStop = false;
            // 
            // lblPupil1Center
            // 
            this.lblPupil1Center.AutoSize = true;
            this.lblPupil1Center.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblPupil1Center.Location = new System.Drawing.Point(126, 0);
            this.lblPupil1Center.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPupil1Center.Name = "lblPupil1Center";
            this.lblPupil1Center.Size = new System.Drawing.Size(64, 13);
            this.lblPupil1Center.TabIndex = 64;
            this.lblPupil1Center.Text = "Pupil Center";
            this.lblPupil1Center.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(124, 0);
            this.splitter2.Margin = new System.Windows.Forms.Padding(2);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(2, 24);
            this.splitter2.TabIndex = 63;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(122, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(2, 24);
            this.splitter1.TabIndex = 61;
            this.splitter1.TabStop = false;
            // 
            // comboBox_Eye1Timer
            // 
            this.comboBox_Eye1Timer.BackColor = System.Drawing.SystemColors.Control;
            this.comboBox_Eye1Timer.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBox_Eye1Timer.FormattingEnabled = true;
            this.comboBox_Eye1Timer.Location = new System.Drawing.Point(0, 0);
            this.comboBox_Eye1Timer.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Eye1Timer.Name = "comboBox_Eye1Timer";
            this.comboBox_Eye1Timer.Size = new System.Drawing.Size(122, 21);
            this.comboBox_Eye1Timer.TabIndex = 60;
            // 
            // timerReset
            // 
            this.timerReset.Interval = 500;
            this.timerReset.Tick += new System.EventHandler(this.timerReset_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 668);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Surkhab Server";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage_Camera.ResumeLayout(false);
            this.gbEye2CameraDevice.ResumeLayout(false);
            this.gbEye2CameraDevice.PerformLayout();
            this.gbEye1CameraDevice.ResumeLayout(false);
            this.gbEye1CameraDevice.PerformLayout();
            this.tabPage_Eye1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG1ABlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdGlint1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG1AConstant)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP1ABlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP1AConstant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdEye1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIris1Size)).EndInit();
            this.tabPage_Eye2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG2ABlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdGlint2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarG2AConstant)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP2ABlockSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarP2AConstant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThresholdEye2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarIris2Size)).EndInit();
            this.tabPage_Clients.ResumeLayout(false);
            this.tabPage_Clients.PerformLayout();
            this.panelClients.ResumeLayout(false);
            this.panelClients.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage_Gesture.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox_imgEye1.ResumeLayout(false);
            this.panelDisplayEye1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imEye1)).EndInit();
            this.groupBox_imgEye2.ResumeLayout(false);
            this.panelDisplayEye2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imEye2)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel6;

        private System.Windows.Forms.ComboBox comboBox_Eye1Timer;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.TextBox lblIP;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.Label lblGlint1Center;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Label lblPupil1Center;
        private System.Windows.Forms.Timer timerReset;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_Camera;
        private System.Windows.Forms.GroupBox gbEye1CameraDevice;
        private System.Windows.Forms.CheckBox cb_eye1_VFlip;
        private System.Windows.Forms.Button btnSettingsEye1;
        private System.Windows.Forms.Button btnStartEye1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDeviceCapabilityEye1;
        private System.Windows.Forms.ComboBox cmbDeviceEye1;
        private System.Windows.Forms.TabPage tabPage_Eye1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton cbG1M;
        private System.Windows.Forms.Panel panel4;
        private TransparentTrackBar trackBarG1ABlockSize;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton rbG1Gaussian;
        private System.Windows.Forms.RadioButton rbG1Mean;
        private System.Windows.Forms.RadioButton cbG1A;
        private TransparentTrackBar trackBarThresholdGlint1;
        private TransparentTrackBar trackBarG1AConstant;
        private System.Windows.Forms.CheckBox cbShowGlint1;
        private System.Windows.Forms.CheckBox cbGlint1Detection;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton cbP1M;
        private System.Windows.Forms.Panel panel3;
        private TransparentTrackBar trackBarP1ABlockSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton rbP1Gaussian;
        private System.Windows.Forms.RadioButton rbP1Mean;
        private System.Windows.Forms.RadioButton cbP1A;
        private System.Windows.Forms.CheckBox cbRemoveGlint1;
        private System.Windows.Forms.CheckBox cbDilateErode1;
        private TransparentTrackBar trackBarP1AConstant;
        private TransparentTrackBar trackBarThresholdEye1;
        private System.Windows.Forms.CheckBox cbShowPupil1;
        private System.Windows.Forms.CheckBox cbPupil1Detection;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbShowIris1;
        private TransparentTrackBar trackBarIris1Size;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage_Clients;
        private System.Windows.Forms.GroupBox panelClients;
        private System.Windows.Forms.RadioButton radioButtonAutoActivation;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox TextBoxServer;
        private System.Windows.Forms.TabPage tabPage_Gesture;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button btnDbBlink;
        private System.Windows.Forms.Button btnBlink;
        private System.Windows.Forms.Label lblGlint2Center;
        private System.Windows.Forms.Label lblPupil2Center;
        private System.Windows.Forms.ComboBox comboBox_Eye2Timer;
        private System.Windows.Forms.GroupBox gbEye2CameraDevice;
        private System.Windows.Forms.CheckBox cb_eye2_VFlip;
        private System.Windows.Forms.Button btnSettingsEye2;
        private System.Windows.Forms.Button btnStartEye2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbDeviceCapabilityEye2;
        private System.Windows.Forms.ComboBox cmbDeviceEye2;
        private System.Windows.Forms.TabPage tabPage_Eye2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton cbG2M;
        private System.Windows.Forms.Panel panel7;
        private TransparentTrackBar trackBarG2ABlockSize;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton rbG2Mean;
        private System.Windows.Forms.RadioButton cbG2A;
        private TransparentTrackBar trackBarThresholdGlint2;
        private TransparentTrackBar trackBarG2AConstant;
        private System.Windows.Forms.CheckBox cbShowGlint2;
        private System.Windows.Forms.CheckBox cbGlint2Detection;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton cbP2M;
        private System.Windows.Forms.Panel panel8;
        private TransparentTrackBar trackBarP2ABlockSize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rbP2Gaussian;
        private System.Windows.Forms.RadioButton rbP2Mean;
        private System.Windows.Forms.RadioButton cbP2A;
        private System.Windows.Forms.CheckBox cbRemoveGlint2;
        private System.Windows.Forms.CheckBox cbDilateErode2;
        private TransparentTrackBar trackBarP2AConstant;
        private TransparentTrackBar trackBarThresholdEye2;
        private System.Windows.Forms.CheckBox cbShowPupil2;
        private System.Windows.Forms.CheckBox cbPupil2Detection;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbShowIris2;
        private TransparentTrackBar trackBarIris2Size;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rbG2Gaussian;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox_imgEye1;
        private System.Windows.Forms.Panel panelDisplayEye1;
        private System.Windows.Forms.PictureBox imEye1;
        private System.Windows.Forms.GroupBox groupBox_imgEye2;
        private System.Windows.Forms.Panel panelDisplayEye2;
        private System.Windows.Forms.PictureBox imEye2;
        private System.Windows.Forms.CheckBox cb_eye2_Rotate180;
        private System.Windows.Forms.CheckBox cb_eye1_Rotate180;
    }
}

