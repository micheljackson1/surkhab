﻿//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Camilo Rodegheri
// email: camilo.rodegheri@gmail.com
// modified by: Michel Jackson
// email: michel@sens.com

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Surkhab
{
    public partial class setupSelectionForm : Form
    {
        public setupSelectionForm()
        {
            InitializeComponent();
        }


        private void Go()
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            METState.Current.monoOrBi = METState.MonoOrBi.MonocularEyeTracking;
            Go();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            METState.Current.monoOrBi = METState.MonoOrBi.BinocularEyeTracking;
            Go();
        }

    }
}
