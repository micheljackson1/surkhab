﻿
//This file is part of Surkhab
//Portions (C) 2015 Sensimetrics Corporation
//Copyright (C) 2012 Diako Mardanbegi
// ------------------------------------------------------------------------
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------
// author: Diako Mardanbegi
// email: dima@itu.dk
// modified by: Camilo Rodegheri
// email: camilo.rodegheri@gmail.com
// modified by: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using Emgu.CV.Structure;
using Emgu.CV;
using System.Windows.Forms.DataVisualization.Charting;


using NLog;


namespace Surkhab
{

    public partial class MainForm : Form
    {
        //
        private Dictionary<string, System.Windows.Forms.Button> btnClients = new Dictionary<string, Button>();
        private Dictionary<string, System.Windows.Forms.Label> lblClients = new Dictionary<string, Label>();
        private Dictionary<string, System.Windows.Forms.TextBox> txtClients = new Dictionary<string, TextBox>();
        private Dictionary<string, System.Windows.Forms.RadioButton> radClients = new Dictionary<string, RadioButton>();

        private Dictionary<int, string> ClientsPos = new Dictionary<int, string>();


        public static Logger logger;


        public MainForm()
        {
            InitializeComponent();

            logger = LogManager.GetCurrentClassLogger();
            logger.Info("Surkhab server created");

            // update control style
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);

            METState.Current.METCoreObject.SendToForm = new _SendToForm(UpdateControl);


            Icon ico = new Icon(Properties.Resources.Untitled_1, 64, 64);
            this.Icon = ico;


            ///Set the METState.Current.RemoteOrHeadMount 
            setupSelectionForm setupSelectionForm = new setupSelectionForm(); ;
            setupSelectionForm.ShowDialog();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            splitContainer1.Panel2.AutoScroll = true;

            splitContainer1.Panel2.VerticalScroll.Visible = true;
            splitContainer1.Panel2.HorizontalScroll.Visible = true;

            splitContainer1.Panel1.HorizontalScroll.Minimum = 0;
            splitContainer1.Panel1.VerticalScroll.Minimum = 0;


            //..............

            // set up tab contents depending on whether mono- or bi-nocular tracking
            if (METState.Current.monoOrBi == METState.MonoOrBi.MonocularEyeTracking)
            {
                tabPage_Eye2.Enabled = false;               // using Visible has no effect
                tabControl1.TabPages.Remove(tabPage_Eye2);  // can't go back; no changing modes

                gbEye2CameraDevice.Enabled = false;
                gbEye2CameraDevice.Visible = false;

                groupBox_imgEye2.Visible = false;

                comboBox_Eye2Timer.Enabled = false;
                comboBox_Eye2Timer.Visible = false;
                lblPupil2Center.Enabled = false;
                lblPupil2Center.Visible = false;
                lblGlint2Center.Enabled = false;
                lblGlint2Center.Visible = false;
            }

            METState.Current.eye1.showIris = cbShowIris1.Checked;
            METState.Current.eye1.showPupil = cbShowPupil1.Checked;
            METState.Current.eye1.showGlint = cbShowGlint1.Checked;

            METState.Current.eye1.vFlip = cb_eye1_VFlip.Checked;
            METState.Current.eye1.rotate180 = cb_eye1_Rotate180.Checked;

            METState.Current.eye1.glintThreshold = trackBarThresholdGlint1.Value;

            METState.Current.eye1.pupilThreshold = trackBarThresholdEye1.Value;
            METState.Current.eye1.dilateErode = cbDilateErode1.Checked;

            METState.Current.eye1.detectPupil = cbPupil1Detection.Checked;
            METState.Current.eye1.detectGlint = cbGlint1Detection.Checked;

            METState.Current.eye1.removeGlint = cbRemoveGlint1.Checked;

            cmbDeviceEye1.Text = "Loading ...";
            cmbDeviceEye1_Update();

            METState.Current.eye1.irisDiameter = trackBarIris1Size.Value;

            //Adaptive Threshold
            //          pupil
            METState.Current.eye1.pupilAdaptive = cbP1A.Checked;
            trackBarP1AConstant.Enabled = cbP1A.Checked;
            trackBarThresholdEye1.Enabled = cbP1M.Checked;

            METState.Current.eye1.pupilAdaptive_blockSize = trackBarP1ABlockSize.Value % 2 != 0 ? trackBarP1ABlockSize.Value : trackBarP1ABlockSize.Value + 1;
            METState.Current.eye1.pupilAdaptive_Constant = trackBarP1AConstant.Value;
            METState.Current.eye1.pupilAdaptive_type = rbP1Mean.Checked ? Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C : Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;

            //          glint
            METState.Current.eye1.pupilAdaptive = cbG1A.Checked;
            trackBarG1AConstant.Enabled = cbG1A.Checked;
            trackBarThresholdGlint1.Enabled = cbG1M.Checked;

            METState.Current.eye1.glintAdaptive_blockSize = trackBarG1ABlockSize.Value % 2 != 0 ? trackBarG1ABlockSize.Value : trackBarG1ABlockSize.Value + 1;
            METState.Current.eye1.glintAdaptive_Constant = trackBarG1AConstant.Value - trackBarG1AConstant.Maximum / 2;
            METState.Current.eye1.glintAdaptive_type = rbG1Mean.Checked ? Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C : Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;

            //          blink
            METState.Current.eye1.Gesture += new Eye.GestureHandler(Eye1BlinkHandler);

            if (METState.Current.monoOrBi == METState.MonoOrBi.BinocularEyeTracking)
            {
                METState.Current.eye2.showIris = cbShowIris2.Checked;
                METState.Current.eye2.showPupil = cbShowPupil2.Checked;
                METState.Current.eye2.showGlint = cbShowGlint2.Checked;

                METState.Current.eye2.vFlip = cb_eye2_VFlip.Checked;
                METState.Current.eye2.rotate180 = cb_eye2_Rotate180.Checked;

                METState.Current.eye2.glintThreshold = trackBarThresholdGlint2.Value;

                METState.Current.eye2.pupilThreshold = trackBarThresholdEye2.Value;
                METState.Current.eye2.dilateErode = cbDilateErode2.Checked;

                METState.Current.eye2.detectPupil = cbPupil2Detection.Checked;
                METState.Current.eye2.detectGlint = cbGlint2Detection.Checked;

                METState.Current.eye2.removeGlint = cbRemoveGlint2.Checked;

                cmbDeviceEye2.Text = "Loading ...";
                cmbDeviceEye2_Update();

                METState.Current.eye2.irisDiameter = trackBarIris2Size.Value;

                //Adaptive Threshold
                //          pupil
                METState.Current.eye2.pupilAdaptive = cbP2A.Checked;
                trackBarP2AConstant.Enabled = cbP2A.Checked;
                trackBarThresholdEye2.Enabled = cbP2M.Checked;

                METState.Current.eye2.pupilAdaptive_blockSize = trackBarP2ABlockSize.Value % 2 != 0 ? trackBarP2ABlockSize.Value : trackBarP2ABlockSize.Value + 1;
                METState.Current.eye2.pupilAdaptive_Constant = trackBarP2AConstant.Value;
                METState.Current.eye2.pupilAdaptive_type = rbP2Mean.Checked ? Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C : Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;

                //          glint
                METState.Current.eye2.pupilAdaptive = cbG2A.Checked;
                trackBarG2AConstant.Enabled = cbG2A.Checked;
                trackBarThresholdGlint2.Enabled = cbG2M.Checked;

                METState.Current.eye2.glintAdaptive_blockSize = trackBarG2ABlockSize.Value % 2 != 0 ? trackBarG2ABlockSize.Value : trackBarG2ABlockSize.Value + 1;
                METState.Current.eye2.glintAdaptive_Constant = trackBarG2AConstant.Value - trackBarG2AConstant.Maximum / 2;
                METState.Current.eye2.glintAdaptive_type = rbG2Mean.Checked ? Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C : Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;

                //          blink
                METState.Current.eye2.Gesture += new Eye.GestureHandler(Eye2BlinkHandler);
            }
        }


        private void timerReset_Tick(object sender, EventArgs e)
        {
            timerReset.Stop();
        }

        //Build full name of the resource
        private string getResourceName(string gestureName, bool activeImage)
        {
            string active = "";
            if (activeImage)
                active = "a";

            return "Surkhab.Resources." + gestureName + active + ".png";
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (Exception error)
            {
                logger.ErrorException("MainForm.Dispose", error);
                // System.Windows.Forms.MessageBox.Show(error.ToString()); 
                // METState.Current.ErrorSound.Play();
            }
        }

        //...................................................................
        private void CreateClientControl(string clientName)
        {
            AddControls(clientName);
            ShowControls(clientName);
        }

        private void HighLight_Client(string clientName)
        {
            foreach (KeyValuePair<string, Label> kvp in lblClients)
            {
                if (kvp.Key == clientName)
                {
                    kvp.Value.BackColor = Color.Chartreuse;
                }
                else
                {
                    kvp.Value.BackColor = Color.Transparent;
                }
            }
        }

        private int GetPosForClient()
        {
            int n = 1;
            int i = 1;

            for (i = 1; i <= ClientsPos.Count(); i++)
            {
                if (ClientsPos.ContainsKey(i) != true)
                {

                    n = i;
                    break;
                }

            }

            if (ClientsPos.Count() + 1 == i & n == 1) n = i;


            return n;
        }

        // Function to create Control array
        // 'anyControl' is type of control, 'cNumber' is number of control
        private void AddControls(string clientName)
        {
            if (btnClients.ContainsKey(clientName) != true)
            {

                lblClients.Add(clientName, new Label());
                txtClients.Add(clientName, new TextBox());
                btnClients.Add(clientName, new Button());
                radClients.Add(clientName, new RadioButton());

                int tg;
                tg = GetPosForClient();
                ClientsPos.Add(tg, clientName);
                lblClients[clientName].Tag = tg;
                txtClients[clientName].Tag = tg;
                btnClients[clientName].Tag = tg;
                radClients[clientName].Tag = tg;
            }
        }

        private void RemoveControls(string clientName)
        {

            if (btnClients.ContainsKey(clientName))
            {
                panelClients.Controls.Remove(btnClients[clientName]);

                panelClients.Controls.Remove(lblClients[clientName]);

                panelClients.Controls.Remove(txtClients[clientName]);
                panelClients.Controls.Remove(radClients[clientName]);

                ClientsPos.Remove((int)(btnClients[clientName].Tag));
                lblClients.Remove(clientName);
                txtClients.Remove(clientName);
                btnClients.Remove(clientName);
                if (radClients[clientName].Checked)
                {
                    METState.Current.server.activeScreen = "";
                    METState.Current.server.ForcedActiveScreen = "";
                    radioButtonAutoActivation.Checked = true;
                };
                radClients.Remove(clientName);
            }
        }

        private void ShowControls(string clientName)
        {
            int xPos = 10;//distance from left and right eadge & between controls
            int yPos = 10;//vertical distance between controls

            int w = panelClients.Width;//220
            int h = 24;
            int top0 = radioButtonAutoActivation.Top + radioButtonAutoActivation.Height;//distance from top eadge


            //lbl
            lblClients[clientName].AutoSize = true;
            // lblClients[clientName].Width = w - radioButtonAutoActivation.Width;
            lblClients[clientName].Height = h;
            lblClients[clientName].TextAlign = ContentAlignment.MiddleLeft;
            lblClients[clientName].BorderStyle = System.Windows.Forms.BorderStyle.None;
            lblClients[clientName].Text = clientName;

            lblClients[clientName].Left = xPos;
            lblClients[clientName].Top = ((int)(lblClients[clientName].Tag) - 1) * 2 * h + yPos + top0;

            panelClients.Controls.Add(lblClients[clientName]);

            //rad
            radClients[clientName].AutoSize = true;
            // radClients[clientName].Width = w / 3;
            radClients[clientName].Height = h;
            radClients[clientName].Text = "Active";

            radClients[clientName].Left = radioButtonAutoActivation.Left;// xPos + 2 * w / 3;
            radClients[clientName].Top = lblClients[clientName].Top;

            panelClients.Controls.Add(radClients[clientName]);
            radClients[clientName].Visible = clientName.StartsWith("Monitor") | clientName.StartsWith("TV") ? true : false;
            radClients[clientName].CheckedChanged += new System.EventHandler(Click_radClients);

            //txt
            txtClients[clientName].Width = 3 * (w - 3 * xPos) / 5;
            txtClients[clientName].Height = h;
            txtClients[clientName].BackColor = System.Drawing.SystemColors.Info;
            txtClients[clientName].Text = "";

            txtClients[clientName].Left = xPos;
            txtClients[clientName].Top = radClients[clientName].Top + radClients[clientName].Height;

            panelClients.Controls.Add(txtClients[clientName]);
            txtClients[clientName].KeyDown += new System.Windows.Forms.KeyEventHandler(KeyDown_txtClients);//// the Event 

            //btn
            btnClients[clientName].Width = 2 * (w - 3 * xPos) / 5; ;
            btnClients[clientName].Height = h;
            btnClients[clientName].Text = "send";

            btnClients[clientName].Left = txtClients[clientName].Left + txtClients[clientName].Width + xPos;
            btnClients[clientName].Top = radClients[clientName].Top + radClients[clientName].Height;

            panelClients.Controls.Add(btnClients[clientName]);

            btnClients[clientName].Click += new System.EventHandler(Click_btnClients);//// the Event of click Button
        }

        //

        public void Eye1BlinkHandler(object sender, GestureEventArgs e)
        {
            METState.Current.server.Send("Commands", new string[]
            {
                DateTime.Now.Ticks.ToString(),
                (METState.Current.monoOrBi == METState.MonoOrBi.MonocularEyeTracking) ? "Eye" : "Eye1",
                e.Gesture,
            });
        }

        public void Eye2BlinkHandler(object sender, GestureEventArgs e)
        {
            METState.Current.server.Send("Commands", new string[]
            {
                DateTime.Now.Ticks.ToString(),
                "Eye2",
                e.Gesture,
            });
        }

        //

        public void Click_btnClients(Object sender, System.EventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show("send to " +  ((System.Windows.Forms.Button)sender).Tag.ToString());
            int tg = (int)(((System.Windows.Forms.Button)sender).Tag);
            string cName = ClientsPos[tg];

            SendTextToClient(cName);
        }

        public void Click_radClients(Object sender, System.EventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show("send to " +  ((System.Windows.Forms.Button)sender).Tag.ToString());
            int tg = (int)(((System.Windows.Forms.RadioButton)sender).Tag);
            string cName = ClientsPos[tg];
            //  if (radClients[cName].Checked) radClients[cName].Checked = false; ;

            METState.Current.server.ForcedActiveScreen = radClients[cName].Checked ? cName : "";
            METState.Current.server.activeScreen = METState.Current.server.ForcedActiveScreen;
        }

        public void KeyDown_txtClients(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                int tg = (int)(((System.Windows.Forms.TextBox)sender).Tag);

                string cName = ClientsPos[tg];
                SendTextToClient(cName);
            }
        }

        public void SendTextToClient(string cName)
        {
            METState.Current.server.Send(cName, txtClients[cName].Text);

            METState.Current.METCoreObject.SendToForm("\r\n To " + cName + ": " + txtClients[cName].Text + "\r\n", "TextBoxServer");
            txtClients[cName].Text = "";
        }


        //.......................................................................


        private void cmbDeviceEye1_Update()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    VideoSource.FindCamera find = new VideoSource.FindCamera();
                    find.Search();
                    VideoSource.IVideoSource[] devices = find.DeviceList.ToArray();

                    this.Invoke((MethodInvoker)delegate
                    {
                        var eyeSelDvc = cmbDeviceEye1.SelectedItem;
                        cmbDeviceEye1.BeginUpdate();
                        cmbDeviceEye1.Items.Clear();

                        cmbDeviceCapabilityEye1.Items.Clear();
                        cmbDeviceCapabilityEye1.Text = "";

                        cmbDeviceEye1.Items.AddRange(devices);
                        cmbDeviceEye1.EndUpdate();
                        cmbDeviceEye1.SelectedItem = eyeSelDvc;
                        switch (devices.Count())
                        {
                            case 0:
                                break;

                            case 1:
                                cmbDeviceEye1.SelectedIndex = 0;
                                break;

                            case 2:
                                cmbDeviceEye1.SelectedIndex = 0;
                                break;

                            default:
                                //IN 2TA Jabeja MItoonan beshan
                                cmbDeviceEye1.SelectedIndex = 0;
                                break;
                        }
                    });
                });
        }

        private void cmbDeviceEye2_Update()
        {
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                VideoSource.FindCamera find = new VideoSource.FindCamera();
                find.Search();
                VideoSource.IVideoSource[] devices = find.DeviceList.ToArray();

                this.Invoke((MethodInvoker)delegate
                {
                    var eyeSelDvc = cmbDeviceEye2.SelectedItem;
                    cmbDeviceEye2.BeginUpdate();
                    cmbDeviceEye2.Items.Clear();

                    cmbDeviceCapabilityEye2.Items.Clear();
                    cmbDeviceCapabilityEye2.Text = "";

                    cmbDeviceEye2.Items.AddRange((object[])devices);

                    cmbDeviceEye2.EndUpdate();
                    cmbDeviceEye2.SelectedItem = eyeSelDvc;
                    switch (devices.Count())
                    {
                        case 0:
                            break;

                        case 1:
                            cmbDeviceEye2.SelectedIndex = 0;
                            break;

                        case 2:
                            cmbDeviceEye2.SelectedIndex = 1;
                            break;

                        default:
                            //IN 2TA Jabeja MItoonan beshan
                            cmbDeviceEye2.SelectedIndex = 1;
                            break;
                    }
                });
            });
        }


        private void cmbDeviceEye1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dev = cmbDeviceEye1.SelectedItem as VideoSource.IVideoSource;
            if (dev == null) return;

            logger.Info("Camera 1 set to {0}", dev.Name);

            var cmbCap = this.cmbDeviceCapabilityEye1;
            cmbCap.BeginUpdate();
            cmbCap.Items.Clear();
            cmbCap.Items.AddRange(dev.Capabilities.ToArray());
            if (cmbCap.Items.Count > 0)
                cmbCap.SelectedIndex = 0;
            cmbCap.EndUpdate();

            this.btnSettingsEye1.Visible = dev.HasSettings;

        }

        private void cmbDeviceEye2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var dev = cmbDeviceEye2.SelectedItem as VideoSource.IVideoSource;
            if (dev == null) return;

            logger.Info("Camera 2 set to {0}", dev.Name);

            var cmbCap = this.cmbDeviceCapabilityEye2;
            cmbCap.BeginUpdate();
            cmbCap.Items.Clear();
            cmbCap.Items.AddRange(dev.Capabilities.ToArray());
            if (cmbCap.Items.Count > 0)
                cmbCap.SelectedIndex = 0;
            cmbCap.EndUpdate();

            this.btnSettingsEye2.Visible = dev.HasSettings;

        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            logger.Info("Server closing");

            METState.Current.server.Close();
            Thread.Sleep(10);
            // METState.Current.server = null;

            logger.Info("Server exiting");
        }

        private void radioButtonAutoActivation_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonAutoActivation.Checked) METState.Current.server.ForcedActiveScreen = "";
        }




        private void btnStartEye1_Click(object sender, EventArgs e)
        {
            if (btnStartEye1.Text == "Start")
            {
                logger.Info("Camera 1 starting");

                btnStartEye1.Text = "Stop";

                var camOld = METState.Current.Eye1Camera;
                if (camOld != null && camOld.IsRunning)
                    camOld.Stop();
                var cam = cmbDeviceEye1.SelectedItem as VideoSource.IVideoSource;
                cam.SelectedCap = (VideoSource.DeviceCapabilityInfo)cmbDeviceCapabilityEye1.SelectedItem;
                METState.Current.Eye1Camera = cam;

                METState.Current.Eye1Camera.NewFrame += new AForge.Video.NewFrameEventHandler(METState.Current.METCoreObject.Eye1FrameCaptured);

                METState.Current.METCoreObject.TrackerEventEye1 += new TrackerEventHandler(imEye1Update);

                cam.Start();
            }
            else
            {
                logger.Info("Camera 1 stopping");

                //stop video
                btnStartEye1.Text = "Start";
                METState.Current.METCoreObject.TrackerEventEye1 -= new TrackerEventHandler(imEye1Update);

                var camOld = METState.Current.Eye1Camera;
                if (camOld.IsRunning)
                    camOld.Stop();

                imEye1.Image = null;
                imEye1.Size = System.Drawing.Size.Empty;
                bFirstFrameEye1 = false;
            }
        }

        private void btnStartEye2_Click(object sender, EventArgs e)
        {
            if (btnStartEye2.Text == "Start")
            {
                logger.Info("Camera 2 starting");

                btnStartEye2.Text = "Stop";

                var camOld = METState.Current.Eye2Camera;
                if (camOld != null && camOld.IsRunning)
                    camOld.Stop();
                var cam = cmbDeviceEye2.SelectedItem as VideoSource.IVideoSource;
                cam.SelectedCap = (VideoSource.DeviceCapabilityInfo)cmbDeviceCapabilityEye2.SelectedItem;
                METState.Current.Eye2Camera = cam;

                METState.Current.Eye2Camera.NewFrame += new AForge.Video.NewFrameEventHandler(METState.Current.METCoreObject.Eye2FrameCaptured);

                METState.Current.METCoreObject.TrackerEventEye2 += new TrackerEventHandler(imEye2Update);

                cam.Start();
            }
            else
            {
                logger.Info("Camera 2 stopping");

                //stop video
                btnStartEye2.Text = "Start";
                METState.Current.METCoreObject.TrackerEventEye2 -= new TrackerEventHandler(imEye2Update);

                var camOld = METState.Current.Eye2Camera;
                if (camOld.IsRunning)
                    camOld.Stop();

                imEye2.Image = null;
                imEye2.Size = System.Drawing.Size.Empty;
                bFirstFrameEye2 = false;
            }
        }


        private void btnSettingsEye1Click(object sender, EventArgs e)
        {
            var src = (cmbDeviceEye1.SelectedItem as VideoSource.IVideoSource);
            if (src != null && src.HasSettings)
                src.ShowSettings();
        }

        private void btnSettingsEye2_Click(object sender, EventArgs e)
        {
            var src = (cmbDeviceEye2.SelectedItem as VideoSource.IVideoSource);
            if (src != null && src.HasSettings)
                src.ShowSettings();
        }

        private void trackBarIris1Size_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.irisDiameter = trackBarIris1Size.Value;
        }

        private void trackBarIris2Size_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.irisDiameter = trackBarIris2Size.Value;
        }

        private void cbShowIris1_CheckedChanged(object sender, EventArgs e)
        {
            cbShowIris1.BackColor = cbShowIris1.Checked ? Color.Yellow : Color.Transparent;
            METState.Current.eye1.showIris = cbShowIris1.Checked;
        }

        private void cbShowIris2_CheckedChanged(object sender, EventArgs e)
        {
            cbShowIris2.BackColor = cbShowIris2.Checked ? Color.Yellow : Color.Transparent;
            METState.Current.eye2.showIris = cbShowIris2.Checked;
        }

        private void cbPupil1Detection_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.detectPupil = cbPupil1Detection.Checked;
            METState.Current.eye1.firstPupilDetection = cbPupil1Detection.Checked;
        }

        private void cbPupil2Detection_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.detectPupil = cbPupil2Detection.Checked;
            METState.Current.eye2.firstPupilDetection = cbPupil2Detection.Checked;
        }

        private void cbShowPupil1_CheckedChanged(object sender, EventArgs e)
        {
            cbShowPupil1.BackColor = cbShowPupil1.Checked ? Color.Yellow : Color.Transparent;

            METState.Current.eye1.showPupil = cbShowPupil1.Checked;
        }

        private void cbShowPupil2_CheckedChanged(object sender, EventArgs e)
        {
            cbShowPupil2.BackColor = cbShowPupil2.Checked ? Color.Yellow : Color.Transparent;

            METState.Current.eye2.showPupil = cbShowPupil2.Checked;
        }



        private void cbP1A_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive = true;
            trackBarP1AConstant.Enabled = cbP1A.Checked;
        }

        private void cbP2A_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive = true;
            trackBarP2AConstant.Enabled = cbP2A.Checked;
        }

        private void cbP1M_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive = false;
            trackBarThresholdEye1.Enabled = cbP1M.Checked;
        }

        private void cbP2M_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive = false;
            trackBarThresholdEye2.Enabled = cbP2M.Checked;
        }



        private void trackBarThreshold1_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilThreshold = trackBarThresholdEye1.Value;
        }

        private void trackBarThreshold2_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilThreshold = trackBarThresholdEye2.Value;
        }

        private void trackBarP1AConstant_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive_Constant = trackBarP1AConstant.Value;
        }

        private void trackBarP2AConstant_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive_Constant = trackBarP2AConstant.Value;
        }


        private void cbDilateErode1_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.dilateErode = cbDilateErode1.Checked;
        }

        private void cbDilateErode2_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.dilateErode = cbDilateErode2.Checked;
        }

        private void cbRemoveGlint1_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.removeGlint = cbRemoveGlint1.Checked;
        }

        private void cbRemoveGlint2_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.removeGlint = cbRemoveGlint2.Checked;
        }

        private void cbGlint1Detection_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.detectGlint = cbGlint1Detection.Checked;
        }

        private void cbGlint2Detection_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.detectGlint = cbGlint2Detection.Checked;
        }

        private void cbShowGlint1_CheckedChanged(object sender, EventArgs e)
        {
            cbShowGlint1.BackColor = cbShowGlint1.Checked ? Color.Yellow : Color.Transparent;

            METState.Current.eye1.showGlint = cbShowGlint1.Checked;
        }

        private void cbShowGlint2_CheckedChanged(object sender, EventArgs e)
        {
            cbShowGlint2.BackColor = cbShowGlint2.Checked ? Color.Yellow : Color.Transparent;

            METState.Current.eye2.showGlint = cbShowGlint2.Checked;
        }

        private void cbG1A_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive = true;
            trackBarG1AConstant.Enabled = cbG1A.Checked;
        }

        private void cbG2A_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive = true;
            trackBarG2AConstant.Enabled = cbG2A.Checked;
        }

        private void cbG1M_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive = false;
            trackBarThresholdGlint1.Enabled = cbG1M.Checked;
        }

        private void cbG2M_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive = false;
            trackBarThresholdGlint2.Enabled = cbG2M.Checked;
        }


        private void trackBarThresholdGlint1_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintThreshold = trackBarThresholdGlint1.Value;
        }

        private void trackBarThresholdGlint2_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintThreshold = trackBarThresholdGlint2.Value;
        }

        private void trackBarG1AConstant_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive_Constant = trackBarG1AConstant.Value - trackBarG1AConstant.Maximum / 2;
        }

        private void trackBarG2AConstant_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive_Constant = trackBarG2AConstant.Value - trackBarG2AConstant.Maximum / 2;
        }


        private void rbP1Mean_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C;
        }

        private void rbP2Mean_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C;
        }

        private void rbP1Gaussian_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;
        }

        private void rbP2Gaussian_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;
        }

        private void trackBarP1ABlockSize_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.pupilAdaptive_blockSize = trackBarP1ABlockSize.Value % 2 != 0 ? trackBarP1ABlockSize.Value : trackBarP1ABlockSize.Value + 1;
        }

        private void trackBarP2ABlockSize_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.pupilAdaptive_blockSize = trackBarP2ABlockSize.Value % 2 != 0 ? trackBarP2ABlockSize.Value : trackBarP2ABlockSize.Value + 1;
        }


        private void rbG1Mean_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C;
        }

        private void rbG2Mean_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_MEAN_C;
        }

        private void rbG1Gaussian_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;
        }

        private void rbG2Gaussian_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive_type = Emgu.CV.CvEnum.ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C;
        }

        private void trackBarG1ABlockSize_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.glintAdaptive_blockSize = trackBarG1ABlockSize.Value % 2 != 0 ? trackBarG1ABlockSize.Value : trackBarG1ABlockSize.Value + 1;
        }

        private void trackBarG2ABlockSize_ValueChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.glintAdaptive_blockSize = trackBarG2ABlockSize.Value % 2 != 0 ? trackBarG2ABlockSize.Value : trackBarG2ABlockSize.Value + 1;
        }


        //.......................................................................Updating the Form.......................................................................

        public void UpdateControl(object message, string controlName)
        {
            if (METState.Current.TimerEnable == true)
            {
                switch (controlName)
                {
                    case "textBoxTimerEye1":
                        if (comboBox_Eye1Timer.InvokeRequired)
                        {
                            Invoke(new _SendToForm(UpdateControl), new object[] { message, "textBoxTimerEye1" });
                        }
                        else
                        {
                            comboBox_Eye1Timer.Items.Clear();
                            //foreach (KeyValuePair<string, object> kvp in METState.Current.ProcessTimeEyeBranch.TimerResults.Reverse())
                            //{
                            //    if (kvp.Key == "Total 1")
                            //    {
                            //        // comboBox_EyeTimer.Items.Add("Eye: " + kvp.Value.ToString() + " ms");// kvp.Key +
                            //        // comboBox_EyeTimer.SelectedIndex = 0;
                            //        comboBox_Eye1Timer.Text = "Eye1: " + kvp.Value.ToString() + " ms";
                            //    }
                            //    else
                            //    {
                            //        comboBox_Eye1Timer.Items.Add(kvp.Key + ": " + kvp.Value.ToString() + " ms");
                            //    }
                            //}
                        }
                        break;

                    case "textBoxTimerEye2":
                        if (comboBox_Eye2Timer.InvokeRequired)
                        {
                            Invoke(new _SendToForm(UpdateControl), new object[] { message, "textBoxTimerEye2" });
                        }
                        else
                        {
                            comboBox_Eye2Timer.Items.Clear();
                            //foreach (KeyValuePair<string, object> kvp in METState.Current.ProcessTimeEyeBranch.TimerResults.Reverse())
                            //{
                            //    if (kvp.Key == "Total 2")
                            //    {
                            //        // comboBox_EyeTimer.Items.Add("Eye: " + kvp.Value.ToString() + " ms");// kvp.Key +
                            //        // comboBox_EyeTimer.SelectedIndex = 0;
                            //        comboBox_Eye2Timer.Text = "Eye2: " + kvp.Value.ToString() + " ms";
                            //    }
                            //    else
                            //    {
                            //        comboBox_Eye2Timer.Items.Add(kvp.Key + ": " + kvp.Value.ToString() + " ms");

                            //    }
                            //}
                        }
                        break;
                }
            }


            switch (controlName)
            {
                case "timerReset":
                    if (lblIP.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "timerReset" });
                    }
                    else
                    {
                        timerReset.Start();
                    }
                    break;

                case "lblIP":
                    if (lblIP.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "lblIP" });
                    }
                    else
                    {
                        lblIP.Text = message.ToString();
                        // lblIP.Text ="Server IP : ***.***.***.***";
                    }
                    break;

                case "lblP1C":
                    if (lblPupil1Center.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "lblP1C" });
                    }
                    else
                    {
                        if (message.GetType() == typeof(AForge.Point)) //(message != "NoPupil")
                        {
                            AForge.Point p = (AForge.Point)message;
                            lblPupil1Center.Text = "pupil: (" + ((int)p.X).ToString() + "," + ((int)p.Y).ToString() + ")";
                        }
                        else
                        {
                            lblPupil1Center.Text = message.ToString();
                        }
                    }
                    break;

                case "lblP2C":
                    if (lblPupil2Center.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "lblP2C" });
                    }
                    else
                    {
                        if (message.GetType() == typeof(AForge.Point)) //(message != "NoPupil")
                        {
                            AForge.Point p = (AForge.Point)message;
                            lblPupil2Center.Text = "pupil: (" + ((int)p.X).ToString() + "," + ((int)p.Y).ToString() + ")";
                        }
                        else
                        {
                            lblPupil2Center.Text = message.ToString();
                        }
                    }
                    break;

                case "lblG1C"://test
                    if (lblGlint1Center.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "lblG1C" });
                    }
                    else
                    {
                        if (message.GetType() == typeof(AForge.Point)) //(message != "NoGlint")
                        {
                            AForge.Point p = (AForge.Point)message;
                            lblGlint1Center.Text = "glint: (" + ((int)p.X).ToString() + "," + ((int)p.Y).ToString() + ")";
                        }
                        else
                        {
                            lblGlint1Center.Text = message.ToString();
                        }

                    }
                    break;

                case "lblG2C"://test
                    if (lblGlint1Center.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "lblG2C" });
                    }
                    else
                    {
                        if (message.GetType() == typeof(AForge.Point)) //(message != "NoGlint")
                        {
                            AForge.Point p = (AForge.Point)message;
                            lblGlint2Center.Text = "glint: (" + ((int)p.X).ToString() + "," + ((int)p.Y).ToString() + ")";
                        }
                        else
                        {
                            lblGlint2Center.Text = message.ToString();
                        }

                    }
                    break;

                case "Pupil1Threshold":
                    if (trackBarThresholdEye1.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "Pupil1Threshold" });
                    }
                    else
                    {
                        trackBarThresholdEye1.Value = (int)message;
                    }
                    break;

                case "Pupil2Threshold":
                    if (trackBarThresholdEye2.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "Pupil2Threshold" });
                    }
                    else
                    {
                        trackBarThresholdEye2.Value = (int)message;
                    }
                    break;

                case "TextBoxServer":

                    if (TextBoxServer.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "TextBoxServer" });
                    }
                    else
                    {

                        TextBoxServer.Text += (string)message;
                        TextBoxServer.SelectionStart = TextBoxServer.Text.Length;
                        TextBoxServer.ScrollToCaret();
                    }


                    break;
                case "PanelClients_Add":

                    if (panelClients.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "PanelClients_Add" });
                    }
                    else
                    {
                        CreateClientControl((string)message);
                    }


                    break;
                case "PanelClients_Remove":

                    if (panelClients.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "PanelClients_Remove" });
                    }
                    else
                    {
                        RemoveControls((string)message);
                    }


                    break;
                case "ActiveMonitor_Highlight":

                    if (TextBoxServer.InvokeRequired)
                    {
                        Invoke(new _SendToForm(UpdateControl), new object[] { message, "ActiveMonitor_Highlight" });
                    }
                    else
                    {
                        HighLight_Client((string)message);
                    }

                    break;

            }

        }

        bool bFirstFrameEye1 = false;
        private Bitmap _bitmapimEye1;
        public void imEye1Update(object sender, METEventArg eventArgs)
        {

            if (btnStartEye1.Text == "Stop")//METState.Current.EyeCamera.IsRunning == true &&
            {
                _bitmapimEye1 = (Bitmap)METState.Current.eye1.ForShow.Bitmap.Clone();

                if (!bFirstFrameEye1)
                {
                    imEye1.Invoke(
                        (MethodInvoker)delegate
                        {
                            groupBox_imgEye1.Width = _bitmapimEye1.Width;
                            imEye1.Width = _bitmapimEye1.Width;
                            imEye1.Height = _bitmapimEye1.Height;
                        });

                    bFirstFrameEye1 = true;
                }

                imEye1.Invalidate();
            }
        }

        bool bFirstFrameEye2 = false;
        private Bitmap _bitmapimEye2;
        public void imEye2Update(object sender, METEventArg eventArgs)
        {

            if (btnStartEye2.Text == "Stop")//METState.Current.EyeCamera.IsRunning == true &&
            {
                _bitmapimEye2 = (Bitmap)METState.Current.eye2.ForShow.Bitmap.Clone();

                if (!bFirstFrameEye2)
                {
                    imEye2.Invoke(
                        (MethodInvoker)delegate
                        {
                            groupBox_imgEye2.Width = _bitmapimEye2.Width;
                            imEye2.Width = _bitmapimEye2.Width;
                            imEye2.Height = _bitmapimEye2.Height;
                        });

                    bFirstFrameEye2 = true;
                }

                imEye2.Invalidate();
            }
        }


        private void cb_eye1_VFlip_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.vFlip = cb_eye1_VFlip.Checked;
        }

        private void cb_eye2_VFlip_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.vFlip = cb_eye2_VFlip.Checked;
        }


        private void cb_eye1_Rotate180_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye1.rotate180 = cb_eye1_Rotate180.Checked;
        }

        private void cb_eye2_Rotate180_CheckedChanged(object sender, EventArgs e)
        {
            METState.Current.eye2.rotate180 = cb_eye2_Rotate180.Checked;
        }


        private void imEye1_Paint(object sender, PaintEventArgs e)
        {
            if (_bitmapimEye1 != null)
            {
                e.Graphics.DrawImage(_bitmapimEye1, 0, 0, _bitmapimEye1.Width, _bitmapimEye1.Height);
            }
        }

        private void imEye2_Paint(object sender, PaintEventArgs e)
        {
            if (_bitmapimEye2 != null)
            {
                e.Graphics.DrawImage(_bitmapimEye2, 0, 0, _bitmapimEye2.Width, _bitmapimEye2.Height);
            }
        }

    }
}
