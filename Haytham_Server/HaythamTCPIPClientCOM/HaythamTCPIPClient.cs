﻿//This file is part of HaytamTCPIPClientCOM
//Copyright (C) 2015 Sensimetrics Corporation
// ------------------------------------------------------------------------
//Sensimetrics Corporation remains the owner of all rights, title and interest
//in the Software and Documentation. You may copy the Software for backup and
//archival purposes, provided that the original and each copy is kept in your
//possession. The Software and Documentation are protected by United States
//copyright laws and international treaties. You must treat the Software and
//Documentation like any other copyrighted material. ANY USE OR DISCLOSURE OF
//THE SOFTWARE, OR OF ITS ALGORITHMS, PROTOCOLS OR INTERFACES, OTHER THAN IN
//STRICT ACCORDANCE WITH THIS LICENSE AGREEMENT, MAY BE ACTIONABLE.
// ------------------------------------------------------------------------
// author: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.IO;

using System.Threading;
using System.Runtime.InteropServices;

using NLog;

using Sanford.Collections.Generic;

using Haytham;

/*
 * Note that in this application, "gaze" is not used to mean the same thing
 * as it is in the original Haytham server or in the field in general.
 * Here "gaze" is used to mean "pupil position".
 */


namespace HaythamTCPIPClientCOM
{
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("E8CBF4BA-1802-4314-875B-0076063356E5")]
    [ProgId("SensimetricsGazeCOM.HaythamTCPIPClient")]
    [ComSourceInterfaces(typeof(IHaythamTCPIPClient_Events))]
    public class HaythamTCPIPClient : IHaythamTCPIPClient
    {
        #region "Cache data received from server"

        private object pupilDataLock = new object();

        private class Pupil
        {
            public long Timestamp;
            public bool Found;
            public int Diameter;
            public double CenterXFraction;
            public double CenterYFraction;

            public bool Updated;
        };

        volatile Pupil pupil1 = new Pupil();
        volatile Pupil pupil2 = new Pupil();

        volatile string command;
        volatile string arg;

        #endregion

        #region "Derived quantities"

        Double[,] azCalibration;

        // size of window for rejecting azimuths
        int filterLength = 1;           // default - not filtered!
        double allowedDeviation = 15.0;
        Deque<Double> azHistory = new Deque<double>();

        private class AzElResults
        {
            public HaythamGazeQuality q;
            public double az;
            public double az1;
            public double az2;
        };

        AzElResults azElResults = new AzElResults();

        #endregion


        TcpClient tcpClient;


        #region "Connect to Haytham server"
        [ComVisible(false)]
        public delegate void ConnectedEventHandler(bool isConnected);

        public event ConnectedEventHandler Connected;

        Thread threadConnect = null;
        [ComVisible(false)]
        ConnectThread connectThread;
        #endregion

        #region "Write to and read from haytham server"
        private BinaryWriter writer = null; // facilitates writing to the stream
        private BinaryReader reader = null; // facilitates reading from the strea  
        private NetworkStream stream = null; // network data stream

        private volatile bool closeStreams = false;
        #endregion

        #region "Haytham server replies"
        private Thread ioThread;
        private AutoResetEvent ioThreadStopped;


        [ComVisible(false)]
        public delegate void ServerInfoEventHandler(string connectionName);
        public event ServerInfoEventHandler ServerInfo;

        [ComVisible(false)]
        public delegate void PupilInfoEventHandler(bool found, int diameter, double centerX, double centerY);
        public event PupilInfoEventHandler Pupil1Info;
        public event PupilInfoEventHandler Pupil2Info;

        [ComVisible(false)]
        public delegate void AzimuthInfoEventHandler(HaythamGazeQuality quality, double azMean, double az1, double az2, double elMean, double el1, double el2, int pupilDiameter1, int pupilDiameter2);
        public event AzimuthInfoEventHandler AzimuthInfo;

        [ComVisible(false)]
        public delegate void CommandInfoEventHandler(string command, string arg);
        public event CommandInfoEventHandler CommandInfo;
        #endregion

        #region "Logging"

        private static Logger logger;

        #endregion


        static HaythamTCPIPClient()
        {
            // initialize NLog - make sure we can find a config file!
            string assemblyFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            LogManager.ThrowExceptions = true;
            LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(assemblyFolder + "\\NLog.config", true);
            logger = LogManager.GetCurrentClassLogger();

            logger.Trace("configuration file expected in {0}", assemblyFolder);
        }


        public HaythamTCPIPClient() {
            pupil1.Found = false;
            pupil1.Diameter = 0;
            pupil1.CenterXFraction = 0.0F;
            pupil1.CenterYFraction = 0.0F;

            pupil1.Updated = true;

            pupil2.Found = false;
            pupil2.Diameter = 0;
            pupil2.CenterXFraction = 0.0F;
            pupil2.CenterYFraction = 0.0F;

            pupil2.Updated = true;

            command = "";

            azCalibration = new Double[,] {{0, 0}};

            tcpClient = new TcpClient();

            logger.Info("HaythamTCPIPClient created");

            //// testing a private method - in lieu of unit test
            //TestAzimuthCalculation();

            //// test history stats
            //TestAzHistoryStats();

            //// test a public method that depends on history - in lieu of unit test
            //TestOutlierPointRejection();

            //// test processing & azimuth calculation with non-synchronous data from both eyes
            //TestBinocularSequence();

            //// test processing & azimuth calculation with non-synchronous data from both eyes when there is rapid movement
            //TestBinocularSequenceMove();

            //// test processing & azimuth calculation with a divergence between the eyes near row 51
            //TestBinocularSequenceDivergence();

            //// test processing & azimuth calculation with simulated "flicker" of gaze away from steady target, then returning
            //// NOTE: flick NOT rejected, due to averaging between eyes
            //TestBinocularSequenceSteadyFlickMedian5();

            //// test processing & azimuth calculation with simulated "flicker" of gaze away from steady target, then returning
            //// NOTE: flick should be rejected, due to longer median filter
            //TestBinocularSequenceSteadyFlickMedian7();

            //// test processing with movement derived from mono data; binocular measures simulated
            //TestBinocularSimMoving();

            //// test processing with movement derived from mono data and "glance ahead" of magnitude derived from mono data; binocular measures simulated
            //TestBinocularSimMovingFlick();

            //// same with bigger "glance ahead"
            //TestBinocularSimMovingBigFlick();

            //// test smaller "glance ahead" but with inter-pupil deviation threshold lower
            //TestBinocularSimMovingFlickLowerDeviationAllowed();

            //// test bigger "glance back"
            //TestBinocularSimMovingBigFlickBack();

            logger.Debug("Attempting default connection ...");
            Connect("", 0);
        }

        private void ResetDataCaches()
        {
            azElResults = new AzElResults();
            azHistory.Clear();
            pupil1 = new Pupil();
            pupil2 = new Pupil();

            allowedDeviation = 15.0;
        }

        private void TestAzimuthCalculation()
        {
            logger.Trace("Testing polynomial evaluation ...");

            azCalibration = new Double[,] { { -19.0, 0 }, { 7.0, 0 }, { -4.0, 0 }, { 6.0, 0 } };
            logger.Trace("128 = {0}", azimuthFromPolynomial(3.0, 0));

            azCalibration = new Double[,] { { 0, -19.0 }, { 0, 7.0 }, { 0, -4.0 }, { 0, 6.0 } };
            logger.Trace("128 = {0}", azimuthFromPolynomial(3.0, 1));

            azCalibration = new Double[,] { { 0, 0 }, { 0.5, 0 }, { 0.5, 0 } };
            logger.Trace("3 = {0}", azimuthFromPolynomial(2.0, 0));
        }

        private void TestAzHistoryStats()
        {
            ResetDataCaches();

            foreach (double i in new double[] { 9, 11, 3, 3, 5 })
            {
                azHistory.PushFront(i);
            }

            double mean, sd, median;
            CalculateAzHistoryStats(out mean, out sd, out median);

            logger.Trace("mean 6.2 = {0} sd 3.633180425 = {1} median 5 = {2}", mean, sd, median);

            azHistory.Clear();
            foreach (double i in new double[] { 3, 5, 7, 9 })
            {
                azHistory.PushFront(i);
            }
            CalculateAzHistoryStats(out mean, out sd, out median);

            logger.Trace("mean 6 = {0} sd 2.581988897 = {1} median 6 = {2}", mean, sd, median);
        }

        private void TestOutlierPointRejection()
        {
            #region "Check whether single point in dummy sequence accepted"

            logger.Trace("Setting dummy calibration ...");
            TestWithDummyCalibration();

            logger.Trace("Populating raw data from dummy sequence - no filtering...");
            // a new raw data value that is valid (next in sequence)
            TestSetEyeData(true, 47, 0.5109714, 0.0, false, 0, 0.0, 0.0);

            // get the calculated azimuth
            logger.Trace("Getting azimuth based on new data ...");
            object azimuthData;
            AzElGaze(out azimuthData);

            // expected: should be Eye1, Current, dummy (calculated azimuth) 2.042825223
            Double[] azEl_data = (Double[])azimuthData;
            logger.Trace("Eye1Current: {0} 2.042825223: {1}", (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1]);

            #endregion

            #region "Check whether single point in dummy sequence accepted when filtering"

            logger.Trace("Setting dummy calibration ...");
            filterLength = 11;
            TestWithDummyCalibration();

            logger.Trace("Populating raw data from dummy sequence - filtering...");
            // a new raw data value that is valid (next in sequence)
            TestSetEyeData(true, 47, 0.5109714, 0.0, false, 0, 0.0, 0.0);

            // get the calculated azimuth
            logger.Trace("Getting azimuth based on new data ...");
            AzElGaze(out azimuthData);

            // expected: should be Eye1, Current, dummy (calculated azimuth) 0.614628113
            azEl_data = (Double[])azimuthData;
            logger.Trace("Eye1Current: {0} 0.614628113: {1}", (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1]);

            #endregion


            double[] xs;
            double[] expectedAzs;

            #region "Check whether entire tail of dummy sequence is accepted"

            logger.Trace("Processing sequence - no filtering...");
            TestWithDummyCalibration();

            filterLength = 1;

            xs = new double[] {0.5138584, 0.5163397, 0.5186505, 0.5202093, 0.5217203, 0.5222324, 0.5225998, 0.5234205, 0.5245802, 0.5259353,
                            0.5280232, 0.5292752, 0.5314754, 0.5331897, 0.5354856, 0.5374723, 0.5393229, 0.5411325, 0.5425939, 0.5441355,
                            0.5457211, 0.5468897, 0.5486133, 0.5508010, 0.5479954, 0.5512861, 0.5545871, 0.5578049, 0.5604997, 0.5628721,
                            0.5645987, 0.5660580, 0.5679168, 0.5697351, 0.5727631, 0.5763704, 0.5790960, 0.5828288, 0.5878636, 0.5905027,
                            0.5924414, 0.5939637, 0.5954598, 0.5967057, 0.5979514, 0.5995446, 0.6021314, 0.6043962, 0.6070555, 0.6088537,
                            0.6109554, 0.6127989, 0.6147738, 0.6165930, 0.6188908, 0.6208969, 0.6229975, 0.6253671};
            expectedAzs = new double[] {2.369824958, 2.650872566, 2.912608273, 3.089167742, 3.260313083, 3.318316742, 3.359930772, 3.452888403, 3.584243302, 3.737730431,
                                    3.974219086, 4.116028462, 4.365236919, 4.559409294, 4.819457334, 5.044483442, 5.254094009, 5.459060660, 5.624587994, 5.799199283,
                                    5.978794288, 6.111157257, 6.306383008, 6.554175637, 6.236395777, 6.609121107, 6.983013079, 7.347481298, 7.652711255, 7.921424165,
                                    8.116989715, 8.282279189, 8.492818541, 8.698770608, 9.041740888, 9.450326320, 9.759044878, 10.18184523, 10.75211827, 11.05103929,
                                    11.27062862, 11.44305387, 11.61251154, 11.75363000, 11.89472579, 12.07518162, 12.36817882, 12.62470428, 12.92591328, 13.12958870,
                                    13.36764043, 13.57644681, 13.80013637, 14.00619038, 14.26645363, 14.49367710, 14.73160424, 15.00000000};

            CheckSequence(xs, expectedAzs, null);

            #endregion

            #region "Check whether entire tail of dummy sequence is accepted when filtering"

            logger.Trace("Processing sequence - filtering...");

            SetFilterLength(5);
            TestWithDummyCalibration();

            xs = new double[] {0.5138584, 0.5163397, 0.5186505, 0.5202093, 0.5217203, 0.5222324, 0.5225998, 0.5234205, 0.5245802, 0.5259353,
                              0.5280232, 0.5292752, 0.5314754, 0.5331897, 0.5354856, 0.5374723, 0.5393229, 0.5411325, 0.5425939, 0.5441355};
            expectedAzs = new double[] {1.47659579, 1.761177305, 2.369824981, 2.650872589, 2.912608296, 3.089167765, 3.260313106, 3.318316766, 3.359930795, 3.452888427,
                3.584243325, 3.737730454, 3.974219109, 4.116028485, 4.365236943, 4.559409317, 4.819457357, 5.044483465, 5.254094033, 5.459060684};

            //xs = new double[] {0.5138584, 0.5163397, 0.5186505, 0.5202093, 0.5217203};
            //expectedAzs = new double[] {1.47659579, 1.761177305, 2.369824981, 2.650872589, 2.912608296};

            CheckSequence(xs, expectedAzs, null, false);

            #endregion

            #region "Check stable sequence with blink but continues cleanly after blink"

            logger.Trace("Setting pre-blink history ...");
            azHistory.Clear();
            azHistory.PushFront(-13.26180442);
            azHistory.PushFront(-13.13666784);
            azHistory.PushFront(-13.2116841);
            azHistory.PushFront(-13.2964639);
            azHistory.PushFront(-13.44816142);
            azHistory.PushFront(-12.92569808);
            azHistory.PushFront(-13.51563414);
            azHistory.PushFront(-13.89753404);
            azHistory.PushFront(-14.19973979);
            azHistory.PushFront(-14.4159198);

            logger.Trace("Truncating history to 5 items ...");
            SetFilterLength(5);

            double[] blinkXs = { 0.3657249, 0.3666984, 0.3649366, 0.3654014, 0.3657431, 0.3656037, 0.3642501, 0.3648525, 0.3645380, 0.3633062,
                                   0.3609176, 0.0000000, 0.3596519, 0.3599565, 0.3588960, 0.3579062, 0.3576897, 0.3591176, 0.3598267, 0.3608069,
                                   0.3612605, 0.3610926, 0.3605227, 0.3591320, 0.3585598, 0.3591140, 0.3590765, 0.3590551, 0.3588908, 0.3579985,
                                   0.3580236, 0.3575453, 0.3575625, 0.3568329, 0.3567182, 0.3569818, 0.3575273, 0.3577892 };

            // note that "0" should trigger reply with stale data:
            //double[] expectedBlinkAzs = { -14.40870474, -14.29844002, -14.49799254, -14.44534638, -14.40664329, -14.42243261, -14.57574984, -14.50751824, -14.54314048, -14.68266188,
            //                                -14.95320970, -14.95320970, -15.09657083, -15.06206992, -15.18218880, -15.29429976, -15.31882191, -15.15708900, -15.07677188, -14.96574828,
            //                                -14.91437070, -14.93338810, -14.99793855, -15.15545796, -15.22026892, -15.15749675, -15.16174424, -15.16416814, -15.18277779, -15.28384528,
            //                                -15.28100230, -15.33517756, -15.33322938, -15.41586845, -15.42886010, -15.39900311, -15.33721635, -15.30755192 };
            double[] expectedBlinkAzs = { -14.19973979, -14.29844000, -14.40870472, -14.41591980, -14.40870472, -14.42243259, -14.44534636, -14.44534636, -14.50751822, -14.54314046,
                                            -14.57574982, -14.68266186, -14.95320968, -14.95320968, -15.06206990, -15.09657081, -15.18218879, -15.18218879, -15.18218879, -15.15708898,
                                            -15.07677186, -14.96574826, -14.96574826, -14.96574826, -14.99793854, -15.15545794, -15.15749674, -15.16174422, -15.16416812, -15.16416812,
                                            -15.18277777, -15.28100228, -15.28384527, -15.33322936, -15.33517754, -15.39900309, -15.39900309, -15.39900309 };
            HaythamGazeQuality[] expectedQs = { (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5,
                                            (HaythamGazeQuality)5, (HaythamGazeQuality)3, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5,
                                            (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5,
                                            (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5, (HaythamGazeQuality)5};

            CheckSequence(blinkXs, expectedBlinkAzs, expectedQs, false);

            #endregion

            #region "Sequence with rapid motions, then blink, moving (during and) after blink"

            logger.Trace("Setting pre-moving blink history ...");
            ResetDataCaches();

            azHistory.PushFront(-7.530451638);
            azHistory.PushFront(-7.331533406);
            azHistory.PushFront(-7.019303594);
            azHistory.PushFront(-6.591678100);
            azHistory.PushFront(-6.057287816);
            azHistory.PushFront(-5.496948229);
            azHistory.PushFront(-4.938975907);
            azHistory.PushFront(-4.216439014);
            azHistory.PushFront(-3.394669538);
            azHistory.PushFront(-2.527548246);

            logger.Trace("Truncating history to 5 items ...");
            SetFilterLength(5);

            double[] movingBlinkXs =
            {
                //0.4264513, 0.4282075, 0.4309641, 0.4347395, 0.4394575, 0.4444046, 0.4493308, 0.4557099, 0.4629651, 0.4706207,
                0.4779998, 0.4842189, 0.4890078, 0.4922176, 0.4936712, 0.4934250, 0.4924322, 0.4898562, 0.4875953, 0.4857041,
                0.4628564, 0.0000000, 0.4330998, 0.4316754, 0.4313847, 0.4310622, 0.4309180, 0.4308968, 0.4307165, 0.4304926,
                0.4302593, 0.4300326, 0.4298155, 0.4296461, 0.4294427, 0.4383036, 0.4399976, 0.4399973, 0.4396003, 0.4394683,
                0.4394953, 0.4804201, 0.4904121, 0.4880968, 0.4845622, 0.4801747, 0.4747473, 0.4682953, 0.4607433, 0.4535584,
                0.4397716, 0.4314806, 0.4265408, 0.4215191, 0.4169365, 0.4137174, 0.4101309, 0.4067964, 0.4140276, 0.4063472,
                0.3987452, 0.3905757, 0.3902963, 0.3891869, 0.3864403, 0.3883175, 0.3901146, 0.3899935, 0.3855664, 0.3814458,
                0.3801391, 0.3800225, 0.3803555, 0.3804781, 0.3809465, 0.3809878, 0.3813349, 0.3812773, 0.3816569, 0.3812183,
                0.3814010, 0.3809909, 0.3810317, 0.3812699, 0.3811764, 0.3810124, 0.3807948, 0.3808262, 0.3808635, 0.3810541,
                0.3809202, 0.3815457, 0.3851177, 0.3850343, 0.3846329, 0.3847834, 0.3857568, 0.3847169, 0.3774559, 0.3772132,
                0.3785387, 0.3787187, 0.3785662, 0.3782034, 0.3781696, 0.3801933, 0.3858552, 0.3918057, 0.3975449, 0.4038655,
                0.4105729, 0.4191515, 0.4283109, 0.4372505, 0.4468256, 0.4568164, 0.4664382, 0.4773759, 0.4889574, 0.5013967,
                0.5153946, 0.5295724, 0.5454245, 0.5610677, 0.5769344, 0.5918632, 0.6056890, 0.6180733, 0.6174897, 0.6283619,
                0.6364396 };

            // note that "0" should trigger reply with stale data:
            //double[] expectedMovingBlinkAzs =
            //{
            //    //-7.530451638, -7.331533406, -7.019303594, -6.591678100, -6.057287816, -5.496948229, -4.938975907, -4.216439014, -3.394669538, -2.527548246,
            //    -1.691745078, -0.987330790, -0.444909927, -0.081347838, 0.0832960180, 0.0554098620, -0.057040896, -0.348814819, -0.604898540, -0.819107718,
            //    -3.406981582, -3.406981582, -6.777400811, -6.938737292, -6.971663799, -7.008192172, -7.024525169, -7.026926414, -7.047348323, -7.072708642,
            //    -7.099133664, -7.124811128, -7.149401237, -7.168588544, -7.191626904, -6.187985771, -5.996112701, -5.996146681, -6.041113392, -6.056064541,
            //    -6.053006351, -1.417606714, -0.285850097, -0.548095503, -0.948446478, -1.445402258, -2.060143637, -2.790937641, -3.646324547, -4.460131404,
            //    -6.021710880, -6.960801563, -7.520314306, -8.089103558, -8.608157588, -8.972773053, -9.379002547, -9.756688940, -8.937637854, -9.807568150,
            //    -10.66861837, -11.59394720, -11.62559380, -11.75125140, -12.06234855, -11.84972510, -11.64617428, -11.65989083, -12.16133195, -12.62805696,
            //    -12.77606200, -12.78926885, -12.75155118, -12.73766474, -12.68461081, -12.67993292, -12.64061819, -12.64714233, -12.60414645, -12.65382504,
            //    -12.63313129, -12.67958179, -12.67496053, -12.64798050, -12.65857090, -12.67714657, -12.70179331, -12.69823675, -12.69401191, -12.67242336,
            //    -12.68758972, -12.61674166, -12.21215453, -12.22160094, -12.26706602, -12.25001944, -12.13976605, -12.25755165, -13.07997807, -13.10746780,
            //    -12.95733335, -12.93694542, -12.95421853, -12.99531153, -12.99913993, -12.76992297, -12.12862065, -11.45462968, -10.80457188, -10.08866106,
            //    -9.328938854, -8.357272790, -7.319821674, -6.307266485, -5.222730578, -4.091109881, -3.001284440, -1.762411907, -0.450618547, 0.9583346230,
            //    2.5438246090, 4.1496911980, 5.9451995110, 7.7170464990, 9.5142084990, 11.205138060, 12.771134920, 14.173858450, 14.107756250, 15.339209840,
            //    16.254140830 };
            double[] expectedMovingBlinkAzs =
            {
                -3.394669538, -2.527548246, -1.691745057, -0.987330768, -0.444909905, -0.081347816, -0.057040874, -0.057040874, -0.057040874, -0.348814797,
                -0.604898518, -0.819107696, -3.406981561, -3.406981561, -6.777400791, -6.938737272, -6.971663778, -7.008192152, -7.024525149, -7.026926394,
                -7.047348303, -7.072708622, -7.099133644, -7.124811108, -7.149401217, -7.149401217, -7.149401217, -6.187985750, -6.041113372, -6.041113372,
                -6.041113372, -6.041113372, -6.041113372, -1.417606692, -0.948446456, -0.948446456, -0.948446456, -1.445402236, -2.060143615, -2.790937619,
                -3.646324526, -4.460131383, -6.021710859, -6.960801543, -7.520314286, -8.089103538, -8.608157569, -8.972773033, -8.972773033, -9.379002528,
                -9.756688921, -9.807568131, -10.66861835, -11.59394718, -11.62559378, -11.75125138, -11.75125138, -11.75125138, -11.84972508, -11.84972508,
                -12.16133193, -12.62805694, -12.75155116, -12.75155116, -12.75155116, -12.73766472, -12.68461079, -12.67993290, -12.64714231, -12.64714231,
                -12.64061817, -12.64714231, -12.65382502, -12.65382502, -12.65857088, -12.67496051, -12.67496051, -12.67714655, -12.69401190, -12.69401190,
                -12.69401190, -12.68758970, -12.67242334, -12.61674164, -12.26706600, -12.25001943, -12.22160092, -12.25001943, -12.25755163, -12.25755163,
                -12.95733333, -12.95733333, -12.95733333, -12.95733333, -12.95733333, -12.95421851, -12.95421851, -12.76992295, -12.12862063, -11.45462966,
                -10.80457186, -10.08866105, -9.328938834, -8.357272770, -7.319821654, -6.307266464, -5.222730557, -4.091109860, -3.001284418, -1.762411885,
                -0.450618525, 0.958334646, 2.543824632, 4.149691222, 5.945199535, 7.717046523, 9.514208524, 11.20513809, 12.77113495, 14.10775627,
                14.17385847 };

            HaythamGazeQuality[] expectedMovingBlinkQs = new HaythamGazeQuality[expectedMovingBlinkAzs.Length];
            for (int i = 0; i < expectedMovingBlinkAzs.Length; i++)
            {
                if ((i > 1) && (movingBlinkXs[i] == 0.0))
                {
                    expectedMovingBlinkQs[i] = HaythamGazeQuality.BothEyesStale;
                }
                else
                {
                    expectedMovingBlinkQs[i] = HaythamGazeQuality.Eye1Current;
                }
                //logger.Trace("i: {0} q: {1}", i, expectedMovingBlinkQs[i]);
            }

            CheckSequence(movingBlinkXs, expectedMovingBlinkAzs, expectedMovingBlinkQs, true);

            #endregion


            logger.Trace("Exiting test ...");
            TestSetEyeData(false, 0, 0.0, 0.0, false, 0, 0.0, 0.0);
        }

        private void TestBinocularSequence()
        {
            bool allAsExpected = true;

            #region "Edited data from log file"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.367204", "0"},
                {"Eye2", "45", "0.5272527", "0"},
                {"Eye1", "35", "0.371001", "0"},
                {"Eye2", "45", "0.5265834", "0"},
                {"Eye1", "35", "0.3688995", "0"},
                {"Eye2", "45", "0.5265391", "0"},
                {"Eye1", "35", "0.3718939", "0"},
                {"Eye2", "45", "0.5263407", "0"},
                {"Eye1", "35", "0.3664173", "0"},
                {"Eye2", "45", "0.5259122", "0"},
                {"Eye1", "35", "0.3660227", "0"},
                {"Eye2", "45", "0.5257889", "0"},
                {"Eye1", "35", "0.3660599", "0"},
                {"Eye2", "45", "0.5256888", "0"},
                {"Eye1", "35", "0.3636182", "0"},
                {"Eye2", "45", "0.5229677", "0"},
                {"Eye1", "35", "0.3623708", "0"},
                {"Eye2", "45", "0.5151721", "0"},
                {"Eye1", "35", "0.3615527", "0"},
                {"Eye2", "45", "0.514671", "0"},
                {"Eye1", "35", "0.361803", "0"},
                {"Eye2", "45", "0.5145577", "0"},
                {"Eye1", "35", "0.3606822", "0"},
                {"Eye2", "45", "0.5173606", "0"},
                {"Eye1", "35", "0.3605079", "0"},
                {"Eye2", "45", "0.5175408", "0"},
                {"Eye1", "35", "0.361185", "0"},
                {"Eye2", "45", "0.5173385", "0"},
                {"Eye1", "35", "0.3633041", "0"},
                {"Eye2", "45", "0.5058822", "0"},
                {"Eye1", "35", "0.3646826", "0"},
                {"Eye2", "45", "0.5060856", "0"},
                {"Eye1", "35", "0.3645433", "0"},
                {"Eye2", "45", "0.5066952", "0"},
                {"Eye1", "35", "0.3688846", "0"},
                {"Eye2", "45", "0.5065088", "0"},
                {"Eye1", "35", "0.3671939", "0"},
                {"Eye2", "45", "0.5151297", "0"},
                {"Eye1", "35", "0.3643062", "0"},
                {"Eye2", "45", "0.5148128", "0"},
                {"Eye1", "35", "0.3636773", "0"},
                {"Eye2", "45", "0.514657", "0"},
                {"Eye1", "35", "0.3655139", "0"},
                {"Eye2", "45", "0.5142359", "0"},
                {"Eye1", "35", "0.3643491", "0"},
                {"Eye2", "45", "0.5142475", "0"},
                {"Eye1", "35", "0.3641974", "0"},
                {"Eye2", "45", "0.5141453", "0"},
                {"Eye1", "35", "0.3619969", "0"},
                {"Eye2", "45", "0.5144364", "0"},
                {"Eye1", "35", "0.3613615", "0"},
                {"Eye2", "45", "0.5374878", "0"},
                {"Eye1", "35", "0.2014851", "0"},
                {"Eye2", "45", "0.5468818", "0"},
                {"Eye1", "35", "0.2602437", "0"},
                {"Eye2", "45", "0.5485543", "0"},
                {"Eye1", "35", "0.2552892", "0"},
                {"Eye2", "45", "0.5481974", "0"},
                {"Eye1", "35", "0.2488215", "0"},
                {"Eye2", "45", "0.5481452", "0"},
                {"Eye1", "35", "0.2485543", "0"},
                {"Eye2", "45", "0.5480933", "0"},
                {"Eye1", "35", "0.2522355", "0"},
                {"Eye2", "45", "0.5479835", "0"},
                {"Eye1", "35", "0.2542964", "0"},
                {"Eye2", "45", "0.5479906", "0"},
                {"Eye1", "35", "0.2592428", "0"},
                {"Eye2", "45", "0.5486127", "0"},
                {"Eye1", "35", "0.2553895", "0"},
                {"Eye2", "45", "0.5487402", "0"},
            };
            #endregion

            #region "Calculated azs using spreadsheet"
            double[] expectedPerEyeAzs = {
                14.17435602, 0.040949181, 14.84280741, 0.509449811, 14.47284413,
                0.540459191, 15.00000000, 0.679336413, 14.03585965, 0.979280414,
                13.96639141, 1.065588688, 13.97294036, 1.135657287, 13.54308580,
                3.040389192, 13.32348447, 8.497200056, 13.17946022, 8.847963041,
                13.22352484, 8.927271455, 13.02621109, 6.965280694, 12.99552605,
                6.839143217, 13.11472764, 6.980750385, 13.48778936, 15.00000000,
                13.73047049, 14.85762285, 13.70594711, 14.43091138, 14.47022102,
                14.56138877, 14.17257794, 8.526879462, 13.66420631, 8.748705026,
                13.55349020, 8.857762845, 13.87681857, 9.152526949, 13.67175874,
                9.144407112, 13.64505237, 9.215945681, 13.25766040, 9.012179756,
                13.14579998, -7.12347753, -15.00000000, -13.69914602, -4.655710268,
                -14.86987260, -5.527936351, -14.62004760, -6.666557126, -14.58350833,
                -6.713596950, -14.54717906, -6.065531827, -14.47032059, -5.702716057,
                -14.47529049, -4.831915957, -14.91075178, -5.510278812, -15.00000000
            };
            #endregion

            #region "Median-filtered in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                14.17435602, 10.64100431, 7.441878297, 7.559003455, 7.491146968,
                7.491146968, 7.506651658, 7.676128612, 7.506651658, 7.507570032,
                7.507570032, 7.507570032, 7.507570032, 7.515990048, 7.515990048,
                7.519264526, 7.554298826, 8.181936830, 8.291737498, 10.83833014,
                10.91034226, 11.01371163, 11.01371163, 11.01371163, 10.97674127,
                9.995745891, 9.980403374, 9.980403374, 9.980403374, 10.04773901,
                10.23426987, 14.24389468, 14.28178498, 14.28178498, 14.29404667,
                14.29404667, 14.36698336, 14.36698336, 14.36698336, 11.34972870,
                11.20645567, 11.20562652, 11.20562652, 11.20645567, 11.36729071,
                11.40808292, 11.40808292, 11.41214284, 11.40808292, 11.39472974,
                11.23680304, 11.13492008, 11.07898987, 3.011161226, -9.177428143,
                -9.762791436, -10.19890448, -10.07399197, -10.07399197, -10.19890448,
                -10.62503273, -10.63038800, -10.63038800, -10.62503273, -10.30635544,
                -10.26792621, -10.08900328, -10.08651833, -10.08651833, -10.08900328
            };
            #endregion

            logger.Trace("Processing binocular sequence - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.47089704, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < 15; row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // early part - no divergence expected and no other quirks: compare calculated individual azimuths with ones from spreadsheet
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs /*expectedPerEyeAzs*/, row, azEl_data) && allAsExpected;

                if (! allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private static bool logBinocularAzimuths(double[] expectedAzs, int row, Double[] azEl_data, bool trace = false)
        {
            // hard-coded as within 1 degree
            bool expectedCloseToCalculated;

            if (trace)
            {
                logger.Trace("row: {2} expected filtered mean az: {0} calculated az {1}", expectedAzs[row], azEl_data[1], row);
            }
            expectedCloseToCalculated = Math.Abs(expectedAzs[row] - azEl_data[1]) < 1.0;

            return expectedCloseToCalculated;
        }

        private void TestBinocularSequenceMove()
        {
            bool allAsExpected = true;

            #region "Edited data from log file"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.367204", "0"},
                {"Eye2", "45", "0.5272527", "0"},
                {"Eye1", "35", "0.371001", "0"},
                {"Eye2", "45", "0.5265834", "0"},
                {"Eye1", "35", "0.3688995", "0"},
                {"Eye2", "45", "0.5265391", "0"},
                {"Eye1", "35", "0.3718939", "0"},
                {"Eye2", "45", "0.5263407", "0"},
                {"Eye1", "35", "0.3664173", "0"},
                {"Eye2", "45", "0.5259122", "0"},
                {"Eye1", "35", "0.3660227", "0"},
                {"Eye2", "45", "0.5257889", "0"},
                {"Eye1", "35", "0.3660599", "0"},
                {"Eye2", "45", "0.5256888", "0"},
                {"Eye1", "35", "0.3636182", "0"},
                {"Eye2", "45", "0.5229677", "0"},
                {"Eye1", "35", "0.3623708", "0"},
                {"Eye2", "45", "0.5151721", "0"},
                {"Eye1", "35", "0.3615527", "0"},
                {"Eye2", "45", "0.514671", "0"},
                {"Eye1", "35", "0.361803", "0"},
                {"Eye2", "45", "0.5145577", "0"},
                {"Eye1", "35", "0.3606822", "0"},
                {"Eye2", "45", "0.5173606", "0"},
                {"Eye1", "35", "0.3605079", "0"},
                {"Eye2", "45", "0.5175408", "0"},
                {"Eye1", "35", "0.361185", "0"},
                {"Eye2", "45", "0.5173385", "0"},
                {"Eye1", "35", "0.3633041", "0"},
                {"Eye2", "45", "0.5058822", "0"},
                {"Eye1", "35", "0.3646826", "0"},
                {"Eye2", "45", "0.5060856", "0"},
                {"Eye1", "35", "0.3645433", "0"},
                {"Eye2", "45", "0.5066952", "0"},
                {"Eye1", "35", "0.3688846", "0"},
                {"Eye2", "45", "0.5065088", "0"},
                {"Eye1", "35", "0.3671939", "0"},
                {"Eye2", "45", "0.5151297", "0"},
                {"Eye1", "35", "0.3643062", "0"},
                {"Eye2", "45", "0.5148128", "0"},
                {"Eye1", "35", "0.3636773", "0"},
                {"Eye2", "45", "0.514657", "0"},
                {"Eye1", "35", "0.3655139", "0"},
                {"Eye2", "45", "0.5142359", "0"},
                {"Eye1", "35", "0.3643491", "0"},
                {"Eye2", "45", "0.5142475", "0"},
                {"Eye1", "35", "0.3641974", "0"},
                {"Eye2", "45", "0.5141453", "0"},
                {"Eye1", "35", "0.3619969", "0"},
                {"Eye2", "45", "0.5144364", "0"},
                {"Eye1", "35", "0.3613615", "0"},
                {"Eye2", "45", "0.5374878", "0"},
                {"Eye1", "35", "0.2014851", "0"},
                {"Eye2", "45", "0.5468818", "0"},
                {"Eye1", "35", "0.2602437", "0"},
                {"Eye2", "45", "0.5485543", "0"},
                {"Eye1", "35", "0.2552892", "0"},
                {"Eye2", "45", "0.5481974", "0"},
                {"Eye1", "35", "0.2488215", "0"},
                {"Eye2", "45", "0.5481452", "0"},
                {"Eye1", "35", "0.2485543", "0"},
                {"Eye2", "45", "0.5480933", "0"},
                {"Eye1", "35", "0.2522355", "0"},
                {"Eye2", "45", "0.5479835", "0"},
                {"Eye1", "35", "0.2542964", "0"},
                {"Eye2", "45", "0.5479906", "0"},
                {"Eye1", "35", "0.2592428", "0"},
                {"Eye2", "45", "0.5486127", "0"},
                {"Eye1", "35", "0.2553895", "0"},
                {"Eye2", "45", "0.5487402", "0"},
            };
            #endregion

            #region "Calculated azs using spreadsheet"
            double[] expectedPerEyeAzs = {
                14.17435602, 0.040949181, 14.84280741, 0.509449811, 14.47284413,
                0.540459191, 15.00000000, 0.679336413, 14.03585965, 0.979280414,
                13.96639141, 1.065588688, 13.97294036, 1.135657287, 13.54308580,
                3.040389192, 13.32348447, 8.497200056, 13.17946022, 8.847963041,
                13.22352484, 8.927271455, 13.02621109, 6.965280694, 12.99552605,
                6.839143217, 13.11472764, 6.980750385, 13.48778936, 15.00000000,
                13.73047049, 14.85762285, 13.70594711, 14.43091138, 14.47022102,
                14.56138877, 14.17257794, 8.526879462, 13.66420631, 8.748705026,
                13.55349020, 8.857762845, 13.87681857, 9.152526949, 13.67175874,
                9.144407112, 13.64505237, 9.215945681, 13.25766040, 9.012179756,
                13.14579998, -7.12347753, -15.00000000, -13.69914602, -4.655710268,
                -14.86987260, -5.527936351, -14.62004760, -6.666557126, -14.58350833,
                -6.713596950, -14.54717906, -6.065531827, -14.47032059, -5.702716057,
                -14.47529049, -4.831915957, -14.91075178, -5.510278812, -15.00000000
            };
            #endregion

            #region "Median-filtered in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                14.17435602, 10.64100431, 7.441878297, 7.559003455, 7.491146968,
                7.491146968, 7.506651658, 7.676128612, 7.506651658, 7.507570032,
                7.507570032, 7.507570032, 7.507570032, 7.515990048, 7.515990048,
                7.519264526, 7.554298826, 8.181936830, 8.291737498, 10.83833014,
                10.91034226, 11.01371163, 11.01371163, 11.01371163, 10.97674127,
                9.995745891, 9.980403374, 9.980403374, 9.980403374, 10.04773901,
                10.23426987, 14.24389468, 14.28178498, 14.28178498, 14.29404667,
                14.29404667, 14.36698336, 14.36698336, 14.36698336, 11.34972870,
                11.20645567, 11.20562652, 11.20562652, 11.20645567, 11.36729071,
                11.40808292, 11.40808292, 11.41214284, 11.40808292, 11.39472974,
                11.23680304, 11.13492008, 11.07898987, 3.011161226, -9.177428143,
                -9.762791436, -10.19890448, -10.07399197, -10.07399197, -10.19890448,
                -10.62503273, -10.63038800, -10.63038800, -10.62503273, -10.30635544,
                -10.26792621, -10.08900328, -10.08651833, -10.08651833, -10.08900328
            };
            #endregion

            logger.Trace("Processing binocular sequence - rapid move - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.47089704, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            bool expectedDivergence, calculatedDivergence;

            for (int row = 0; row < 40; row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az after a couple of video frames, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // early part - no divergence expected, a rapid move aroudn row 17
                // compare calculated individual azimuths with ones from spreadsheet
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs /*expectedPerEyeAzs*/, row, azEl_data, false) && allAsExpected;

                // check whether azs diverged from each other?
                expectedDivergence = Math.Abs(expectedPerEyeAzs[row] - expectedPerEyeAzs[row - 1]) > allowedDeviation;
                calculatedDivergence = Math.Abs(azEl_data[2] - azEl_data[3]) > allowedDeviation;
                //logger.Trace("row: {2} expected azs diverged: {0} calculated diverged: {1}", expectedDivergence, calculatedDivergence, row);

                allAsExpected = (expectedDivergence == false) && (calculatedDivergence == expectedDivergence) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSequenceDivergence()
        {
            bool allAsExpected = true;

            #region "Edited data from log file"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.367204", "0"},
                {"Eye2", "45", "0.5272527", "0"},
                {"Eye1", "35", "0.371001", "0"},
                {"Eye2", "45", "0.5265834", "0"},
                {"Eye1", "35", "0.3688995", "0"},
                {"Eye2", "45", "0.5265391", "0"},
                {"Eye1", "35", "0.3718939", "0"},
                {"Eye2", "45", "0.5263407", "0"},
                {"Eye1", "35", "0.3664173", "0"},
                {"Eye2", "45", "0.5259122", "0"},
                {"Eye1", "35", "0.3660227", "0"},
                {"Eye2", "45", "0.5257889", "0"},
                {"Eye1", "35", "0.3660599", "0"},
                {"Eye2", "45", "0.5256888", "0"},
                {"Eye1", "35", "0.3636182", "0"},
                {"Eye2", "45", "0.5229677", "0"},
                {"Eye1", "35", "0.3623708", "0"},
                {"Eye2", "45", "0.5151721", "0"},
                {"Eye1", "35", "0.3615527", "0"},
                {"Eye2", "45", "0.514671", "0"},
                {"Eye1", "35", "0.361803", "0"},
                {"Eye2", "45", "0.5145577", "0"},
                {"Eye1", "35", "0.3606822", "0"},
                {"Eye2", "45", "0.5173606", "0"},
                {"Eye1", "35", "0.3605079", "0"},
                {"Eye2", "45", "0.5175408", "0"},
                {"Eye1", "35", "0.361185", "0"},
                {"Eye2", "45", "0.5173385", "0"},
                {"Eye1", "35", "0.3633041", "0"},
                {"Eye2", "45", "0.5058822", "0"},
                {"Eye1", "35", "0.3646826", "0"},
                {"Eye2", "45", "0.5060856", "0"},
                {"Eye1", "35", "0.3645433", "0"},
                {"Eye2", "45", "0.5066952", "0"},
                {"Eye1", "35", "0.3688846", "0"},
                {"Eye2", "45", "0.5065088", "0"},
                {"Eye1", "35", "0.3671939", "0"},
                {"Eye2", "45", "0.5151297", "0"},
                {"Eye1", "35", "0.3643062", "0"},
                {"Eye2", "45", "0.5148128", "0"},
                {"Eye1", "35", "0.3636773", "0"},
                {"Eye2", "45", "0.514657", "0"},
                {"Eye1", "35", "0.3655139", "0"},
                {"Eye2", "45", "0.5142359", "0"},
                {"Eye1", "35", "0.3643491", "0"},
                {"Eye2", "45", "0.5142475", "0"},
                {"Eye1", "35", "0.3641974", "0"},
                {"Eye2", "45", "0.5141453", "0"},
                {"Eye1", "35", "0.3619969", "0"},
                {"Eye2", "45", "0.5144364", "0"},
                {"Eye1", "35", "0.3613615", "0"},
                {"Eye2", "45", "0.5374878", "0"},
                {"Eye1", "35", "0.2014851", "0"},
                {"Eye2", "45", "0.5468818", "0"},
                {"Eye1", "35", "0.2602437", "0"},
                {"Eye2", "45", "0.5485543", "0"},
                {"Eye1", "35", "0.2552892", "0"},
                {"Eye2", "45", "0.5481974", "0"},
                {"Eye1", "35", "0.2488215", "0"},
                {"Eye2", "45", "0.5481452", "0"},
                {"Eye1", "35", "0.2485543", "0"},
                {"Eye2", "45", "0.5480933", "0"},
                {"Eye1", "35", "0.2522355", "0"},
                {"Eye2", "45", "0.5479835", "0"},
                {"Eye1", "35", "0.2542964", "0"},
                {"Eye2", "45", "0.5479906", "0"},
                {"Eye1", "35", "0.2592428", "0"},
                {"Eye2", "45", "0.5486127", "0"},
                {"Eye1", "35", "0.2553895", "0"},
                {"Eye2", "45", "0.5487402", "0"},
            };
            #endregion

            #region "Calculated azs using spreadsheet"
            double[] expectedPerEyeAzs = {
                14.17435602, 0.040949181, 14.84280741, 0.509449811, 14.47284413,
                0.540459191, 15.00000000, 0.679336413, 14.03585965, 0.979280414,
                13.96639141, 1.065588688, 13.97294036, 1.135657287, 13.54308580,
                3.040389192, 13.32348447, 8.497200056, 13.17946022, 8.847963041,
                13.22352484, 8.927271455, 13.02621109, 6.965280694, 12.99552605,
                6.839143217, 13.11472764, 6.980750385, 13.48778936, 15.00000000,
                13.73047049, 14.85762285, 13.70594711, 14.43091138, 14.47022102,
                14.56138877, 14.17257794, 8.526879462, 13.66420631, 8.748705026,
                13.55349020, 8.857762845, 13.87681857, 9.152526949, 13.67175874,
                9.144407112, 13.64505237, 9.215945681, 13.25766040, 9.012179756,
                13.14579998, -7.12347753, -15.00000000, -13.69914602, -4.655710268,
                -14.86987260, -5.527936351, -14.62004760, -6.666557126, -14.58350833,
                -6.713596950, -14.54717906, -6.065531827, -14.47032059, -5.702716057,
                -14.47529049, -4.831915957, -14.91075178, -5.510278812, -15.00000000
            };
            #endregion

            #region "Median-filtered in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                14.17435602, 10.64100431, 7.441878297, 7.559003455, 7.491146968,
                7.491146968, 7.506651658, 7.676128612, 7.506651658, 7.507570032,
                7.507570032, 7.507570032, 7.507570032, 7.515990048, 7.515990048,
                7.519264526, 7.554298826, 8.181936830, 8.291737498, 10.83833014,
                10.91034226, 11.01371163, 11.01371163, 11.01371163, 10.97674127,
                9.995745891, 9.980403374, 9.980403374, 9.980403374, 10.04773901,
                10.23426987, 14.24389468, 14.28178498, 14.28178498, 14.29404667,
                14.29404667, 14.36698336, 14.36698336, 14.36698336, 11.34972870,
                11.20645567, 11.20562652, 11.20562652, 11.20645567, 11.36729071,
                11.40808292, 11.40808292, 11.41214284, 11.40808292, 11.39472974,
                11.23680304, 11.23680304, 11.13492008, 11.07898987, -9.177428143,
                -9.762791436, -10.19890448, -10.07399197, -10.07399197, -10.19890448,
                -10.62503273, -10.63038800, -10.63038800, -10.62503273, -10.30635544,
                -10.26792621, -10.08900328, -10.08651833, -10.08651833, -10.08900328
            };
            #endregion

            logger.Trace("Processing binocular sequence - divergence - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.47089704, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            bool expectedDivergence, calculatedDivergence;

            for (int row = 0; row < 60; row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az after a couple of video frames, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // early part - should be normal
                // compare calculated individual azimuths with ones from spreadsheet
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs /*expectedPerEyeAzs*/, row, azEl_data, false) && allAsExpected;

                // check whether azs diverged from each other?
                expectedDivergence = Math.Abs(expectedPerEyeAzs[row] - expectedPerEyeAzs[row - 1]) > allowedDeviation;
                calculatedDivergence = Math.Abs(azEl_data[2] - azEl_data[3]) > allowedDeviation;
                //logger.Trace("row: {2} expected azs diverged: {0} calculated diverged: {1}", expectedDivergence, calculatedDivergence, row);

                // divergence expected here!
                if (row == 51)
                {
                    allAsExpected = (expectedDivergence == true) && (calculatedDivergence == expectedDivergence) && allAsExpected;
                }

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSequenceSteadyFlickMedian5()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.285979463333333", "0"},
                {"Eye2", "45", "0.527489775", "0"},
                {"Eye1", "35", "0.2870729198", "0"},
                {"Eye2", "45", "0.5272147695", "0"},
                {"Eye1", "35", "0.286746302933333", "0"},
                {"Eye2", "45", "0.527296914", "0"},
                {"Eye1", "35", "0.2860930692", "0"},
                {"Eye2", "45", "0.527461203", "0"},
                {"Eye1", "35", "0.286675299266667", "0"},
                {"Eye2", "45", "0.5273147715", "0"},
                {"Eye1", "35", "0.2863060802", "0"},
                {"Eye2", "45", "0.5274076305", "0"},
                {"Eye1", "35", "0.286192474333333", "0"},
                {"Eye2", "45", "0.5274362025", "0"},
                {"Eye1", "35", "0.2867321022", "0"},
                {"Eye2", "45", "0.5273004855", "0"},
                {"Eye1", "35", "0.286277678733333", "0"},
                {"Eye2", "45", "0.5274147735", "0"},
                {"Eye1", "35", "0.287229127866667", "0"},
                {"Eye2", "45", "0.527175483", "0"},
                {"Eye1", "35", "0.343080612066667", "0"},
                {"Eye2", "45", "0.5131287735", "0"},
                {"Eye1", "35", "0.286291879466667", "0"},
                {"Eye2", "45", "0.527411202", "0"},
                {"Eye1", "35", "0.286575894133333", "0"},
                {"Eye2", "45", "0.527339772", "0"},
                {"Eye1", "35", "0.287229127866667", "0"},
                {"Eye2", "45", "0.527175483", "0"},
                {"Eye1", "35", "0.2861356714", "0"},
                {"Eye2", "45", "0.5274504885", "0"},
                {"Eye1", "35", "0.286263478", "0"},
                {"Eye2", "45", "0.527418345", "0"},
                {"Eye1", "35", "0.286377083866667", "0"},
                {"Eye2", "45", "0.527389773", "0"},
                {"Eye1", "35", "0.2866895", "0"},
                {"Eye2", "45", "0.5273112", "0"},
                {"Eye1", "35", "0.286320280933333", "0"},
                {"Eye2", "45", "0.527404059", "0"},
                {"Eye1", "35", "0.286334481666667", "0"},
                {"Eye2", "45", "0.5274004875", "0"},
                {"Eye1", "35", "0.286504890466667", "0"},
                {"Eye2", "45", "0.5273576295", "0"},
                {"Eye1", "35", "0.286448087533333", "0"},
                {"Eye2", "45", "0.5273719155", "0"},
                {"Eye1", "35", "0.286192474333333", "0"},
                {"Eye2", "45", "0.5274362025", "0"},
                {"Eye1", "35", "0.286888310266667", "0"},
                {"Eye2", "45", "0.527261199", "0"},
                {"Eye1", "35", "0.2870729198", "0"},
                {"Eye2", "45", "0.5272147695", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -0.1250, -0.1250, 0.0675, 0.0675, 0.0100, 0.0100, -0.1050, -0.1050, -0.0025, -0.0025,
                -0.0675, -0.0675, -0.0875, -0.0875, 0.0075, 0.0075, -0.0725, -0.0725, 0.0950, 0.0950,
                9.9275, 9.9275, -0.0700, -0.0700, -0.0200, -0.0200, 0.0950, 0.0950, -0.0975, -0.0975,
                -0.0750, -0.0750, -0.0550, -0.0550, 0.0000, 0.0000, -0.0650, -0.0650, -0.0625, -0.0625,
                -0.0325, -0.0325, -0.0425, -0.0425, -0.0875, -0.0875, 0.0350, 0.0350, 0.0675, 0.0675
            };
            #endregion

            // NOTE: 5-point median filter on mean azimuths, when polled after every frame,
            // CANNOT reject the 2-eye "flick" away from steady target and back, because the
            // averaging over the most recent positions from both eyes effectively makes the
            // duration of the "flick" extend over several frames
            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -0.1250, -0.1250, -0.1250, -0.076875, -0.02875, 0.0100, 0.0100, 0.0100, -0.0475, -0.0475,
                -0.0475, -0.05375, -0.05375, -0.0675, -0.0675, -0.0675, -0.0400, -0.0400, -0.0325, 0.0075,
                0.01125, 0.0950, 4.92875, 4.92875, 4.92875, -0.0200, -0.0200, -0.0200, -0.00125, -0.00125,
                -0.00125, -0.0750, -0.0750, -0.0750, -0.0650, -0.0550, -0.0325, -0.0325, -0.0325, -0.0625,
                -0.0625, -0.0625, -0.0475, -0.0425, -0.0425, -0.0425, -0.0425, -0.0425, -0.02625, 0.0350
            };
            #endregion

            logger.Trace("Processing binocular sequence - flick to same side - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSequenceSteadyFlickMedian7()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.285979463333333", "0"},
                {"Eye2", "45", "0.527489775", "0"},
                {"Eye1", "35", "0.2870729198", "0"},
                {"Eye2", "45", "0.5272147695", "0"},
                {"Eye1", "35", "0.286746302933333", "0"},
                {"Eye2", "45", "0.527296914", "0"},
                {"Eye1", "35", "0.2860930692", "0"},
                {"Eye2", "45", "0.527461203", "0"},
                {"Eye1", "35", "0.286675299266667", "0"},
                {"Eye2", "45", "0.5273147715", "0"},
                {"Eye1", "35", "0.2863060802", "0"},
                {"Eye2", "45", "0.5274076305", "0"},
                {"Eye1", "35", "0.286192474333333", "0"},
                {"Eye2", "45", "0.5274362025", "0"},
                {"Eye1", "35", "0.2867321022", "0"},
                {"Eye2", "45", "0.5273004855", "0"},
                {"Eye1", "35", "0.286277678733333", "0"},
                {"Eye2", "45", "0.5274147735", "0"},
                {"Eye1", "35", "0.287229127866667", "0"},
                {"Eye2", "45", "0.527175483", "0"},
                {"Eye1", "35", "0.343080612066667", "0"},
                {"Eye2", "45", "0.5131287735", "0"},
                {"Eye1", "35", "0.286291879466667", "0"},
                {"Eye2", "45", "0.527411202", "0"},
                {"Eye1", "35", "0.286575894133333", "0"},
                {"Eye2", "45", "0.527339772", "0"},
                {"Eye1", "35", "0.287229127866667", "0"},
                {"Eye2", "45", "0.527175483", "0"},
                {"Eye1", "35", "0.2861356714", "0"},
                {"Eye2", "45", "0.5274504885", "0"},
                {"Eye1", "35", "0.286263478", "0"},
                {"Eye2", "45", "0.527418345", "0"},
                {"Eye1", "35", "0.286377083866667", "0"},
                {"Eye2", "45", "0.527389773", "0"},
                {"Eye1", "35", "0.2866895", "0"},
                {"Eye2", "45", "0.5273112", "0"},
                {"Eye1", "35", "0.286320280933333", "0"},
                {"Eye2", "45", "0.527404059", "0"},
                {"Eye1", "35", "0.286334481666667", "0"},
                {"Eye2", "45", "0.5274004875", "0"},
                {"Eye1", "35", "0.286504890466667", "0"},
                {"Eye2", "45", "0.5273576295", "0"},
                {"Eye1", "35", "0.286448087533333", "0"},
                {"Eye2", "45", "0.5273719155", "0"},
                {"Eye1", "35", "0.286192474333333", "0"},
                {"Eye2", "45", "0.5274362025", "0"},
                {"Eye1", "35", "0.286888310266667", "0"},
                {"Eye2", "45", "0.527261199", "0"},
                {"Eye1", "35", "0.2870729198", "0"},
                {"Eye2", "45", "0.5272147695", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -0.1250, -0.1250, 0.0675, 0.0675, 0.0100, 0.0100, -0.1050, -0.1050, -0.0025, -0.0025,
                -0.0675, -0.0675, -0.0875, -0.0875, 0.0075, 0.0075, -0.0725, -0.0725, 0.0950, 0.0950,
                9.9275, 9.9275, -0.0700, -0.0700, -0.0200, -0.0200, 0.0950, 0.0950, -0.0975, -0.0975,
                -0.0750, -0.0750, -0.0550, -0.0550, 0.0000, 0.0000, -0.0650, -0.0650, -0.0625, -0.0625,
                -0.0325, -0.0325, -0.0425, -0.0425, -0.0875, -0.0875, 0.0350, 0.0350, 0.0675, 0.0675
            };
            #endregion

            // NOTE: 7-point median filter on mean azimuths, when polled after every frame,
            // REJECTS the 2-eye "flick" away from steady target and back
            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -0.125, -0.1250, -0.1250, -0.076875, -0.02875, -0.009375, -0.02875, -0.02875, -0.02875, -0.0025,
                -0.0350, -0.0475, -0.05375, -0.0675, -0.05375, -0.0400, -0.0400, -0.0675, -0.0400, -0.0325,
                0.0075, 0.01125, 0.0950, 0.0950, 0.0950, 0.0950, 0.0375, 0.0375, -0.00125, -0.0200,
                -0.0200, -0.0200, -0.0650, -0.0650, -0.0650, -0.0650, -0.0550, -0.0550, -0.0550, -0.0550,
                -0.0475, -0.0475, -0.0475, -0.0475, -0.0475, -0.0475, -0.0425, -0.0375, -0.0375, -0.02625
            };
            #endregion

            logger.Trace("Processing binocular sequence - flick to same side - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(7);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 7)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSimMoving()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.213203855177062", "0"},
                {"Eye2", "45", "0.545792920227016", "0"},
                {"Eye1", "35", "0.213105738863169", "0"},
                {"Eye2", "45", "0.545817596587514", "0"},
                {"Eye1", "35", "0.212872318543954", "0"},
                {"Eye2", "45", "0.545876302053669", "0"},
                {"Eye1", "35", "0.212850572108482", "0"},
                {"Eye2", "45", "0.545881771306028", "0"},
                {"Eye1", "35", "0.214152591678478", "0"},
                {"Eye2", "45", "0.545554311956917", "0"},
                {"Eye1", "35", "0.217795376974401", "0"},
                {"Eye2", "45", "0.544638147461816", "0"},
                {"Eye1", "35", "0.221623843373206", "0"},
                {"Eye2", "45", "0.5436752839658", "0"},
                {"Eye1", "35", "0.225316362381023", "0"},
                {"Eye2", "45", "0.54274661138764", "0"},
                {"Eye1", "35", "0.229382945814396", "0"},
                {"Eye2", "45", "0.541723861196409", "0"},
                {"Eye1", "35", "0.23369839082294", "0"},
                {"Eye2", "45", "0.540638522046223", "0"},
                {"Eye1", "35", "0.239217739087587", "0"},
                {"Eye2", "45", "0.539250399907424", "0"},
                {"Eye1", "35", "0.245110765746391", "0"},
                {"Eye2", "45", "0.537768297242872", "0"},
                {"Eye1", "35", "0.250862376236056", "0"},
                {"Eye2", "45", "0.536321760899878", "0"},
                {"Eye1", "35", "0.257022858315746", "0"},
                {"Eye2", "45", "0.534772392903792", "0"},
                {"Eye1", "35", "0.263450795816321", "0"},
                {"Eye2", "45", "0.533155759576173", "0"},
                {"Eye1", "35", "0.26964132400656", "0"},
                {"Eye2", "45", "0.531598834950348", "0"},
                {"Eye1", "35", "0.276678483393201", "0"},
                {"Eye2", "45", "0.529828981650561", "0"},
                {"Eye1", "35", "0.284129854470884", "0"},
                {"Eye2", "45", "0.527954953656424", "0"},
                {"Eye1", "35", "0.292133121771892", "0"},
                {"Eye2", "45", "0.525942123157139", "0"},
                {"Eye1", "35", "0.301139169970166", "0"},
                {"Eye2", "45", "0.523677092162955", "0"},
                {"Eye1", "35", "0.310260963249549", "0"},
                {"Eye2", "45", "0.521382951153994", "0"},
                {"Eye1", "35", "0.320459977147623", "0"},
                {"Eye2", "45", "0.518817887978597", "0"},
                {"Eye1", "35", "0.330524587780306", "0"},
                {"Eye2", "45", "0.516286627371777", "0"},
                {"Eye1", "35", "0.340732995109087", "0"},
                {"Eye2", "45", "0.51371920173826", "0"},
                {"Eye1", "35", "0.350337971022787", "0"},
                {"Eye2", "45", "0.511303539767109", "0"},
                {"Eye1", "35", "0.359233292548136", "0"},
                {"Eye2", "45", "0.509066356652544", "0"},
                {"Eye1", "35", "0.367201173637577", "0"},
                {"Eye2", "45", "0.507062425824258", "0"},
                {"Eye1", "35", "0.366825693763559", "0"},
                {"Eye2", "45", "0.507156859424169", "0"},
                {"Eye1", "35", "0.37382071138915", "0"},
                {"Eye2", "45", "0.505397604823482", "0"},
                {"Eye1", "35", "0.379017787774265", "0"},
                {"Eye2", "45", "0.504090534415655", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -12.93694542, -12.93694542, -12.95421853, -12.95421853, -12.99531153, -12.99531153, -12.99913993, -12.99913993, -12.76992297, -12.76992297,
                -12.12862065, -12.12862065, -11.45462968, -11.45462968, -10.80457188, -10.80457188, -10.08866106, -10.08866106, -9.328938854, -9.328938854,
                -8.357272790, -8.357272790, -7.319821674, -7.319821674, -6.307266485, -6.307266485, -5.222730578, -5.222730578, -4.091109881, -4.091109881,
                -3.001284440, -3.001284440, -1.762411907, -1.762411907, -0.450618547, -0.450618547, 0.958334623, 0.958334623, 2.543824609, 2.543824609,
                4.149691198, 4.149691198, 5.945199511, 5.945199511, 7.717046499, 7.717046499, 9.514208499, 9.514208499, 11.20513806, 11.20513806,
                12.77113492, 12.77113492, 14.17385845, 14.17385845, 14.10775625, 14.10775625, 15.33920984, 15.33920984, 16.25414083, 16.25414083
            };
            #endregion

            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -12.93694542, -12.93694542, -12.93694542, -12.94126370, -12.94558197, -12.94990025, -12.95421853, -12.97476503, -12.97476503, -12.97476503,
                -12.97476503, -12.88453145, -12.76992297, -12.44927181, -12.12862065, -11.79162517, -11.45462968, -11.12960078, -10.80457188, -10.44661647,
                -10.08866106, -9.708799959, -9.328938854, -8.843105822, -8.357272790, -7.838547232, -7.319821674, -6.813544079, -6.307266485, -5.764998531,
                -5.222730578, -4.656920230, -4.091109881, -3.546197160, -3.001284440, -2.381848173, -1.762411907, -1.106515227, -0.450618547, 0.253858038,
                0.958334623, 1.751079616, 2.543824609, 3.346757904, 4.149691198, 5.047445355, 5.945199511, 6.831123005, 7.717046499, 8.615627499,
                9.514208499, 10.35967328, 11.20513806, 11.98813649, 12.77113492, 13.47249668, 14.10775625, 14.14080735, 14.17385845, 14.72348304
            };
            #endregion

            logger.Trace("Processing binocular sequence - moving no flick - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSimMovingFlick()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.213203855177062", "0"},
                {"Eye2", "45", "0.545792920227016", "0"},
                {"Eye1", "35", "0.213105738863169", "0"},
                {"Eye2", "45", "0.545817596587514", "0"},
                {"Eye1", "35", "0.212872318543954", "0"},
                {"Eye2", "45", "0.545876302053669", "0"},
                {"Eye1", "35", "0.212850572108482", "0"},
                {"Eye2", "45", "0.545881771306028", "0"},
                {"Eye1", "35", "0.214152591678478", "0"},
                {"Eye2", "45", "0.545554311956917", "0"},
                {"Eye1", "35", "0.217795376974401", "0"},
                {"Eye2", "45", "0.544638147461816", "0"},
                {"Eye1", "35", "0.221623843373206", "0"},
                {"Eye2", "45", "0.5436752839658", "0"},
                {"Eye1", "35", "0.225316362381023", "0"},
                {"Eye2", "45", "0.54274661138764", "0"},
                {"Eye1", "35", "0.229382945814396", "0"},
                {"Eye2", "45", "0.541723861196409", "0"},
                {"Eye1", "35", "0.23369839082294", "0"},
                {"Eye2", "45", "0.540638522046223", "0"},
                {"Eye1", "35", "0.239217739087587", "0"},
                {"Eye2", "45", "0.539250399907424", "0"},
                {"Eye1", "35", "0.245110765746391", "0"},
                {"Eye2", "45", "0.537768297242872", "0"},
                {"Eye1", "35", "0.250862376236056", "0"},
                {"Eye2", "45", "0.536321760899878", "0"},
                {"Eye1", "35", "0.257022858315746", "0"},
                {"Eye2", "45", "0.534772392903792", "0"},
                {"Eye1", "35", "0.263450795816321", "0"},
                {"Eye2", "45", "0.533155759576173", "0"},
                {"Eye1", "35", "0.326451553333333", "0"},
                {"Eye2", "45", "0.517311", "0"},
                {"Eye1", "35", "0.276678483393201", "0"},
                {"Eye2", "45", "0.529828981650561", "0"},
                {"Eye1", "35", "0.284129854470884", "0"},
                {"Eye2", "45", "0.527954953656424", "0"},
                {"Eye1", "35", "0.292133121771892", "0"},
                {"Eye2", "45", "0.525942123157139", "0"},
                {"Eye1", "35", "0.301139169970166", "0"},
                {"Eye2", "45", "0.523677092162955", "0"},
                {"Eye1", "35", "0.310260963249549", "0"},
                {"Eye2", "45", "0.521382951153994", "0"},
                {"Eye1", "35", "0.320459977147623", "0"},
                {"Eye2", "45", "0.518817887978597", "0"},
                {"Eye1", "35", "0.330524587780306", "0"},
                {"Eye2", "45", "0.516286627371777", "0"},
                {"Eye1", "35", "0.340732995109087", "0"},
                {"Eye2", "45", "0.51371920173826", "0"},
                {"Eye1", "35", "0.350337971022787", "0"},
                {"Eye2", "45", "0.511303539767109", "0"},
                {"Eye1", "35", "0.359233292548136", "0"},
                {"Eye2", "45", "0.509066356652544", "0"},
                {"Eye1", "35", "0.367201173637577", "0"},
                {"Eye2", "45", "0.507062425824258", "0"},
                {"Eye1", "35", "0.366825693763559", "0"},
                {"Eye2", "45", "0.507156859424169", "0"},
                {"Eye1", "35", "0.37382071138915", "0"},
                {"Eye2", "45", "0.505397604823482", "0"},
                {"Eye1", "35", "0.379017787774265", "0"},
                {"Eye2", "45", "0.504090534415655", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -12.93694542, -12.93694542, -12.95421853, -12.95421853, -12.99531153, -12.99531153, -12.99913993, -12.99913993, -12.76992297, -12.76992297,
                -12.12862065, -12.12862065, -11.45462968, -11.45462968, -10.80457188, -10.80457188, -10.08866106, -10.08866106, -9.328938854, -9.328938854,
                -8.357272790, -8.357272790, -7.319821674, -7.319821674, -6.307266485, -6.307266485, -5.222730578, -5.222730578, -4.091109881, -4.091109881,
                7.000000000, 7.000000000, -1.762411907, -1.762411907, -0.450618547, -0.450618547, 0.958334623, 0.958334623, 2.543824609, 2.543824609,
                4.149691198, 4.149691198, 5.945199511, 5.945199511, 7.717046499, 7.717046499, 9.514208499, 9.514208499, 11.20513806, 11.20513806,
                12.77113492, 12.77113492, 14.17385845, 14.17385845, 14.10775625, 14.10775625, 15.33920984, 15.33920984, 16.25414083, 16.25414083
            };
            #endregion

            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -12.93694542, -12.93694542, -12.93694542, -12.94126370, -12.94558197, -12.95421853, -12.97476503, -12.99531153, -12.99531153, -12.99531153,
                -12.88453145, -12.76992297, -12.44927181, -12.12862065, -11.79162517, -11.45462968, -11.12960078, -10.80457188, -10.44661647, -10.08866106,
                -9.708799959, -9.328938854, -8.843105822, -8.357272790, -7.838547232, -7.319821674, -6.813544079, -6.307266485, -5.764998531, -5.222730578,
                -4.656920230, -4.091109881, 1.454445059, 1.454445059, 1.454445059, -0.450618547, -0.450618547, -0.450618547, 0.253858038, 0.958334623,
                1.751079616, 2.543824609, 3.346757904, 4.149691198, 5.047445355, 5.945199511, 6.831123005, 7.717046499, 8.615627499, 9.514208499,
                10.35967328, 11.20513806, 11.98813649, 12.77113492, 13.47249668, 14.10775625, 14.14080735, 14.17385845, 14.72348304, 15.33920984
            };
            #endregion

            logger.Trace("Processing binocular sequence - moving with flick to same side - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSimMovingBigFlick()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.213203855177062", "0"},
                {"Eye2", "45", "0.545792920227016", "0"},
                {"Eye1", "35", "0.213105738863169", "0"},
                {"Eye2", "45", "0.545817596587514", "0"},
                {"Eye1", "35", "0.212872318543954", "0"},
                {"Eye2", "45", "0.545876302053669", "0"},
                {"Eye1", "35", "0.212850572108482", "0"},
                {"Eye2", "45", "0.545881771306028", "0"},
                {"Eye1", "35", "0.214152591678478", "0"},
                {"Eye2", "45", "0.545554311956917", "0"},
                {"Eye1", "35", "0.217795376974401", "0"},
                {"Eye2", "45", "0.544638147461816", "0"},
                {"Eye1", "35", "0.221623843373206", "0"},
                {"Eye2", "45", "0.5436752839658", "0"},
                {"Eye1", "35", "0.225316362381023", "0"},
                {"Eye2", "45", "0.54274661138764", "0"},
                {"Eye1", "35", "0.229382945814396", "0"},
                {"Eye2", "45", "0.541723861196409", "0"},
                {"Eye1", "35", "0.23369839082294", "0"},
                {"Eye2", "45", "0.540638522046223", "0"},
                {"Eye1", "35", "0.239217739087587", "0"},
                {"Eye2", "45", "0.539250399907424", "0"},
                {"Eye1", "35", "0.245110765746391", "0"},
                {"Eye2", "45", "0.537768297242872", "0"},
                {"Eye1", "35", "0.250862376236056", "0"},
                {"Eye2", "45", "0.536321760899878", "0"},
                {"Eye1", "35", "0.257022858315746", "0"},
                {"Eye2", "45", "0.534772392903792", "0"},
                {"Eye1", "35", "0.263450795816321", "0"},
                {"Eye2", "45", "0.533155759576173", "0"},
                {"Eye1", "35", "0.3718939", "0"},
                {"Eye2", "45", "0.5058822", "0"},
                {"Eye1", "35", "0.276678483393201", "0"},
                {"Eye2", "45", "0.529828981650561", "0"},
                {"Eye1", "35", "0.284129854470884", "0"},
                {"Eye2", "45", "0.527954953656424", "0"},
                {"Eye1", "35", "0.292133121771892", "0"},
                {"Eye2", "45", "0.525942123157139", "0"},
                {"Eye1", "35", "0.301139169970166", "0"},
                {"Eye2", "45", "0.523677092162955", "0"},
                {"Eye1", "35", "0.310260963249549", "0"},
                {"Eye2", "45", "0.521382951153994", "0"},
                {"Eye1", "35", "0.320459977147623", "0"},
                {"Eye2", "45", "0.518817887978597", "0"},
                {"Eye1", "35", "0.330524587780306", "0"},
                {"Eye2", "45", "0.516286627371777", "0"},
                {"Eye1", "35", "0.340732995109087", "0"},
                {"Eye2", "45", "0.51371920173826", "0"},
                {"Eye1", "35", "0.350337971022787", "0"},
                {"Eye2", "45", "0.511303539767109", "0"},
                {"Eye1", "35", "0.359233292548136", "0"},
                {"Eye2", "45", "0.509066356652544", "0"},
                {"Eye1", "35", "0.367201173637577", "0"},
                {"Eye2", "45", "0.507062425824258", "0"},
                {"Eye1", "35", "0.366825693763559", "0"},
                {"Eye2", "45", "0.507156859424169", "0"},
                {"Eye1", "35", "0.37382071138915", "0"},
                {"Eye2", "45", "0.505397604823482", "0"},
                {"Eye1", "35", "0.379017787774265", "0"},
                {"Eye2", "45", "0.504090534415655", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -12.93694542, -12.93694542, -12.95421853, -12.95421853, -12.99531153, -12.99531153, -12.99913993, -12.99913993, -12.76992297, -12.76992297,
                -12.12862065, -12.12862065, -11.45462968, -11.45462968, -10.80457188, -10.80457188, -10.08866106, -10.08866106, -9.328938854, -9.328938854,
                -8.357272790, -8.357272790, -7.319821674, -7.319821674, -6.307266485, -6.307266485, -5.222730578, -5.222730578, -4.091109881, -4.091109881,
                15.00000000, 15.00000000, -1.762411907, -1.762411907, -0.450618547, -0.450618547, 0.958334623, 0.958334623, 2.543824609, 2.543824609,
                4.149691198, 4.149691198, 5.945199511, 5.945199511, 7.717046499, 7.717046499, 9.514208499, 9.514208499, 11.20513806, 11.20513806,
                12.77113492, 12.77113492, 14.17385845, 14.17385845, 14.10775625, 14.10775625, 15.33920984, 15.33920984, 16.25414083, 16.25414083
            };
            #endregion

            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -12.93694542, -12.93694542, -12.93694542, -12.94126370, -12.94558197, -12.95421853, -12.97476503, -12.99531153, -12.99531153, -12.99531153,
                -12.88453145, -12.76992297, -12.44927181, -12.12862065, -11.79162517, -11.45462968, -11.12960078, -10.80457188, -10.44661647, -10.08866106,
                -9.708799959, -9.328938854, -8.843105822, -8.357272790, -7.838547232, -7.319821674, -6.813544079, -6.307266485, -5.764998531, -5.222730578,
                -4.656920230, -4.091109881, -4.091109881, -1.762411907, -1.106515227, -0.450618547, -0.450618547, -0.450618547, 0.253858038, 0.958334623,
                1.751079616, 2.543824609, 3.346757904, 4.149691198, 5.047445355, 5.945199511, 6.831123005, 7.717046499, 8.615627499, 9.514208499,
                10.35967328, 11.20513806, 11.98813649, 12.77113492, 13.47249668, 14.10775625, 14.14080735, 14.17385845, 14.72348304, 15.33920984
            };
            #endregion

            logger.Trace("Processing binocular sequence - moving with flick to same side - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSimMovingFlickLowerDeviationAllowed()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.213203855177062", "0"},
                {"Eye2", "45", "0.545792920227016", "0"},
                {"Eye1", "35", "0.213105738863169", "0"},
                {"Eye2", "45", "0.545817596587514", "0"},
                {"Eye1", "35", "0.212872318543954", "0"},
                {"Eye2", "45", "0.545876302053669", "0"},
                {"Eye1", "35", "0.212850572108482", "0"},
                {"Eye2", "45", "0.545881771306028", "0"},
                {"Eye1", "35", "0.214152591678478", "0"},
                {"Eye2", "45", "0.545554311956917", "0"},
                {"Eye1", "35", "0.217795376974401", "0"},
                {"Eye2", "45", "0.544638147461816", "0"},
                {"Eye1", "35", "0.221623843373206", "0"},
                {"Eye2", "45", "0.5436752839658", "0"},
                {"Eye1", "35", "0.225316362381023", "0"},
                {"Eye2", "45", "0.54274661138764", "0"},
                {"Eye1", "35", "0.229382945814396", "0"},
                {"Eye2", "45", "0.541723861196409", "0"},
                {"Eye1", "35", "0.23369839082294", "0"},
                {"Eye2", "45", "0.540638522046223", "0"},
                {"Eye1", "35", "0.239217739087587", "0"},
                {"Eye2", "45", "0.539250399907424", "0"},
                {"Eye1", "35", "0.245110765746391", "0"},
                {"Eye2", "45", "0.537768297242872", "0"},
                {"Eye1", "35", "0.250862376236056", "0"},
                {"Eye2", "45", "0.536321760899878", "0"},
                {"Eye1", "35", "0.257022858315746", "0"},
                {"Eye2", "45", "0.534772392903792", "0"},
                {"Eye1", "35", "0.263450795816321", "0"},
                {"Eye2", "45", "0.533155759576173", "0"},
                {"Eye1", "35", "0.326451553333333", "0"},
                {"Eye2", "45", "0.517311", "0"},
                {"Eye1", "35", "0.276678483393201", "0"},
                {"Eye2", "45", "0.529828981650561", "0"},
                {"Eye1", "35", "0.284129854470884", "0"},
                {"Eye2", "45", "0.527954953656424", "0"},
                {"Eye1", "35", "0.292133121771892", "0"},
                {"Eye2", "45", "0.525942123157139", "0"},
                {"Eye1", "35", "0.301139169970166", "0"},
                {"Eye2", "45", "0.523677092162955", "0"},
                {"Eye1", "35", "0.310260963249549", "0"},
                {"Eye2", "45", "0.521382951153994", "0"},
                {"Eye1", "35", "0.320459977147623", "0"},
                {"Eye2", "45", "0.518817887978597", "0"},
                {"Eye1", "35", "0.330524587780306", "0"},
                {"Eye2", "45", "0.516286627371777", "0"},
                {"Eye1", "35", "0.340732995109087", "0"},
                {"Eye2", "45", "0.51371920173826", "0"},
                {"Eye1", "35", "0.350337971022787", "0"},
                {"Eye2", "45", "0.511303539767109", "0"},
                {"Eye1", "35", "0.359233292548136", "0"},
                {"Eye2", "45", "0.509066356652544", "0"},
                {"Eye1", "35", "0.367201173637577", "0"},
                {"Eye2", "45", "0.507062425824258", "0"},
                {"Eye1", "35", "0.366825693763559", "0"},
                {"Eye2", "45", "0.507156859424169", "0"},
                {"Eye1", "35", "0.37382071138915", "0"},
                {"Eye2", "45", "0.505397604823482", "0"},
                {"Eye1", "35", "0.379017787774265", "0"},
                {"Eye2", "45", "0.504090534415655", "0"}
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -12.93694542, -12.93694542, -12.95421853, -12.95421853, -12.99531153, -12.99531153, -12.99913993, -12.99913993, -12.76992297, -12.76992297,
                -12.12862065, -12.12862065, -11.45462968, -11.45462968, -10.80457188, -10.80457188, -10.08866106, -10.08866106, -9.328938854, -9.328938854,
                -8.357272790, -8.357272790, -7.319821674, -7.319821674, -6.307266485, -6.307266485, -5.222730578, -5.222730578, -4.091109881, -4.091109881,
                7.000000000, 7.000000000, -1.762411907, -1.762411907, -0.450618547, -0.450618547, 0.958334623, 0.958334623, 2.543824609, 2.543824609,
                4.149691198, 4.149691198, 5.945199511, 5.945199511, 7.717046499, 7.717046499, 9.514208499, 9.514208499, 11.20513806, 11.20513806,
                12.77113492, 12.77113492, 14.17385845, 14.17385845, 14.10775625, 14.10775625, 15.33920984, 15.33920984, 16.25414083, 16.25414083
            };
            #endregion

            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -12.93694542, -12.93694542, -12.93694542, -12.94126370, -12.94558197, -12.95421853, -12.97476503, -12.99531153, -12.99531153, -12.99531153,
                -12.88453145, -12.76992297, -12.44927181, -12.12862065, -11.79162517, -11.45462968, -11.12960078, -10.80457188, -10.44661647, -10.08866106,
                -9.708799959, -9.328938854, -8.843105822, -8.357272790, -7.838547232, -7.319821674, -6.813544079, -6.307266485, -5.764998531, -5.222730578,
                -4.656920230, -4.091109881, -4.091109881, -1.762411907, -1.106515227, -0.450618547, -0.450618547, -0.450618547, 0.253858038, 0.958334623,
                1.751079616, 2.543824609, 3.346757904, 4.149691198, 5.047445355, 5.945199511, 6.831123005, 7.717046499, 8.615627499, 9.514208499,
                10.35967328, 11.20513806, 11.98813649, 12.77113492, 13.47249668, 14.10775625, 14.14080735, 14.17385845, 14.72348304, 15.33920984
            };
            #endregion

            logger.Trace("Processing binocular sequence - moving with flick to same side - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            SetAllowedDeviation(10.0);
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestBinocularSimMovingBigFlickBack()
        {
            bool allAsExpected = true;

            #region "Synthetic data from spreadsheet"
            string[,] rawTextData =
            {
                {"Eye1", "35", "0.213203855177062", "0"},
                {"Eye2", "45", "0.545792920227016", "0"},
                {"Eye1", "35", "0.213105738863169", "0"},
                {"Eye2", "45", "0.545817596587514", "0"},
                {"Eye1", "35", "0.212872318543954", "0"},
                {"Eye2", "45", "0.545876302053669", "0"},
                {"Eye1", "35", "0.212850572108482", "0"},
                {"Eye2", "45", "0.545881771306028", "0"},
                {"Eye1", "35", "0.214152591678478", "0"},
                {"Eye2", "45", "0.545554311956917", "0"},
                {"Eye1", "35", "0.217795376974401", "0"},
                {"Eye2", "45", "0.544638147461816", "0"},
                {"Eye1", "35", "0.221623843373206", "0"},
                {"Eye2", "45", "0.5436752839658", "0"},
                {"Eye1", "35", "0.225316362381023", "0"},
                {"Eye2", "45", "0.54274661138764", "0"},
                {"Eye1", "35", "0.229382945814396", "0"},
                {"Eye2", "45", "0.541723861196409", "0"},
                {"Eye1", "35", "0.23369839082294", "0"},
                {"Eye2", "45", "0.540638522046223", "0"},
                {"Eye1", "35", "0.239217739087587", "0"},
                {"Eye2", "45", "0.539250399907424", "0"},
                {"Eye1", "35", "0.245110765746391", "0"},
                {"Eye2", "45", "0.537768297242872", "0"},
                {"Eye1", "35", "0.250862376236056", "0"},
                {"Eye2", "45", "0.536321760899878", "0"},
                {"Eye1", "35", "0.257022858315746", "0"},
                {"Eye2", "45", "0.534772392903792", "0"},
                {"Eye1", "35", "0.263450795816321", "0"},
                {"Eye2", "45", "0.533155759576173", "0"},
                {"Eye1", "35", "0.173083633333333", "0"},
                {"Eye2", "45", "0.5558832", "0"},
                {"Eye1", "35", "0.276678483393201", "0"},
                {"Eye2", "45", "0.529828981650561", "0"},
                {"Eye1", "35", "0.284129854470884", "0"},
                {"Eye2", "45", "0.527954953656424", "0"},
                {"Eye1", "35", "0.292133121771892", "0"},
                {"Eye2", "45", "0.525942123157139", "0"},
                {"Eye1", "35", "0.301139169970166", "0"},
                {"Eye2", "45", "0.523677092162955", "0"},
                {"Eye1", "35", "0.310260963249549", "0"},
                {"Eye2", "45", "0.521382951153994", "0"},
                {"Eye1", "35", "0.320459977147623", "0"},
                {"Eye2", "45", "0.518817887978597", "0"},
                {"Eye1", "35", "0.330524587780306", "0"},
                {"Eye2", "45", "0.516286627371777", "0"},
                {"Eye1", "35", "0.340732995109087", "0"},
                {"Eye2", "45", "0.51371920173826", "0"},
                {"Eye1", "35", "0.350337971022787", "0"},
                {"Eye2", "45", "0.511303539767109", "0"},
                {"Eye1", "35", "0.359233292548136", "0"},
                {"Eye2", "45", "0.509066356652544", "0"},
                {"Eye1", "35", "0.367201173637577", "0"},
                {"Eye2", "45", "0.507062425824258", "0"},
                {"Eye1", "35", "0.366825693763559", "0"},
                {"Eye2", "45", "0.507156859424169", "0"},
                {"Eye1", "35", "0.37382071138915", "0"},
                {"Eye2", "45", "0.505397604823482", "0"},
                {"Eye1", "35", "0.379017787774265", "0"},
                {"Eye2", "45", "0.504090534415655", "0"},
            };
            #endregion

            #region "Synthetic azs from spreadsheet"
            double[] expectedPerEyeAzs = {
                -12.93694542, -12.93694542, -12.95421853, -12.95421853, -12.99531153, -12.99531153, -12.99913993, -12.99913993, -12.76992297, -12.76992297,
                -12.12862065, -12.12862065, -11.45462968, -11.45462968, -10.80457188, -10.80457188, -10.08866106, -10.08866106, -9.328938854, -9.328938854,
                -8.357272790, -8.357272790, -7.319821674, -7.319821674, -6.307266485, -6.307266485, -5.222730578, -5.222730578, -4.091109881, -4.091109881,
                -20.00000000, -20.00000000, -1.762411907, -1.762411907, -0.450618547, -0.450618547, 0.958334623, 0.958334623, 2.543824609, 2.543824609,
                4.149691198, 4.149691198, 5.945199511, 5.945199511, 7.717046499, 7.717046499, 9.514208499, 9.514208499, 11.20513806, 11.20513806,
                12.77113492, 12.77113492, 14.17385845, 14.17385845, 14.10775625, 14.10775625, 15.33920984, 15.33920984, 16.25414083, 16.25414083
            };
            #endregion

            #region "Median-filtered mean azs in spreadsheet"
            double[] expectedMedianFilteredMeanAzs = {
                -12.93694542, -12.93694542, -12.93694542, -12.94126370, -12.94558197, -12.95421853, -12.97476503, -12.99531153, -12.99531153, -12.99531153,
                -12.88453145, -12.76992297, -12.44927181, -12.12862065, -11.79162517, -11.45462968, -11.12960078, -10.80457188, -10.44661647, -10.08866106,
                -9.708799959, -9.328938854, -8.843105822, -8.357272790, -7.838547232, -7.319821674, -6.813544079, -6.307266485, -5.764998531, -5.222730578,
                -4.656920230, -4.656920230, -4.656920230, -4.091109881, -4.091109881, -1.762411907, -1.106515227, -0.450618547, 0.253858038, 0.958334623,
                1.751079616, 2.543824609, 3.346757904, 4.149691198, 5.047445355, 5.945199511, 6.831123005, 7.717046499, 8.615627499, 9.514208499,
                10.35967328, 11.20513806, 11.98813649, 12.77113492, 13.47249668, 14.10775625, 14.14080735, 14.17385845, 14.72348304, 15.33920984
            };
            #endregion

            logger.Trace("Processing binocular sequence - moving with flick back - filtering...");

            #region "Initialize calibration & history"
            SetFilterLength(5);
            azCalibration = new Double[,] { { -50.4708970428757, 369.1104578 }, { 176.0472464, -699.9860003 } };
            ResetDataCaches();
            #endregion

            // outer loop simulates event stream
            for (int row = 0; row < rawTextData.GetLength(0); row++)
            {
                #region "Set latest (current) eye data"
                string eye = rawTextData[row, 0];

                if (eye == "Eye1")
                {
                    TestSetEye1Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                else
                {
                    TestSetEye2Data(true, int.Parse(rawTextData[row, 1]), double.Parse(rawTextData[row, 2]), double.Parse(rawTextData[row, 3]));
                }
                #endregion

                object azimuthData;
                Double[] azEl_data;

                #region "Prime az history"
                if (row <= 5)
                {
                    // Just fill history with current data
                    AzElGaze(out azimuthData);
                    azEl_data = (Double[])azimuthData;
                    continue;
                }
                #endregion

                #region "Get filtered az, compare with expected"

                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                //logger.Trace("i: {0} flag: {1} az: {2} az1: {3} az2: {4}", row, (HaythamGazeQuality)(int)azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3]);

                // compare calculated individual azimuths with ones from spreadsheet
                // note that raw azs includes values flicking to 9.9 then back,
                // due to raw x-coord jumps at rows 20 & 21 but median-filtered azs do not.
                allAsExpected = logBinocularAzimuths(expectedMedianFilteredMeanAzs, row, azEl_data, false) && allAsExpected;

                if (!allAsExpected)
                {
                    logger.Trace("row: {0} not all as expected at or before this row", row);
                }
                #endregion
            }

            logger.Trace("All as expected in sequence: {0}", allAsExpected);

            logger.Trace("Exiting test ...");
        }

        private void TestWithDummyCalibration()
        {
            // a dummy eye calibration
            azCalibration = new Double[,] { { -55.83300171, 40.84699195 }, { 113.2662747, -100.7891793 } };
            //azCalibration = new Double[,] {
            //{135.815741946841,   266.567788275643},
            //{-492.565549293155,  -1184.158529148375},
            //{778.024245467390,   1750.867707455756},
            //{-513.985245279831,  -950.368327841202}};

            logger.Trace("Setting dummy azimuth history ...");
            ResetDataCaches();

            // a sequence of dummy azimuths drawn from the dummy calibration sequence

            double[] xs = new double[] {-0.583276008, -0.320215085, -0.129531312, 0.069262327, 0.335064294, 0.614628113, 0.86498056, 1.193214897, 1.47659579, 1.761177305};
            
            foreach(double x in xs)
            {
                AddToAzHistory(x);
            }

        }

        private void CheckSequence(double[] xs, double[] expectedAzs, HaythamGazeQuality[] qs, bool trace = false)
        {
            object azimuthData;
            Double[] azEl_data;
            bool allAsExpected = true;

            for (int i = 0; i < xs.Length; i++)
            {
                if (xs[i] > 0.0)
                {
                    TestSetEyeData(true, 47, xs[i], 0.0, false, 0, 0.0, 0.0);
                }
                else
                {
                    TestSetEyeData(false, 0, 0.0, 0.0, false, 0, 0.0, 0.0);
                }
                
                AzElGaze(out azimuthData);
                azEl_data = (Double[])azimuthData;

                if (qs == null)
                {
                    if (HaythamGazeQuality.Eye1Current != (HaythamGazeQuality)(int)azEl_data[0])
                    {
                        if (trace)
                        {
                            logger.Trace("i: {0} Quality flag expected Eye1Current but is {1}! Expected azimuth {2} is {3}", i, (HaythamGazeQuality)(int)azEl_data[0], expectedAzs[i], azEl_data[1]);
                        }
                        allAsExpected = false;
                    }
                }
                else
                {
                    if (qs[i] != (HaythamGazeQuality)(int)azEl_data[0])
                    {
                        if (trace)
                        {
                            logger.Trace("i: {0} Quality flag expected {1} but is {2}! Expected azimuth {3} is {4}", i, qs[i], (HaythamGazeQuality)(int)azEl_data[0], expectedAzs[i], azEl_data[1]);
                        }
                        allAsExpected = false;
                    }
                }

                if (Math.Abs(expectedAzs[i] - azEl_data[1]) > 0.5)
                {
                    if (trace)
                    {
                        logger.Trace("i: {0} Expected azimuth {1} more than 0.5 different from {2}", i, expectedAzs[i], azEl_data[1]);
                    }
                    allAsExpected = false;
                }
            }
            logger.Trace("All as expected in sequence: {0}", allAsExpected);
        }

        private void TestSetEyeData(bool p1F, int p1D, double p1X, double p1Y, bool p2F, int p2D, double p2X, double p2Y)
        {
            TestSetEye1Data(p1F, p1D, p1X, p1Y);
            TestSetEye2Data(p2F, p2D, p2X, p2Y);
        }

        private void TestSetEye2Data(bool p2F, int p2D, double p2X, double p2Y)
        {
            pupil2.Found = p2F;
            pupil2.Diameter = p2D;
            pupil2.CenterXFraction = p2X;
            pupil2.CenterYFraction = p2Y;

            pupil2.Updated = true;
        }

        private void TestSetEye1Data(bool p1F, int p1D, double p1X, double p1Y)
        {
            pupil1.Found = p1F;
            pupil1.Diameter = p1D;
            pupil1.CenterXFraction = p1X;
            pupil1.CenterYFraction = p1Y;

            pupil1.Updated = true;
        }


        [ComVisible(true)]
        public void Connect(String serverAddress, int serverPort)
        {
            logger.Info("Connecting ...");
            IPAddress serverip = null;

            // handle defaults
            if (String.IsNullOrEmpty(serverAddress))
            {
                // no address given - try self
                IPHostEntry host;
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        serverip = ip;
                    }
                }
            }
            else
            {
                serverip = IPAddress.Parse(serverAddress);
            }

            if (serverPort == 0)
            {
                // no port - use Haytham (hard-coded?) default
                serverPort = 50000;
            }

            logger.Debug("Using server IP {0} port {1}", serverip.ToString(), serverPort);

            // make sure not to re-use
            if (tcpClient != null)
            {
                logger.Debug("Freeing old connection ...");
                Free();
            }

            // attempt to connect
            if (tcpClient == null)
            {
                tcpClient = new TcpClient();
            }

            connectThread = new ConnectThread(tcpClient, MyConnected, serverip, serverPort);
            threadConnect = new Thread(connectThread.doConnect);

            threadConnect.Start();
        }

        private void MyConnected(bool isConnected)
        {
            if (Connected != null)
            {
                Connected(isConnected);
            }

            if (isConnected && (tcpClient != null)) {
                // tell client not to block on read too long
                tcpClient.ReceiveTimeout = 1000;

                stream = tcpClient.GetStream();
                writer = new BinaryWriter(stream);
                reader = new BinaryReader(stream);

                //send name and type
                logger.Debug("Querying connection name ...");
                writer.Write("Monitor");//type
                writer.Write("?");//name

                String clientName = reader.ReadString();//get approved name

                logger.Debug("Server sent connection name {0}", clientName);

                if (ServerInfo != null)
                {
                    ServerInfo(clientName);
                }

                // send Haytham commands to trigger eye image processing
                logger.Debug("Starting eye processing ...");
                // set to receive mono data
                writer.Write("Status_Eye");
                writer.Write("True");
                // and also both eyes if bino
                writer.Write("Status_Eye1");
                writer.Write("True");
                writer.Write("Status_Eye2");
                writer.Write("True");
                writer.Write("Status_Commands");
                writer.Write("True");

                ioThread = new Thread(new ThreadStart(RunIO));
                ioThread.Start();
                ioThreadStopped = new AutoResetEvent(false);
            }
        }

        private void RunIO()
        {
            // receive messages that is sent to client
            while (!closeStreams)
            {
                string msg = null;
                try
                {
                        msg = reader.ReadString();

                        logger.Trace("Raw message: {0}", msg);
                        ProcessMessage(msg);
                }
                catch (IOException ioe)
                {
                    logger.WarnException("Problem reading messages from server", ioe);
                }
            }
            logger.Debug("Thread reading from server about to exit ...");
            ioThreadStopped.Set();
        }

        private void ProcessMessage(string msg)
        {
            string[] msgArray = ConvertMsgToArray(msg);

            // local copies
            bool p1F;
            int p1D = 0;
            double p1X = 0.0;
            double p1Y = 0.0;

            bool p2F;
            int p2D = 0;
            double p2X = 0.0;
            double p2Y = 0.0;

            // stash values
            long ticks = 0L;

            if (msg.StartsWith("Eye|") || msg.StartsWith("Eye1"))
            {
                // from Haytham server: METCore.cs
                //METState.Current.eye.eyeData[0].time.Ticks.ToString(),
                //METState.Current.eye.eyeData[0].pupilDiameter.ToString(),
                //METState.Current.eye.eyeData[0].pupilFound.ToString(),
                //((float)METState.Current.eye.eyeData[0].pupilCenter.X/METState.Current.EyeCamera.VideoSize.Width).ToString(),
                //((float)METState.Current.eye.eyeData[0].pupilCenter.Y/METState.Current.EyeCamera.VideoSize.Height).ToString(),

                p1F = bool.Parse(msgArray[2]);

                if (p1F)
                {
                    long.TryParse(msgArray[0], out ticks);
                    p1D = int.Parse(msgArray[1]);
                    p1X = float.Parse(msgArray[3]);
                    p1Y = float.Parse(msgArray[4]);
                }

                // locked write
                lock (pupilDataLock)
                {
                    pupil1.Found = p1F;

                    if (p1F)
                    {
                        pupil1.Timestamp = ticks;
                        pupil1.Diameter = p1D;
                        pupil1.CenterXFraction = p1X;
                        pupil1.CenterYFraction = p1Y;

                        pupil1.Updated = true;
                        //logger.Trace("Eye1 updated");
                    }
                }

                // fire event
                if (Pupil1Info != null)
                {
                    Pupil1Info(p1F, p1F ? p1D : 0, p1F ? p1X : 0.0f, p1F ? p1Y : 0.0f);
                }
                if (AzimuthInfo != null)
                {
                    FireAzimuthEvent();
                }
            }
            else if (msg.StartsWith("Eye2"))
            {
                p2F = bool.Parse(msgArray[2]);

                if (p2F)
                {
                    long.TryParse(msgArray[0], out ticks);
                    p2D = int.Parse(msgArray[1]);
                    p2X = float.Parse(msgArray[3]);
                    p2Y = float.Parse(msgArray[4]);
                }

                // locked write
                lock (pupilDataLock)
                {
                    pupil2.Found = p2F;

                    if (p2F)
                    {
                        pupil2.Timestamp = ticks;
                        pupil2.Diameter = p2D;
                        pupil2.CenterXFraction = p2X;
                        pupil2.CenterYFraction = p2Y;

                        pupil2.Updated = true;
                        //logger.Trace("Eye2 updated");
                    }
                }

                // fire event
                if (Pupil2Info != null)
                {
                    Pupil2Info(p2F, p2F ? p2D : 0, p2F ? p2X : 0.0f, p2F ? p2Y : 0.0f);
                }
                if (AzimuthInfo != null)
                {
                    FireAzimuthEvent();
                }
            }
            else if (msg.StartsWith("Commands"))
            {
                System.Console.Out.WriteLine("Commands: {0}", msg);

                command = msgArray[2];
                arg = msgArray[1];

                if (CommandInfo != null)
                {
                    CommandInfo(command, arg);
                }
            }


        }

        private void FireAzimuthEvent()
        {
            object azimuthData;
            AzElGaze(out azimuthData);

            Double[] azEl_data = (Double[])azimuthData;

            AzimuthInfo((HaythamGazeQuality)Convert.ToInt32(azEl_data[0]), azEl_data[1], azEl_data[2], azEl_data[3], azEl_data[4], azEl_data[5], azEl_data[6], Convert.ToInt32(azEl_data[7]), Convert.ToInt32(azEl_data[8]));
        }

        private string[] ConvertMsgToArray(string msg)
        {
            //@PJ same functionality
            var arr = msg.Split('|');
            var msgArr = arr.Skip(1).ToArray();	// skip first keyword

            return msgArr;
        }


        [ComVisible(true)]
        public void RawGaze(out Object raw_gaze_data)
        {
            logger.Trace("Getting RawGaze data");
            Array pupilElements = Array.CreateInstance(typeof(double), 8);

            // local copies
            bool p1F;
            int p1D;
            double p1X;
            double p1Y;
            bool p1U;

            bool p2F;
            int p2D;
            double p2X;
            double p2Y;
            bool p2U;

            lock (pupilDataLock)
            {
                #region  "Locked copy"

                LockedDataSnapshot(out p1F, out p1D, out p1X, out p1Y, out p1U, out p2F, out p2D, out p2X, out p2Y, out p2U);

                #endregion
            }
            pupilElements.SetValue(Convert.ToDouble(p1F), 0);
            pupilElements.SetValue(p1D, 1);
            pupilElements.SetValue(p1X, 2);
            pupilElements.SetValue(p1Y, 3);

            pupilElements.SetValue(Convert.ToDouble(p1F), 4);
            pupilElements.SetValue(p2D, 5);
            pupilElements.SetValue(p2X, 6);
            pupilElements.SetValue(p2Y, 7);

            raw_gaze_data = pupilElements;
        }

        [ComVisible(true)]
        public void AzElGaze(out Object gaze_data)
        {
            if (azCalibration == null)
            {
                string warning = "Cannot calculate azimuth without calibration coefficients. Call SetCalibration() first.";

                logger.Warn(warning);
                throw new InvalidDataException(warning);
            }

            Array pupilAzimuthElevation = Array.CreateInstance(typeof(double), 9);

            #region  "Locked copy so we have a consistent snapshot"

            bool p1F;
            int p1D;
            double p1X;
            double p1Y;
            bool p1U;

            bool p2F;
            int p2D;
            double p2X;
            double p2Y;
            bool p2U;

            LockedDataSnapshot(out p1F, out p1D, out p1X, out p1Y, out p1U, out p2F, out p2D, out p2X, out p2Y, out p2U);

            //logger.Trace("p1F: {0} p1D: {1} p1X: {2} p1Y: {3} p2F: {4} p2D: {5} p2X: {6} p2Y {7}", p1F, p1D, p1X, p1Y, p2F, p2D, p2X, p2Y);

            #endregion

            // if updated, recalculate
            if (p1U || p2U)
            {
                //logger.Trace("Eye1 updated: {0} Eye2 updated: {1} so recalculating", p1U, p2U);

                #region "Determine which data is actually involved in following calculation"

                if (p1F && p2F)
                {
                    azElResults.q = HaythamGazeQuality.BothEyesCurrent;

                    // calculate using both columns
                    azElResults.az1 = azimuthFromPolynomial(p1X, 0);
                    azElResults.az2 = azimuthFromPolynomial(p2X, 1);

                    // if the results are divergent, throw away worse
                    if (Math.Abs(azElResults.az1 - azElResults.az2) >= allowedDeviation)
                    {
                        logger.Trace("current az1 {0} diverged from az2 {1}", azElResults.az1, azElResults.az2);

                        if (azHistory.Count > 0)
                        {
                            logger.Trace("peeking in deque");
                            double lastAz = azHistory.PeekFront();

                            if (Math.Abs(azElResults.az1 - lastAz) < Math.Abs(azElResults.az2 - lastAz))
                            {
                                logger.Trace("az1 closer");
                                azElResults.q = HaythamGazeQuality.Eye1Current;
                                azElResults.az = azElResults.az1;
                            }
                            else
                            {
                                logger.Trace("az2 closer");
                                azElResults.q = HaythamGazeQuality.Eye2Current;
                                azElResults.az = azElResults.az2;
                            }
                        }
                    }
                    else
                    {
                        azElResults.az = (azElResults.az1 + azElResults.az2) / 2.0;
                    }
                }
                else if (p1F)
                {
                    azElResults.q = HaythamGazeQuality.Eye1Current;

                    azElResults.az1 = azElResults.az = azimuthFromPolynomial(p1X, 0);
                    azElResults.az2 = 0.0;
                }
                else if (p2F)
                {
                    azElResults.q = HaythamGazeQuality.Eye2Current;

                    azElResults.az2 = azElResults.az = azimuthFromPolynomial(p2X, 1);
                    azElResults.az1 = 0.0;
                }
                else if ((p1D != 0) && (p2D != 0))
                {
                    azElResults.q = HaythamGazeQuality.BothEyesStale;

                    //logger.Trace("BothEyesStale: cached p1X: {0} cached p2X: {1}", p1X, p2X);

                    azElResults.az1 = azimuthFromPolynomial(p1X, 0);
                    azElResults.az2 = azimuthFromPolynomial(p2X, 1);

                    // if the results are divergent, throw away worse
                    if (Math.Abs(azElResults.az1 - azElResults.az2) >= allowedDeviation)
                    {
                        logger.Trace("stale az1 {0} diverged from az2 {1}", azElResults.az1, azElResults.az2);

                        if (azHistory.Count > 0)
                        {
                            logger.Trace("peeking in deque");
                            double lastAz = azHistory.PeekFront();

                            if (Math.Abs(azElResults.az1 - lastAz) < Math.Abs(azElResults.az2 - lastAz))
                            {
                                logger.Trace("az1 closer");
                                azElResults.q = HaythamGazeQuality.Eye1Stale;
                                azElResults.az = azElResults.az1;
                            }
                            else
                            {
                                logger.Trace("az2 closer");
                                azElResults.q = HaythamGazeQuality.Eye2Stale;
                                azElResults.az = azElResults.az2;
                            }
                        }
                    }
                    else
                    {
                        azElResults.az = (azElResults.az1 + azElResults.az2) / 2.0;
                    }
                }
                else if ((p1D != 0) && (p2D == 0))
                {
                    azElResults.q = HaythamGazeQuality.Eye1Stale;

                    //logger.Trace("Eye1Stale: cached p1X: {0} cached p2X: {1}", p1X, p2X);

                    azElResults.az1 = azElResults.az = azimuthFromPolynomial(p1X, 0);
                    azElResults.az2 = 0.0;
                }
                else if ((p1D == 0) && (p2D != 0))
                {
                    azElResults.q = HaythamGazeQuality.Eye2Stale;

                    //logger.Trace("Eye2Stale: cached p1X: {0} cached p2X: {1}", p1X, p2X);

                    azElResults.az2 = azElResults.az = azimuthFromPolynomial(p2X, 1);
                    azElResults.az1 = 0.0;
                }
                else
                {
                    // at this point, we know p1Found == false && p2Found == false && p1Diam == 0 && p2Diam == 0
                    azElResults.q = HaythamGazeQuality.NoData;

                    azElResults.az1 = azElResults.az2 = azElResults.az = 0.0;
                }

                #endregion

                #region "Median filtering to eliminate noisy tokens"

                // 0 or 1 means "don't filter"
                if (filterLength > 1)
                {
                    // possible alternate approach:
                    // http://stats.stackexchange.com/questions/1142/simple-algorithm-for-online-outlier-detection-of-a-generic-time-series

                    // if data is stale, use last good
                    if (!azElResults.q.HasFlag(HaythamGazeQuality.DataCurrent))
                    {
                        // no current data, so is stale
                        azElResults.q = HaythamGazeQuality.BothEyesStale;
                        azElResults.az = azHistory.PeekFront();
                    }

                    // update history
                    AddToAzHistory(azElResults.az);

                    // must have enough data to calculate statistics (not clear if meaningful!)
                    if (azHistory.Count >= 3)
                    {
                        double mean;
                        double sd;
                        double median;

                        CalculateAzHistoryStats(out mean, out sd, out median);
                        azElResults.az = median;
                    }
                }

                #endregion
            }
            else
            {
                logger.Trace("Neither eye updated; not recalculating");
            }

            // package for transport
            // flag
            // combined az
            // eye1 az
            // eye2 az
            // combined el
            // eye1 el
            // eye2 el
            // p1 diam
            // p2 diam

            pupilAzimuthElevation.SetValue(Convert.ToDouble(azElResults.q), 0);
            pupilAzimuthElevation.SetValue(azElResults.az /* function of pupilCenterXFraction */, 1);

            pupilAzimuthElevation.SetValue(azElResults.az1, 2);
            pupilAzimuthElevation.SetValue(azElResults.az2, 3);

            pupilAzimuthElevation.SetValue(0.0 /* function of pupilCenterYFraction */, 4);
            pupilAzimuthElevation.SetValue(0.0, 5);
            pupilAzimuthElevation.SetValue(0.0, 6);

            pupilAzimuthElevation.SetValue(p1D, 7);
            pupilAzimuthElevation.SetValue(p2D, 8);


            gaze_data = pupilAzimuthElevation;
        }

        private void LockedDataSnapshot(out bool p1F, out int p1D, out double p1X, out double p1Y, out bool p1U, out bool p2F, out int p2D, out double p2X, out double p2Y, out bool p2U)
        {
            lock (pupilDataLock)
            {
                p1F = pupil1.Found;
                p1D = pupil1.Diameter;
                p1X = pupil1.CenterXFraction;
                p1Y = pupil1.CenterYFraction;
                p1U = pupil1.Updated;

                pupil1.Updated = false;

                p2F = pupil2.Found;
                p2D = pupil2.Diameter;
                p2X = pupil2.CenterXFraction;
                p2Y = pupil2.CenterYFraction;
                p2U = pupil2.Updated;

                pupil2.Updated = false;
            }
        }

        private void AddToAzHistory(double x)
        {
            azHistory.PushFront(x);
            while (azHistory.Count > filterLength)
            {
                azHistory.PopBack();
            }
        }

        private void CalculateAzHistoryStats(out double mean, out double sd, out double median)
        {
            double[] ah = azHistory.ToArray<double>();

            // can't use the "out" parameter in the expression below
            double lmean = ah.Average();
            mean = lmean;

            double ssd = ah.Select(v => Math.Pow(v - lmean, 2.0)).Sum();
            sd = Math.Sqrt(ssd / (ah.Length - 1));  // sample is not whole population

            // this should always be short, so sorting is not a big cost
            Array.Sort<double>(ah);
            int medianIndex = ah.Length / 2;
            median = (ah.Length % 2 != 0) ? ah[medianIndex] : (ah[medianIndex] + ah[medianIndex - 1]) / 2.0;
        }

        private double azimuthFromPolynomial(double x, int azCalibrationCol)
        {
            // Horner's method
            double result = 0.0;

            for (int i = (azCalibration.GetLength(0) - 1); i >= 0; i--)
            {
                //logger.Trace("i: {0} coeff: {1}", i, azCalibration[i, azCalibrationCol]);
                result *= x;
                result += azCalibration[i, azCalibrationCol];
            }
            //logger.Trace("result: {0}", result);

            // (quasi-empirical) hard caps on value of azimuth
            result = Math.Max(-40.0, Math.Min(40.0, result));

            return result;
        }

        [ComVisible(true)]
        public void SetCalibration(Object calib_params)
        {
            // expect a SAFEARRAY with m rows, 2 columns
            logger.Debug("Receiving Calibration parameters");
            Type calib_params_type = calib_params.GetType();
            Double[,] param;

            #region "type-check array passed through COM interface"

            string warning;

            if (!calib_params_type.IsArray)
            {
                warning = "calib_params is not array";

                logger.Warn(warning);
                throw new ArgumentException(warning);
            }

            if (calib_params_type.GetElementType() != typeof(Double))
            {
                warning = "elements of calib_params not doubles";

                logger.Warn(warning);
                throw new ArgumentException(warning);
            }

            logger.Trace("Calibration params rank {0}", calib_params_type.GetArrayRank());
            if (calib_params_type.GetArrayRank() != 2)
            {
                warning = "calib_params not 2D array (i.e., not matrix)";

                logger.Warn(warning);
                throw new ArgumentException(warning);
            }

            param = (Double[,])calib_params;

            logger.Trace("Calibration params dimension 0 length {0}", param.GetLength(0));
            logger.Trace("Calibration params dimension 1 length {0}", param.GetLength(1));
            if (param.GetLength(1) != 2)
            {
                warning = "calib_params does not have expected number of columns (2 - one for each eye camera)";

                logger.Warn(warning);
                throw new ArgumentException(warning);
            }

            #endregion

            #region "Reverse order of coefficients because we expect input frmo MATLAB which by convention puts constant term LAST"

            int last = param.GetLength(0) - 1;
            for (int i = 0; i < (param.GetLength(0) / 2); i++)
            {
                // swap the elements in the i-th row with the elements in the last-minus-ith row
                double temp = param[i, 0];

                param[i, 0] = param[last - i, 0];
                param[last - i, 0] = temp;

                temp = param[i, 1];
                param[i, 1] = param[last - i, 1];
                param[last - i, 1] = temp;
            }

            #endregion

            azCalibration = param;
        }

        [ComVisible(true)]
        public void SetFilterLength(int length)
        {
            // only odd values accepted
            if ((length % 2) == 1)
            {
                filterLength = length;

                while (azHistory.Count > filterLength)
                {
                    azHistory.PopBack();
                }
            }
        }

        [ComVisible(true)]
        public void SetAllowedDeviation(Double deviation)
        {
            allowedDeviation = deviation;
        }

        [ComVisible(true)]
        public void Free()
        {
            logger.Info("Freeing resources ...");
            if ((threadConnect != null) && threadConnect.IsAlive)
            {
                logger.Debug("Requesting stoppage of connection attempts ...");
                connectThread.RequestStop();
                threadConnect.Join();
            }

            if (ioThread != null)
            {
                logger.Debug("Requesting stoppage of attempts to read from server ...");

                // request stop directly
                closeStreams = true;

                logger.Debug("Waiting for thread to terminate ...");
                bool stopped = ioThreadStopped.WaitOne(500);

                if (stopped)
                {
                    logger.Debug("Thread signaled termination");
                    // reinitialize for next time
                    closeStreams = false;
                    ioThread = null;
                }
                else
                {
                    // bad, but ... how else?
                    ioThread.Abort();
                }
            }

            logger.Debug("Closing streams ...");
            if (writer != null)
            {
                // send closing message
                writer.Write("CLIENT>>> TERMINATE");

                writer.Close();
                writer.Dispose();
                writer = null;
            }
            if (reader != null)
            {
                reader.Close();
                reader.Dispose();
                reader = null;
            }
            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
                stream = null;
            }

            logger.Debug("Closing connection to server ...");
            if (tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;

                if (Connected != null)
                {
                    Connected(false);
                }
            }
        }

        [ComVisible(false)]
        public class ConnectThread
        {
            TcpClient tcpClient;
            ConnectedEventHandler handler;
            IPAddress serverip;
            int serverPort;

            private volatile bool stopConnecting = false;

            
            public ConnectThread(TcpClient tcpClient, ConnectedEventHandler handler, IPAddress serverip, int serverPort)
            {
                this.tcpClient = tcpClient;
                this.handler = handler;
                this.serverip = serverip;
                this.serverPort = serverPort;
            }

            public void RequestStop()
            {
                logger.Debug("Stopping attempt to connect to server ...");
                stopConnecting = true;
            }

            public void doConnect()
            {
                logger.Debug("Attempting to connect to server ...");

                while (!tcpClient.Connected && !stopConnecting)
                {
                    try
                    {
                        tcpClient.Connect(serverip, 50000);
                    }
                    catch (Exception connectException)
                    {
                        logger.WarnException("Unable to connect to server - retrying", connectException);

                        if (handler != null)
                        {
                            handler(false);
                        }
                    }

                    // wait a little before trying again
                    Thread.Sleep(100);
                }

                if (stopConnecting)
                {
                    logger.Debug("Stopped attempting to connect");
                }
                if (tcpClient.Connected)
                {
                    logger.Debug("Connected to server");
                }
                else
                {
                    logger.Warn("Unable to connect to server");
                }

                if (handler != null)
                {
                    handler(tcpClient.Connected);
                }
            }
        }
    }
}
