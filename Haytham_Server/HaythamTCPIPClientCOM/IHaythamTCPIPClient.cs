﻿//This file is part of HaytamTCPIPClientCOM
//Copyright (C) 2015 Sensimetrics Corporation
// ------------------------------------------------------------------------
//Sensimetrics Corporation remains the owner of all rights, title and interest
//in the Software and Documentation. You may copy the Software for backup and
//archival purposes, provided that the original and each copy is kept in your
//possession. The Software and Documentation are protected by United States
//copyright laws and international treaties. You must treat the Software and
//Documentation like any other copyrighted material. ANY USE OR DISCLOSURE OF
//THE SOFTWARE, OR OF ITS ALGORITHMS, PROTOCOLS OR INTERFACES, OTHER THAN IN
//STRICT ACCORDANCE WITH THIS LICENSE AGREEMENT, MAY BE ACTIONABLE.
// ------------------------------------------------------------------------
// author: Michel Jackson
// email: michel@sens.com


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;


namespace HaythamTCPIPClientCOM
{
    [ComVisible(true), InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("4E77CF22-5B92-4E12-AD90-A3447D778BE0")]
    public interface IHaythamTCPIPClient
    {
        [DispId(1)]
        void Connect(String serverAddress, int serverPort);


        [DispId(2)]
        void RawGaze(out Object raw_gaze_data);

        [DispId(3)]
        void AzElGaze(out Object gaze_data);

        [DispId(4)]
        void SetCalibration(Object calib_params);

        [DispId(5)]
        void SetFilterLength(int filterLength);

        [DispId(6)]
        void SetAllowedDeviation(Double deviation);


        [DispId(7)]
        void Free();
    }


    // Events interface 
    [ComVisible(true), InterfaceType(ComInterfaceType.InterfaceIsIDispatch), Guid("04BB16A8-7D2E-4272-95AF-3A13AC6B95DC")]
    public interface IHaythamTCPIPClient_Events
    {
        [DispId(1)]
        void Connected(bool isConnected);

        [DispId(2)]
        void ServerInfo(string connectionName);

        [DispId(3)]
        void Pupil1Info(bool found, int diameter, double centerX, double centerY);

        [DispId(4)]
        void Pupil2Info(bool found, int diameter, double centerX, double centerY);

        [DispId(5)]
        void AzimuthInfo(HaythamGazeQuality quality, double azMean, double az1, double az2, double elMean, double el1, double el2, int pupilDiameter1, int pupilDiameter2);

        [DispId(6)]
        void CommandInfo(string command);
    }

    // Enum for specifying quality of transformed gaze data
    [ComVisible(true), Guid("961EC410-CCB4-4715-9A56-78D39AD869E7")]
    public enum HaythamGazeQuality
    {
        NoData = 0,
        Eye1Stale = 1,
        Eye2Stale = 2,
        BothEyesStale = 3,
        DataCurrent = 4,
        Eye1Current = 5,
        Eye2Current = 6,
        BothEyesCurrent = 7
    }
}
