﻿using System;
using System.Windows.Forms;


using HaythamTCPIPClientCOM;



namespace HaythamTCPIPClientTest
{
    public partial class Form1 : Form
    {
        HaythamTCPIPClient haythamClient = null;
        HaythamTCPIPClient.ConnectedEventHandler connectedEventHandler;
        HaythamTCPIPClient.ServerInfoEventHandler serverInfoEventHandler;
        HaythamTCPIPClient.PupilInfoEventHandler pupil1InfoEventHandler;
        HaythamTCPIPClient.PupilInfoEventHandler pupil2InfoEventHandler;
        HaythamTCPIPClient.AzimuthInfoEventHandler azimuthEventHandler;
        HaythamTCPIPClient.CommandInfoEventHandler commandInfoEventHandler;


        // dummy calibration - constant term LAST:
        double[,] dummyCalibration = new double[,] {/* coefficient of pupil x */ { 102, 102 }, /* intercepts */ { -51, -51 }};


        public Form1()
        {
            InitializeComponent();

            connectedEventHandler = new HaythamTCPIPClient.ConnectedEventHandler(onConnectedToHaytham);
            serverInfoEventHandler = new HaythamTCPIPClient.ServerInfoEventHandler(onHaythamServerInfo);
            pupil1InfoEventHandler = new HaythamTCPIPClient.PupilInfoEventHandler(onHaythamPupil1Info);
            pupil2InfoEventHandler = new HaythamTCPIPClient.PupilInfoEventHandler(onHaythamPupil2Info);
            azimuthEventHandler = new HaythamTCPIPClient.AzimuthInfoEventHandler(onHaythamAzimuthInfo);
            commandInfoEventHandler = new HaythamTCPIPClient.CommandInfoEventHandler(onHaythamCommandInfo);

            haythamClient = new HaythamTCPIPClient();

            haythamClient.Connected += connectedEventHandler;
            haythamClient.ServerInfo += serverInfoEventHandler;
            haythamClient.Pupil1Info += pupil1InfoEventHandler;
            haythamClient.Pupil2Info += pupil2InfoEventHandler;
            haythamClient.AzimuthInfo += azimuthEventHandler;
            haythamClient.CommandInfo += commandInfoEventHandler;
        }

        private void bnConnect_Click(object sender, EventArgs e)
        {
            String t = txServerPort.Text;
            haythamClient.Connect(txServerIP.Text, String.IsNullOrEmpty(t) || String.IsNullOrWhiteSpace(t) ? 0 : Int32.Parse(t));
        }

        private void onConnectedToHaytham(bool isConnected)
        {
            MessageBox.Show("isConnected: " + isConnected);
        }

        private void onHaythamServerInfo(string clientName)
        {
            MessageBox.Show("clientName: " + clientName);
        }

        private void onHaythamPupil1Info(bool found, int diameter, double centerX, double centerY)
        {
            if (ckPupilEvents.Checked)
            {
                String message = "pupil 1 found: " + found + " diameter: " + diameter + " centerX: " + centerX + " centerY: " + centerY + "\n";

                appendInfoMessage(message);
            }
        }

        private void appendInfoMessage(String message)
        {
            if (txPupilInfo.InvokeRequired)
            {
                txPupilInfo.Invoke((MethodInvoker) delegate { appendText(message); }, new String[] { message });
            }
            else
            {
                appendText(message);
            }
        }

        private void onHaythamPupil2Info(bool found, int diameter, double centerX, double centerY)
        {
            if (ckPupilEvents.Checked)
            {
                String message = "pupil 2 found: " + found + " diameter: " + diameter + " centerX: " + centerX + " centerY: " + centerY + "\n";

                appendInfoMessage(message);
            }
        }

        private void onHaythamAzimuthInfo(HaythamGazeQuality quality, double azMean, double az1, double az2, double elMean, double el1, double el2, int pupilDiameter1, int pupilDiameter2)
        {
            if (ckAzimuthEvents.Checked)
            {
                String message = "azimuth: " + quality + " mean: " + azMean + " az1: " + az1 + " az2: " + az2 + " (elevations ignored) pupilDiameter1: " + pupilDiameter1 + " pupilDiameter2: " + pupilDiameter2;

                appendInfoMessage(message);
            }
        }

        private void appendText(string message)
        {
            txPupilInfo.AppendText(message);
        }

        private void onHaythamCommandInfo(string command, string eye)
        {
            string message = command + " " + eye + "\n";

            appendInfoMessage(message);
        }

        private void bnStopConnecting_Click(object sender, EventArgs e)
        {
            haythamClient.Free();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            haythamClient.CommandInfo -= commandInfoEventHandler;
            haythamClient.AzimuthInfo -= azimuthEventHandler;
            haythamClient.Pupil2Info -= pupil2InfoEventHandler;
            haythamClient.Pupil1Info -= pupil1InfoEventHandler;
            haythamClient.ServerInfo -= serverInfoEventHandler;

            haythamClient.Free();

            haythamClient.Connected -= connectedEventHandler;

            haythamClient = null;
        }

        private void bnGetRaw_Click(object sender, EventArgs e)
        {
            object raw_gaze_data;
            Double[] gaze_data;

            haythamClient.RawGaze(out raw_gaze_data);

            Type gaze_data_type = raw_gaze_data.GetType();

            if (gaze_data_type.IsArray && (gaze_data_type.GetElementType() == typeof(Double)))
            {
                gaze_data = (Double[])raw_gaze_data;

                System.Console.WriteLine("gaze_data.Length: {0}", gaze_data.Length.ToString());

                bool pupil1Found = Convert.ToBoolean(gaze_data[0]);
                int pupil1Diameter = Convert.ToInt32(gaze_data[1]);
                double pupil1CenterXFraction = gaze_data[2];
                double pupil1CenterYFraction = gaze_data[3];

                if (gaze_data.Length == 4)
                {
                    MessageBox.Show(
                        "pupilFound: " + pupil1Found + "\n" +
                        "pupilDiameter: " + pupil1Diameter + "\n" +
                        "pupilCenterXFraction: " + pupil1CenterXFraction + "\n" +
                        "pupilCenterYFraction: " + pupil1CenterYFraction);
                }
                else if (gaze_data.Length == 8)
                {
                    bool pupil2Found = Convert.ToBoolean(gaze_data[4]);
                    int pupil2Diameter = Convert.ToInt32(gaze_data[5]);
                    double pupil2CenterXFraction = gaze_data[6];
                    double pupil2CenterYFraction = gaze_data[7];

                    MessageBox.Show(
                        "pupil1Found: " + pupil1Found + "\n" +
                        "pupil1Diameter: " + pupil1Diameter + "\n" +
                        "pupil1CenterXFraction: " + pupil1CenterXFraction + "\n" +
                        "pupil1CenterYFraction: " + pupil1CenterYFraction + "\n" +
                        "pupil2Found: " + pupil2Found + "\n" +
                        "pupil2Diameter: " + pupil2Diameter + "\n" +
                        "pupil2CenterXFraction: " + pupil2CenterXFraction + "\n" +
                        "pupil2CenterYFraction: " + pupil2CenterYFraction);
                }
                else
                {
                    MessageBox.Show("gaze_data format unknown; length = " + gaze_data.Length.ToString());
                }
            }
        }

        private void bnSetDummyCalibration_Click(object sender, EventArgs e)
        {
            haythamClient.SetCalibration(dummyCalibration);
        }

        private void bnGetTransformed_Click(object sender, EventArgs e)
        {
            object raw_azEl;
            Double[] azEl_data;

            try
            {
                haythamClient.AzElGaze(out raw_azEl);
            }
            catch (Exception except)
            {
                MessageBox.Show("Exception: " + except.Message);
                return;
            }

            Type azEl_data_type = raw_azEl.GetType();

            if (azEl_data_type.IsArray && (azEl_data_type.GetElementType() == typeof(Double)))
            {
                azEl_data = (Double[])raw_azEl;

                if (azEl_data.Length == 9)
                {
                    //MessageBox.Show(
                    //    "Flag: " + azEl_data[0] + "\n" +
                    //    "Azimuth: " + azEl_data[1]);

                    string msg = string.Format(
                        "flag: {0} az: {1} az1: {2} az2: {3} el: {4} el1: {5} el2: {6} p1d: {7} p2d: {8}",
                        azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3], azEl_data[4], azEl_data[5], azEl_data[6], azEl_data[7], azEl_data[8]);

                    MessageBox.Show(msg);
                }
                else
                {
                    MessageBox.Show("azEl_data length unexpected: " + azEl_data.Length);
                }
            }
            else
            {
                MessageBox.Show("azEl format unknown");
            }
        }

        private void bnLoopGetTransformed_Click(object sender, EventArgs e)
        {
            System.Threading.Thread callLoop = new System.Threading.Thread(new System.Threading.ThreadStart(call100Times));
            callLoop.Start();
        }

        private void call100Times()
        {
            // Emulate JD's 170+ Hz loop that calls over & over
            System.Timers.Timer timer = new System.Timers.Timer(5);
            CountedCaller countedCaller = new CountedCaller(100, this);

            timer.Elapsed += countedCaller.onTimedEvent;
            timer.Start();
        }

        private class CountedCaller
        {
            int timesCalled;
            Form1 mainForm;


            public CountedCaller(int timesCalled, Form1 form1)
            {
                this.timesCalled = timesCalled;
                this.mainForm = form1;
            }

            public void onTimedEvent(object source, System.Timers.ElapsedEventArgs e)
            {
                if (timesCalled > 0)
                {
                    call1Time();
                    timesCalled--;
                }
                else
                {
                    if (source.GetType() == typeof(System.Timers.Timer))
                    {
                        System.Timers.Timer t = (System.Timers.Timer)source;

                        t.Stop();
                        t.Elapsed -= this.onTimedEvent;
                        t.Dispose();
                    }
                }
            }

            private void call1Time()
            {
                string msg;
                object raw_azEl;
                Double[] azEl_data;

                try
                {
                    mainForm.haythamClient.AzElGaze(out raw_azEl);
                }
                catch (Exception except)
                {
                    msg = string.Format("Exception: {0}", except.Message);
                    mainForm.appendInfoMessage(msg);
                    return;
                }

                Type azEl_data_type = raw_azEl.GetType();

                if (azEl_data_type.IsArray && (azEl_data_type.GetElementType() == typeof(Double)))
                {
                    azEl_data = (Double[])raw_azEl;

                    if (azEl_data.Length == 9)
                    {
                        msg = string.Format(
                            "flag: {0} az: {1} az1: {2} az2: {3} el: {4} el1: {5} el2: {6} p1d: {7} p2d: {8}",
                            azEl_data[0], azEl_data[1], azEl_data[2], azEl_data[3], azEl_data[4], azEl_data[5], azEl_data[6], azEl_data[7], azEl_data[8]);
                    }
                    else
                    {
                        msg = string.Format("azEl_data length unexpected: {0}", azEl_data.Length);
                    }
                }
                else
                {
                    msg = "azEl format unknown";
                }
                mainForm.appendInfoMessage(msg);
            }
        };

    }
}
