﻿namespace HaythamTCPIPClientTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txServerIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txServerPort = new System.Windows.Forms.TextBox();
            this.bnConnect = new System.Windows.Forms.Button();
            this.bnStopConnecting = new System.Windows.Forms.Button();
            this.bnGetRaw = new System.Windows.Forms.Button();
            this.txPupilInfo = new System.Windows.Forms.TextBox();
            this.bnSetDummyCalibration = new System.Windows.Forms.Button();
            this.bnGetTransformed = new System.Windows.Forms.Button();
            this.bnLoopGetTransformed = new System.Windows.Forms.Button();
            this.ckPupilEvents = new System.Windows.Forms.CheckBox();
            this.ckAzimuthEvents = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txServerIP
            // 
            this.txServerIP.Location = new System.Drawing.Point(72, 6);
            this.txServerIP.Name = "txServerIP";
            this.txServerIP.Size = new System.Drawing.Size(100, 20);
            this.txServerIP.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Server IP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Port #:";
            // 
            // txServerPort
            // 
            this.txServerPort.Location = new System.Drawing.Point(72, 32);
            this.txServerPort.Name = "txServerPort";
            this.txServerPort.Size = new System.Drawing.Size(100, 20);
            this.txServerPort.TabIndex = 3;
            // 
            // bnConnect
            // 
            this.bnConnect.Location = new System.Drawing.Point(72, 59);
            this.bnConnect.Name = "bnConnect";
            this.bnConnect.Size = new System.Drawing.Size(75, 23);
            this.bnConnect.TabIndex = 4;
            this.bnConnect.Text = "Connect";
            this.bnConnect.UseVisualStyleBackColor = true;
            this.bnConnect.Click += new System.EventHandler(this.bnConnect_Click);
            // 
            // bnStopConnecting
            // 
            this.bnStopConnecting.Location = new System.Drawing.Point(72, 88);
            this.bnStopConnecting.Name = "bnStopConnecting";
            this.bnStopConnecting.Size = new System.Drawing.Size(75, 23);
            this.bnStopConnecting.TabIndex = 5;
            this.bnStopConnecting.Text = "Stop";
            this.bnStopConnecting.UseVisualStyleBackColor = true;
            this.bnStopConnecting.Click += new System.EventHandler(this.bnStopConnecting_Click);
            // 
            // bnGetRaw
            // 
            this.bnGetRaw.Location = new System.Drawing.Point(72, 117);
            this.bnGetRaw.Name = "bnGetRaw";
            this.bnGetRaw.Size = new System.Drawing.Size(75, 23);
            this.bnGetRaw.TabIndex = 6;
            this.bnGetRaw.Text = "Get Raw";
            this.bnGetRaw.UseVisualStyleBackColor = true;
            this.bnGetRaw.Click += new System.EventHandler(this.bnGetRaw_Click);
            // 
            // txPupilInfo
            // 
            this.txPupilInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txPupilInfo.Location = new System.Drawing.Point(12, 169);
            this.txPupilInfo.Multiline = true;
            this.txPupilInfo.Name = "txPupilInfo";
            this.txPupilInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txPupilInfo.Size = new System.Drawing.Size(419, 120);
            this.txPupilInfo.TabIndex = 7;
            // 
            // bnSetDummyCalibration
            // 
            this.bnSetDummyCalibration.Location = new System.Drawing.Point(286, 59);
            this.bnSetDummyCalibration.Name = "bnSetDummyCalibration";
            this.bnSetDummyCalibration.Size = new System.Drawing.Size(145, 23);
            this.bnSetDummyCalibration.TabIndex = 8;
            this.bnSetDummyCalibration.Text = "Set Dummy Calibration";
            this.bnSetDummyCalibration.UseVisualStyleBackColor = true;
            this.bnSetDummyCalibration.Click += new System.EventHandler(this.bnSetDummyCalibration_Click);
            // 
            // bnGetTransformed
            // 
            this.bnGetTransformed.Location = new System.Drawing.Point(286, 88);
            this.bnGetTransformed.Name = "bnGetTransformed";
            this.bnGetTransformed.Size = new System.Drawing.Size(145, 23);
            this.bnGetTransformed.TabIndex = 9;
            this.bnGetTransformed.Text = "GetTransformed";
            this.bnGetTransformed.UseVisualStyleBackColor = true;
            this.bnGetTransformed.Click += new System.EventHandler(this.bnGetTransformed_Click);
            // 
            // bnLoopGetTransformed
            // 
            this.bnLoopGetTransformed.Location = new System.Drawing.Point(286, 117);
            this.bnLoopGetTransformed.Name = "bnLoopGetTransformed";
            this.bnLoopGetTransformed.Size = new System.Drawing.Size(145, 23);
            this.bnLoopGetTransformed.TabIndex = 10;
            this.bnLoopGetTransformed.Text = "Loop GetTransformed";
            this.bnLoopGetTransformed.UseVisualStyleBackColor = true;
            this.bnLoopGetTransformed.Click += new System.EventHandler(this.bnLoopGetTransformed_Click);
            // 
            // ckPupilEvents
            // 
            this.ckPupilEvents.AutoSize = true;
            this.ckPupilEvents.Checked = true;
            this.ckPupilEvents.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckPupilEvents.Location = new System.Drawing.Point(15, 146);
            this.ckPupilEvents.Name = "ckPupilEvents";
            this.ckPupilEvents.Size = new System.Drawing.Size(84, 17);
            this.ckPupilEvents.TabIndex = 11;
            this.ckPupilEvents.Text = "Pupil events";
            this.ckPupilEvents.UseVisualStyleBackColor = true;
            // 
            // ckAzimuthEvents
            // 
            this.ckAzimuthEvents.AutoSize = true;
            this.ckAzimuthEvents.Location = new System.Drawing.Point(105, 146);
            this.ckAzimuthEvents.Name = "ckAzimuthEvents";
            this.ckAzimuthEvents.Size = new System.Drawing.Size(98, 17);
            this.ckAzimuthEvents.TabIndex = 12;
            this.ckAzimuthEvents.Text = "Azimuth events";
            this.ckAzimuthEvents.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 301);
            this.Controls.Add(this.ckAzimuthEvents);
            this.Controls.Add(this.ckPupilEvents);
            this.Controls.Add(this.bnLoopGetTransformed);
            this.Controls.Add(this.bnGetTransformed);
            this.Controls.Add(this.bnSetDummyCalibration);
            this.Controls.Add(this.txPupilInfo);
            this.Controls.Add(this.bnGetRaw);
            this.Controls.Add(this.bnStopConnecting);
            this.Controls.Add(this.bnConnect);
            this.Controls.Add(this.txServerPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txServerIP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Haytham TCPIP Client Tester";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txServerIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txServerPort;
        private System.Windows.Forms.Button bnConnect;
        private System.Windows.Forms.Button bnStopConnecting;
        private System.Windows.Forms.Button bnGetRaw;
        private System.Windows.Forms.TextBox txPupilInfo;
        private System.Windows.Forms.Button bnSetDummyCalibration;
        private System.Windows.Forms.Button bnGetTransformed;
        private System.Windows.Forms.Button bnLoopGetTransformed;
        private System.Windows.Forms.CheckBox ckPupilEvents;
        private System.Windows.Forms.CheckBox ckAzimuthEvents;
    }
}

