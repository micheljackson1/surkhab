﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;

namespace Surkhab
{
    [ComVisible(true), InterfaceType(ComInterfaceType.InterfaceIsDual), Guid("2884917E-7CE7-4E35-A5D6-CB23BC4AC598")]
    public interface ISurkhabCOM
    {
        [DispId(1)]
        void Init();
    }
}
