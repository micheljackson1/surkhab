﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;


namespace Surkhab
{
    [ComVisible(true), ClassInterface(ClassInterfaceType.None), Guid("248BD6ED-39EC-424A-B265-834C0A02E787"), ProgId("SensimetricsGazeCOM.Surkhab")]
    public class SurkhabCOM : ISurkhabCOM
    {
        public void Init()
        {
            METState.Current.remoteOrMobile = METState.RemoteOrMobile.MobileEyeTracking;
        }
    }
}
