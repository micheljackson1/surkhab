**This project contains Surkhab, a binocular eye-tracker.**

The project is derived from Haytham (https://sourceforge.net/projects/haytham/), a gaze-tracker written by Diako Mardanbegi at the IT University of Copenhagen (https://en.itu.dk/).

*This is a Visual Studio 2010 (Pro or higher) project, written in C#, targeting .Net 4.*

---

## LICENSE

Surkhab is released under a dual license:

---

## FREE VERSION:

The source code is released under the same license as Haytham, GPL version 3.0. In short, it means that any distributed project that includes or links any portion of Surkhab source code has to be released with the source code under a license compatible with GPLv3.

---

## COMMERCIAL VERSION:

If you want to use the Haytham in a closed source commercial product, you must purchase the license. Please contact Sensimetrics (sensimetrics@sens.com) and the contract adviser at IT University of Copenhagen(jst@itu.dk).